package com.pageobject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CloudControl_Page {
	
	WebDriver driver;
	public CloudControl_Page(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	
	@FindBy(xpath="//input[contains(@id,'email')]")
	public WebElement field_user_name;
	
	@FindBy(xpath="//input[contains(@id,'password')]")
	public WebElement field_password;
	
	@FindBy(xpath="//input[contains(@class,'subbtn')]")
	public WebElement button_Sign_In;
	
	@FindBy(xpath="//a[contains(@class,'manage-company')]")
	public WebElement click_manage_my_company;
	
	@FindBy(xpath="//a[contains(.,'Users')]")
	public WebElement tab_users;
	
	@FindBy(xpath="//a[contains(.,'Add user')]")
	public WebElement buton_add_user;
	
	@FindBy(xpath="//input[contains(@id,'find_by_name')]")
	public WebElement field_search_user;
	
	@FindBy(xpath="//input[contains(@id,'find_by_name_button')]")
	public WebElement button_search;
	
	@FindBy(xpath="//a[contains(.,'ADD BULK USERS')]")
	public WebElement buton_add_bulk_user;
	
	@FindBy(xpath="//html/body/div/div/div/div[2]/div[1]/form/div[2]/div/input")
	public WebElement buton_browse_your_file;
	
	@FindBy(xpath="//div[contains(@id,'user_form')]//input[contains(@id,'user_first_name')]")
	public WebElement field_first_name;
	
	@FindBy(xpath="//input[contains(@id,'user_last_name')]")
	public WebElement field_last_name;
	
	@FindBy(xpath="//input[contains(@id,'user_email')]")
	public WebElement field_user_email;
	
	@FindBy(xpath="//input[contains(@id,'phone')]")
	public WebElement field_phone;
	
	@FindBy(xpath="//input[contains(@id,'room_url')]")
	public WebElement field_room_address;
	
	@FindBy(xpath="//*[@id='password']")
	public WebElement input_password;
	
	@FindBy(xpath="//select[contains(@id,'user_type')]")
	public Select select_user_type;

	@FindBy(xpath="//input[contains(@id,'user_address1')]")
	public WebElement field_address1;
	
	@FindBy(xpath="//input[contains(@id,'user_address2')]")
	public WebElement field_address2;
	
	@FindBy(xpath="//input[contains(@id,'user_city')]")
	public WebElement field_city;
	
	@FindBy(xpath="//input[contains(@id,'user_zip')]")
	public WebElement field_postal_code;
	
	@FindBy(xpath="//select[contains(@id,'country-new')]")
	public Select select_country;
	
	@FindBy(xpath="//input[contains(@id,'province-new')]")
	public WebElement field_state;
	
	@FindBy(xpath="//button[contains(@class,'imeet-buttons')]")
	public WebElement click_save_changes;
	
	@FindBy(xpath="//strong[contains(@class,'notice-detail')]")
	public WebElement confirmation_message;
	
	//Wait for element
	public void waitForElement(WebDriver driver, WebElement myElement, int time){
		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOf(myElement));
	}
}
