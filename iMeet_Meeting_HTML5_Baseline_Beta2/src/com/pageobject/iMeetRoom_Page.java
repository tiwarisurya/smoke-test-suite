package com.pageobject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class iMeetRoom_Page {
	
	WebDriver driver;
	public iMeetRoom_Page(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	
	@FindBy(xpath="//div[contains(@id,'audioScreenAsideCancel')]")
	public WebElement closeicon;
	
	@FindBy(xpath="//select[contains(@id,'phoneType-Dropdown')]")
	public WebElement select_phonetype;
	
	@FindBy(xpath="//div[contains(@id,'addPhoneNumberCountryCode')]")
	public WebElement countrycode;
	
	@FindBy(xpath="//input[contains(@id,'addUserPhoneNumber')]")
	public WebElement phonenumber;
	
	@FindBy(xpath="//input[contains(@id,'addPhoneNumberExt')]")
	public WebElement phoneextension;
	
	@FindBy(xpath="//div[contains(@id,'audioPanelCallMeBtn')]")
	public WebElement button_callme;
	
	@FindBy(xpath="//div[contains(@id,'audioScreenDialInBtn')]")
	public WebElement dialIn;
	
	@FindBy(xpath="//a[contains(@id,'connect')]/span")
	public WebElement connect;
	
	@FindBy(xpath="//a[contains(@id,'addGuest')]/span")
	public WebElement invite;
	
	@FindBy(xpath="//*[@id='sendInviteBubble']/div/span[2]")
	public WebElement send_invite_option;
	
	@FindBy(xpath="//*[@id='addGuestEmail']")
	public WebElement field_guest_email_address;
	
	@FindBy(xpath="//*[@id='addGuestSubject']")
	public WebElement field_enter_subject;
	
	@FindBy(xpath="//*[@id='emailGuestField']/footer/div/a")
	public WebElement button_send_invite;
	
	@FindBy(xpath="//*[@id='chatWindow_chatZoneimeetMsg']/div")
	public WebElement notification_sent_invite;
	
	@FindBy(xpath="//*[@id='errorOfAddGuestEmail']")
	public WebElement error_message_wrong_emailid;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'SEND INVITE')]")
	public WebElement send_invite;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'SEND INVITE')]")
	public WebElement chat_icon;
	
	@FindBy(xpath="//a[contains(@id,'filesCabinetMenu')]/span")
	public WebElement share;
	
	@FindBy(xpath="//a[contains(@title,'Turn On/Off Webcam')]")
	public WebElement video_icon;
	
	@FindBy(xpath="//a[contains(@title,'Call')]")
	public WebElement audio_icon;
	
	@FindBy(xpath="//a[contains(@title,'Info')]")
	public WebElement info_icon;
	
	@FindBy(xpath="//a[contains(@title,'Raise Hand')]")
	public WebElement raise_hand;
	
	@FindBy(xpath="//*[@id='raisedHandCount']")
	public WebElement raise_hand_count;
	
	@FindBy(xpath="//a[contains(@id,'participantRoomButtom')]")
	public WebElement participants_button;
	
	@FindBy(xpath="//span[contains(.,'NANCY12 JAIN12')]")
	public WebElement hostName;
	
	@FindBy(xpath="//a[contains(@id,'chatButton')]")
	public WebElement chat_button;
	
	@FindBy(xpath="//textarea[contains(@class,'chatWindow_textEnter')]")
	public WebElement chat_input_field; 
	
	@FindBy(xpath="//textarea[contains(@id,'roomNotesTextarea')]")
	public WebElement note_input_field; 
	
	@FindBy(xpath="//textarea[contains(@id,'selfNotesTextarea')]")
	//@FindBy(xpath="//textarea[contains(@id,'roomNotesTextarea')]") 
	public WebElement mynote_input_field; 
	
	
	@FindBy(xpath="//*[@id='selfNoteStatusContainer']/span")
	//@FindBy(xpath="//*[@id='roomNoteStatusContainer']/span")
	public WebElement notes_menu_item; 
	
	@FindBy(xpath="//*[@id='emailNotes']")
	public WebElement notes_menu_item_email; 
	
	@FindBy(xpath="//a[contains(@id,'addNoteUser')]")
	public WebElement click_add_note_user; 
	
	@FindBy(xpath="//*[@id='addNewNoteUserEmail']")
	public WebElement input_note_user_email; 
	
	@FindBy(xpath="//*[@id='emailNotesWindow']/fieldset/div[2]/a")
	public WebElement button_Done_note; 
	
	@FindBy(xpath="//span[contains(@class,'enter_chat_txt')]")
	public WebElement received_chat; 
	
	@FindBy(xpath="//p[contains(@class,'noteMsgContent')]") 
	public WebElement received_note; 
	
	@FindBy(xpath="//*[@id='cubeFrontChatIcon5026116']")
	public WebElement guest_chat_icon; 

	@FindBy(xpath="//a[contains(@id,'participantRoomButtom')]")
	public WebElement participantRoomButtom; 
	
	@FindBy(xpath="//a[contains(@id,'notesButton')]")
	public WebElement notes_button;
	
	@FindBy(xpath="//*[@id='selfNotesHeader']/span[2]")
	public WebElement my_notes;
	
	@FindBy(xpath="//a[contains(@id,'panelSort')]")
	public WebElement panel_icon;	
	
	@FindBy(xpath="//a[contains(@id,'roomControl')]")
	public WebElement control_menu;
	
	@FindBy(xpath="//span[contains(@id,'recordingIconMsg')]")
	public WebElement record_meeting;
	
	@FindBy(xpath="//span[contains(@id,'lockIconMsg')]")
	public WebElement lock_meeting;
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement lock_meeting_confirm;
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement button_template_blue;
	
	
	@FindBy(xpath="//*[@id='participantRoomButtom']/div")
	public WebElement text_active_participant;
	
	@FindBy(xpath="//div[contains(@id,'unlockMeetingBtn')]")
	public WebElement unlock_meeting;
	
	@FindBy(xpath="//p[contains(@id,'templateHeader')]")
	public WebElement locked_meeting_message;
	
	//@FindBy(xpath="//*[@id='roomkeyWindow']/div/div/strong") 
	@FindBy(xpath="//div[@id='roomKeyMsgTxt']")
	public WebElement locked_meeting_message_Key;
	
	@FindBy(xpath="//span[contains(@id,'endMeetingMsg')]")
	public WebElement leave_meeting;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'EDIT PROFILE')]")
	public WebElement edit_profile;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'Settings')]")
	public WebElement settings;
	
	@FindBy(xpath="//a[contains(@id,'helpPermimumContactUsTab')]")
	public WebElement help_contactus;
	
	@FindBy(xpath="//*[@id='givefeedback']/footer/div/a[2]")
	public WebElement help_contactUs_send;
	
	@FindBy(xpath="//a[contains(@id,'helpPermimumSupportTab')]")
	public WebElement help_resource;
	
	@FindBy(xpath="//*[@id='supportCommunityLink']")
	public WebElement help_Resource_Community;
	
	@FindBy(xpath="//a[contains(@id,'helpPermimumFeedbackTab')]")
	public WebElement help_premiumfeedback;
	
	@FindBy(xpath="//*[@id='tellusabout']/footer/div/a")
	public WebElement help_feedback_submit;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'HELP')]")
	public WebElement help;
	
	@FindBy(xpath="//a[contains(@id,'settings_meeting_tab')]")
	public WebElement setting_meeting;
	
	@FindBy(xpath="//*[@id='lockMeetingControl']/span[2]")
	public WebElement setting_meeting_lockMeeting;
	
	//@FindBy(xpath="//*[@id='lockMeetingControl']/span[1]")
	@FindBy(xpath="//*[@id='lockMeetingControl']")
	public WebElement setting_meeting_lockMeeting_status;
	
	@FindBy(xpath="//input[contains(@id,'roomKeyField')]")
	public WebElement setting_meeting_roomkey;
	
	@FindBy(xpath="//*[@id='settingsWindow']/div[2]/a")
	public WebElement setting_meeting_roomkey_Done;
	
	@FindBy(xpath="//a[contains(@id,'settings_theme_tab')]")
	public WebElement setting_theme;
	
	@FindBy(xpath="//a[contains(@id,'settings_notification_tab')]")
	public WebElement setting_notifications;
	
	@FindBy(xpath="//a[contains(@id,'settings_files_tab')]")
	public WebElement setting_files;
	
	@FindBy(xpath="//a[contains(@id,'settings_dialing_tab')]")
	public WebElement setting_dialing;
	
	@FindBy(xpath="//a[contains(@id,'settings_more_tab')]")
	public WebElement setting_more;
	
	@FindBy(xpath="//a[contains(@id,'settings_more_account')]")
	public WebElement setting_more_account;
	
	@FindBy(xpath="//a[contains(@id,'settings_more_privacy')]")
	public WebElement setting_more_privacy;
	
	@FindBy(xpath="//div[contains(@class,'feedreport')]")
	public WebElement save_profile;
	
	@FindBy(xpath="//*[@id='chatWindow_chatZoneimeetMsg']/div")
	public WebElement Addto_contact_confirmation;
	
	@FindBy(xpath="//*[@id='filesCabinetMenu']/span")
	public WebElement click_share;
	
	@FindBy(xpath="//*[@id='fileNavigationTxt']")
	public WebElement click_share_file;
	
	@FindBy(xpath="//*[@id='addNewfile']")
	public WebElement click_addnew_file;
	
	@FindBy(xpath="//*[@id='addNewVideo']")
	public WebElement click_addnew_video;
	
	@FindBy(xpath="//*[@id='0']/div[1]")
	public WebElement click_first_file;
	
	@FindBy(xpath="//*[@id='fileshowlink']")
	public WebElement click_present;
	
	@FindBy(xpath="//*[@id='presentationNameDisplay']")
	public WebElement verify_presented_file;
	
	@FindBy(xpath="//*[@id='passControlBtn']")
	public WebElement click_pass_control;
	
	@FindBy(xpath="//*[@id='passControlSearch']")
	public WebElement search_user_field;
	
	@FindBy(xpath="//*[@id='pC_userRow_4644292']/div[2]/p")
	public WebElement search_result_user;
	
	@FindBy(xpath="//*[@id='acceptBtn']/span")
	public WebElement accept_control;
	
	@FindBy(xpath="//*[@id='passControlPopUpBody']/font[2]")  
	public WebElement confirm_control_accepted;

	@FindBy(xpath="//*[@id='okBtn']/span")
	public WebElement ok_button_passcontrol;
	
	@FindBy(xpath="//*[@id='requestControlBtn']/span")
	public WebElement click_request_control;
	
	@FindBy(xpath="//*[@id='acceptBtn']/span")
	public WebElement click_grant;

	@FindBy(xpath="//*[@id='presentationNameDisplay']")
	public WebElement presented_file_name;
	
	@FindBy(xpath="//*[@id='gloginpasswordWindow']/header/div[2]/span")
	public WebElement save_profile_close;
	
	@FindBy(xpath="//*[@id='templateMessage']")
	public WebElement continueAs_guest_message;
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement button_ok;
	
	@FindBy(xpath="//*[@id='fileMemory']")
	public WebElement file_memory;
	
	
	
	
	
	
	
	//Search for a video using keyword imeet
	//*[@id='videoSearch']
	
	//Click on Add video
	//*[@id='addvideorow0']/div[1]
	
	//Verify that Video added 
	//*[@id='videoaddedstatus0']  
	@FindBy(xpath="//*[@id='videoSearch']")
	public WebElement input_field_videosearch;
	
	@FindBy(xpath="//*[@id='addVideoSearchButton']")
	public WebElement search_button;
	
	@FindBy(xpath="//*[@id='addvideorow0']/div[1]")
	public WebElement search_result_video;
	
	@FindBy(xpath="//*[@id='videoaddedstatus0']")
	public WebElement video_Added_Msg;
	
	@FindBy(xpath="//*[@id='youtubeDonelink']")
	public WebElement click_done;
	
	//Wait for element
	public void waitForElement(WebDriver driver, WebElement myElement, int time){
		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOf(myElement));
	}
}
