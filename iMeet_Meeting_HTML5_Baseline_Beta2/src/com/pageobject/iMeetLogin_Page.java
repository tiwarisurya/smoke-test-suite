package com.pageobject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class iMeetLogin_Page {
	
	WebDriver driver;
	public iMeetLogin_Page(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	
	@FindBy(xpath="//p[contains(@id,'goToSignInButton')]")
	public WebElement link_UseTheBrowser;
	
	@FindBy(xpath="//p[contains(@id,'goToSignIn')]")
	public WebElement link_UseTheBrowserButton;
	
	@FindBy(xpath="//a[contains(@class,'join-via-browser-link')]")
	public WebElement link_JoinUsingBrowser;
	
	@FindBy(xpath="//input[contains(@name,'first-name')]")
	public WebElement field_EnterFirstName;
	
	@FindBy(xpath="//input[contains(@name,'last-name')]")
	public WebElement field_EnterLastName;
	
	@FindBy(xpath="//input[contains(@type,'email')]")
	public WebElement field_EnterEmail;
	
	@FindBy(xpath="//input[contains(@type,'password')]")
	public WebElement field_EnterPassword;
	
	@FindBy(xpath="//button[contains(@id,'sign-In-SubmitBtn')]")
	public WebElement button_Submit;
	
	@FindBy(xpath="//div[contains(@class,'enter-sign-in blue-button-wide')]")
	public WebElement button_SignIn;
	
	@FindBy(xpath="//p[contains(@id,'sign-in-error')]")
	public WebElement text_ErrormsgForName;
	
	@FindBy(xpath="//p[contains(@id,'sign-in-email-error')]")
	public WebElement text_ErrormsgForEmail;
	
	//@FindBy(xpath="//a[contains(.,'Continue as guest')]")
	@FindBy(xpath="//a[contains(.,'continueAsGuest')]")
	public WebElement link_ContinueAsGuest;
	
	//@FindBy(xpath="//*[@id='sign-in-password']/div[1]/form/div/a[1]")
	@FindBy(xpath="//a[contains(.,'Forgot password?')]")
	public WebElement link_ForgotPassword;
	
	
	@FindBy(xpath="//p[contains(@id,'sign-in-password-error')]")
	public WebElement text_PasswordIncorrect;
	
	@FindBy(xpath="//*[@id='content']/div/div[2]/h2")
	public WebElement text_forgetyourPassword;
	
	@FindBy(xpath="//*[@id='input-a']")
	public WebElement field_emailaddress;
	
	@FindBy(xpath="//*[@id='jse']")
	public WebElement text_entervalidemail;
	
	@FindBy(xpath="//*[@id='forgot-password']/input")
	public WebElement link_emailmypassword;
	
	
	@FindBy(xpath="//*[@id='content']/div/div[2]/div/div[1]/h2")
	public WebElement text_passwordontheway;
	
	
	@FindBy(xpath="//*[@id='content']/div/div[2]/div/div/p")
	public WebElement text_couldntfinduser;
	
	@FindBy(xpath="//div[contains(@id,'audioScreenAsideCancel')]")
	public WebElement closeicon;
	
	
	//Wait for element
	public void waitForElement(WebDriver driver, WebElement myElement, int time)
	{
		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOf(myElement));
	}
	
	
	public void switchFrame(WebDriver driver)
	{
		//driver.switchTo().frame("client_frame");
		//System.out.println("Dev Remove the iframe from imeet");
	}
}
