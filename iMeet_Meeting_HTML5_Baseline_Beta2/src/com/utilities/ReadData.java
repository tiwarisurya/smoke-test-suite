package com.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ReadData {
	
		
	Workbook wb = null;
	Sheet ws;
	static Hashtable<String, Integer> dict = new Hashtable<String, Integer>();
	static Hashtable<String, Integer> dictionary = new Hashtable<String, Integer>();

	public ReadData(String xlpath, String SheetName) throws Exception, IOException  // Constructor
	{
		wb = Workbook.getWorkbook(new File(xlpath));
		ws = wb.getSheet(SheetName);
	}

// Get Excel Cell Value using Column index
	//public String getCellValue(String sheetName, int colNum, int rowNum)
	public String getCellValue(int colNum, int rowNum)
	{
		//return wb.getSheet(sheetName).getCell(colNum, rowNum).getContents();
		return ws.getCell(colNum,rowNum).getContents();
		
	}

//Returns the Number of Rows
	public int RowCount() throws BiffException, IOException
	{
		int Total_Row = ws.getRows();
		return Total_Row;
	}
	
//Returns the Number of Columns
	public int ColumnCount() throws BiffException, IOException
	{
		int Total_Col = ws.getColumns();
		return Total_Col;
	}
	
	
//write to an existing excel using column index
	public static void WriteToExcel(String fPath, String fSheet, int colnum, int rownum, String value) throws BiffException, IOException,
									RowsExceededException, WriteException
	{
		 Workbook wb = Workbook.getWorkbook(new File(fPath));
		 //create a new excel and copy from existing
		 WritableWorkbook copy = Workbook.createWorkbook(new File(fPath), wb);
		 WritableSheet sheet = copy.getSheet(fSheet);		
		 Label label = new Label(colnum, rownum, value); //Label(colno, rowno, string)
		 sheet.addCell(label);
		 copy.write();
		 copy.close();
	}
	
//write to an existing excel using column Name
	public void WriteReport(String fPath, String fSheet, int colnum, int rownum, String value) throws BiffException, IOException,
	RowsExceededException, WriteException
	{
		Workbook wb = Workbook.getWorkbook(new File(fPath));
		//create a new excel and copy from existing
		WritableWorkbook copy = Workbook.createWorkbook(new File(fPath), wb);
		WritableSheet sheet = copy.getSheet(fSheet);
		Label label = new Label(colnum, rownum, value); //Label(colno, rowno, string)
		sheet.addCell(label);
		copy.write();
	 	copy.close();
}

	
//Returns the Cell value by taking row and Column values as argument
	public String ReadCellValue(int column,int row)
	{
	  return ws.getCell(column,row).getContents();
	}
	
//Create Column Dictionary to hold all the Column Names
	public void ColumnDictionary()
	{
		//Iterate through all the columns in the Excel sheet and store the value in Hash table
		for(int colNum=0; colNum< ws.getColumns(); colNum++)
		{

		  dict.put(ReadCellValue(colNum,0), colNum);
		}
	 }

	public String ReadCellValuesummary(int column,int row)
	{
	  return ws.getCell(column,row).getContents();
	}
	
	public void ColumnDictionarysummary()
	{
		//Iterate through all the columns in the Excel sheet and store the value in Hash table
		for(int colNum=0; colNum< ws.getColumns(); colNum++)
		{

		  dictionary.put(ReadCellValue(colNum,0), colNum);
		}
	 }
	
	 public int GetColNamesummary(String ColName)
	 {
		 try
		 {
			 int value;
			 value = ((Integer) dictionary.get(ColName)).intValue();
			 return value;
		 }
		 catch (NullPointerException e)
		 {
			 System.out.println("In the GetColumn Name function from Utilities package ReadData Class return exception 0");
			 return (0);
		  }
	 }
	 
	//Read Column Names
	 public int GetColName(String ColName)
	 {
		 try
		 {
			 int value;
			 value = ((Integer) dict.get(ColName)).intValue();
			 return value;
		 }
		 catch (NullPointerException e)
		 {
			 System.out.println("In the GetColumn Name function from Utilities package ReadData Class return exception 0");
			 return (0);
		  }
	 }
	 
	 // Read Data by col name is :  ReadData.ReadCell(ReadData.GetCell("EmailUserName"), rowCnt));

// *************************************************************************************************
// Read Data from excel into Array  
		
		public String [][] ReadXL() throws Exception
		{
			int TRow, TCol;
	    	String[][] xdata = null;
	        // Declare the Array Size
	        xdata = new String[ws.getColumns()][ws.getRows()];
	        
	        TRow = ws.getRows(); 
	        TCol = ws.getColumns(); 
	        for (int i = 0; i < TRow; i++){
	          for (int j = 0; j < TCol; j++) {
	              Cell cell = ws.getCell(j, i);
	              xdata[j][i] = cell.getContents();
	           }
	        }
	        return xdata;
	    }
		
		//Get the current directory
		public static String getTestDataDirectory(String filename)
		{
			Path currentRelativePath = Paths.get("");
			String s = currentRelativePath.toAbsolutePath().toString();
			String workingpath = s +"\\src\\Resources\\"+filename;
			return(workingpath);
		}
		
		//Get the screenshot directory
		public static String getScreenshotDirectory()
		{
			Path currentRelativePath = Paths.get("");
			String s = currentRelativePath.toAbsolutePath().toString();
			String workingpath = s +"\\Screenshot";
			return(workingpath);
		}
	
}
