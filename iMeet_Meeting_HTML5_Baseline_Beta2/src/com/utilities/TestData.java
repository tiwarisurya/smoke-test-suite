package com.utilities;

public class TestData {
	
	//public String Url = "https://beta2.imeetbeta.net/testurl/surya?html=true";
	//public String Url = "https://beta2.imeetbeta.net/pgiqa/mhbeta2?html=true";
	//public String Url = "https://beta2.imeetbeta.net/meeting/host1?html=true";
	public String Url = "https://beta2.imeetbeta.net/testmay18/test?html=true";
	
	public String BrowserType = "ff";          
	public String HostFirstName = "Test";
	public String HostlastName = "Host";
	public String HostEmail = "test.user@testing001.com";  // "nancy.jain@nagarro.com";
	
	public String Pswd = "myimeet123";
	public String GuestPswd	= "myimeet123"; // "myimeet123";
	public String WrongUserPswd = "@@@@@@@";
	
	public String GuestFirstName = "Test";
	public String GuestLastName = "User";
	
	public String RegisteredGuestEmail	= "test.user@test.com";
	public String UnregisteredGuestEmail =	"mhsn.ny@gmail.com";
	public String EmailRoomNote = "mhsn.ny@gmail.com";
	
	public String lockedMeetingMessage = "THE MEETING IS LOCKED";
	public String PrivateNote = "This is Private Note as ";
	public String ErrorMessageName = "Enter a valid name.";	
	public String ErrorMessageEmail = "Please enter a valid email";
	public String ChatMessage = "This is Chat";	
	public String NoteMessage ="This is Note";	
	public String LockedMeetingMessage = "THE MEETING IS LOCKED";
	public String RoomKey = "1234";
	public String LockedMeetingMessage_KEY = "This meeting is locked. Please enter the room key to continue.";
	public String FileName_MP4 = "Wildlife_mp4.mp4"; //"imeetmeeting.mp4";
	public String FileUpload = "bmw i8.jpg";
	public String YouTubeVideoAdd = "Global meet";
	
	
	
	//Read Data set for cloud control features
	public String CloudUrl = "https://beta2.imeetbeta.net/login/";
	public String CloudUser = "surya.tiwari@nagarro.com";
	public String CloudPwd = "myimeet123";
	public String user_fName = "Surya";
	public String user_lName = "T";
	public String user_email = "cvtestcx11@gmail.com";
	public String user_updated_phone = "0123456789";
	public String user_phone = "1234567890";
	public String user_roomAddress = "test2";
	public String user_password = "myimeet123";
	public String user_address1 = "testaddress1";
	public String user_address2 = "testaddress2";
	public String user_city = "testcity";
	public String user_postalcode = "123456";
	public String user_state = "teststate";
	
	//Read Data set for BMP
	public String BMPUrl = "https://pgi:erdbeereis@testcloud.bmptest.de/"; // http://pgi:erdbeereis@testcloud.bmptest.de/
	public String BMPUser = "pgi";
	public String BMPPwd = "erdbeereis";
}
