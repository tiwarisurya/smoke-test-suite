package com.test;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;



import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_43_FilePresentationMP4 {
		
	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC43";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName,vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, vFilename_MP4, vChatmessage;
		
	@Test //TC43 File Presentation Mp4 
	public void TC43_filepresentationMP4() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vGuestPswd = d.GuestPswd;
		vHostEmail = d.HostEmail;
		vGuestEmail = d.UnregisteredGuestEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vChatmessage= d.ChatMessage;
		vFilename_MP4 = d.FileName_MP4.toUpperCase();
		
		try
		{
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login.switchFrame(guest);
			//Click to Join 
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			//Verify Audio Connect panel
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			//Login with Host
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(host);
			host = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login1.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Host
			login1.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(5000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on share
				imeet_Room1.click_share.click();
				Thread.sleep(2000);
				
				//Click on share a file
				imeet_Room1.click_share_file.click();
				Thread.sleep(6000);
				
				//Click on Add new file
				imeet_Room1.click_addnew_file.click();
				Thread.sleep(5000);
			
				//Enter file name. File located in Resources Package
				String mp4filename = vFilename_MP4.toLowerCase();
				StringSelection sel = new StringSelection(ReadData.getTestDataDirectory(mp4filename));
			
				//Copy to clipboard
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
			
				//Create object of Robot class
				Robot robot = new Robot();
				Thread.sleep(1000);
			      
				// Press Enter
				robot.keyPress(KeyEvent.VK_ENTER);
			 
				// Release Enter
				robot.keyRelease(KeyEvent.VK_ENTER);
			 
				// Press CTRL+V
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_V);
			 
				// Release CTRL+V
				robot.keyRelease(KeyEvent.VK_CONTROL);
				robot.keyRelease(KeyEvent.VK_V);
				Thread.sleep(1000);
			        
				//Press Enter 
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
			
				//***************************
				// Wait until file load
				Thread.sleep(5000);
				String FileInRoom = imeet_Room1.file_memory.getText();
				int FirstTimeFilenumber=0;
				String[] Part1, Part2;
				String File;
			
				boolean isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
				if(isFileNumberDisplay)
				{
					Part1 = FileInRoom.split(":");
					Part2 = Part1[1].split("\\(");
					File = Part2[0].trim();
					FirstTimeFilenumber = Integer.parseInt(File);	
				}

				int Filecount = FirstTimeFilenumber;
				int timer =1;
				while (timer < 60) // While loop wait up to 1 minute
				{
					Thread.sleep(1000);
					if(Filecount>FirstTimeFilenumber){break;}
					FileInRoom = imeet_Room1.file_memory.getText();
					isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
					if(isFileNumberDisplay)
					{
						Part1 = FileInRoom.split(":");
						Part2 = Part1[1].split("\\(");
						File = Part2[0].trim();
						Filecount = Integer.parseInt(File);	
					}
					timer++;
				}
			
				//**************************
				Thread.sleep(6000);
				//Select a file and 
				imeet_Room1.click_first_file.click();

				//Click on Present
				Thread.sleep(4000);
				imeet_Room1.click_present.click();
			
				//Verify presented file on guest screen
				Thread.sleep(5000);
				String presented_file_name = imeet_Room.presented_file_name.getText();
				String uppercasestring = presented_file_name.toUpperCase();
				System.out.println("Presented file name " + uppercasestring + " where expected mp4 file is " + vFilename_MP4);
				Thread.sleep(5000);
				s_assert.assertEquals(uppercasestring, vFilename_MP4, "Presented file name " + uppercasestring + " where expected mp4 file is " + vFilename_MP4);	
	
			}
			catch(Throwable ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error Occured for Presenting MP4 file. Please check reporter log.");
				Reporter.log(ex.getMessage());
			}
		}
		catch (Throwable ex)
		{
		
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Please check reporter log.");
			Reporter.log(ex.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				guest.quit();
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_43 close
	
} //main close