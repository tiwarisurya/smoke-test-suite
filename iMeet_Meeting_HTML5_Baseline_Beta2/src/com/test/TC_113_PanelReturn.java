package com.test;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_113_PanelReturn{
	
	public WebDriver host;
	public WebDriver guest;
	
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC113";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vguestfirstName, vguestlastName,vhostfirstName, vhostlastName, vHostEmail,vGuestEmail;

		
	@Test //TC113 Panel - Return
	public void TC_113panelReturn() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vHostEmail = d.HostEmail;
		vPswd = d.Pswd;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		

		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(2000);
			login.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
					
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
				
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
					
			//Login with Host
			login.field_EnterEmail.sendKeys(vHostEmail);
					
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(3000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			
			//Login with guest	
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowserguest = new ChooseBrowser(guest);
			guest = chooseBrowserguest.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
						
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
				
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
					
			//Login with Host
			login1.field_EnterEmail.sendKeys(vGuestEmail);
					
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			
			try
			{
				imeet_Room1.participants_button.click();
				Thread.sleep(1000);
		        
				WebElement PanelGuestName = guest.findElement(By.xpath("//span[contains(.,'"+vguestfirstName.toUpperCase()+" "+vguestlastName.toUpperCase()+"')]"));
				String GuestAttrbuteName = PanelGuestName.getAttribute("id");
				String[] parts = GuestAttrbuteName.split("r");
				String GuestAttributId = parts[1]; // 4644292
				Thread.sleep(1000);
				
				WebElement GuestName = guest.findElement(By.xpath("//*[@id='nameOfUser"+GuestAttributId+"']"));
				WebElement PanelGuestStayAway = guest.findElement(By.xpath("//a[@id='panelStayAway"+GuestAttributId+"']"));
				
				GuestName.click();
				Thread.sleep(500);		
				PanelGuestStayAway.click();
				Thread.sleep(1000);
				
				//Guest checking on his cube to see guest Stay Away
				//Cube Stay Away verification
				WebElement GuestStayAwayCube = guest.findElement(By.xpath("//div[contains(@id,'stepAway"+GuestAttributId+"')]"));
				WebElement HostCheckStayAwayCube = host.findElement(By.xpath("//div[contains(@id,'stepAway"+GuestAttributId+"')]"));
				
				boolean isStayAwayDisplayGuestCube = GuestStayAwayCube.isDisplayed();
				boolean isStayAwayDisplayHostCheckCube = HostCheckStayAwayCube.isDisplayed();
				//s_assert.assertTrue(isStayAwayDisplayGuestCube, "Fail. Guest Raise hand not displayed on Guest cube.");
				Thread.sleep(500);
				
				if(isStayAwayDisplayGuestCube && isStayAwayDisplayHostCheckCube)
				{
					//Click on panel to Return
					PanelGuestName.click();
					Thread.sleep(3000);
					//Verify step away is not shown after user returned
					boolean isGuestHostReturnCube = GuestStayAwayCube.isDisplayed() && HostCheckStayAwayCube.isDisplayed();
					s_assert.assertFalse(isGuestHostReturnCube, "AWAY still displayed on Cube , after clicking on Return" );
					
				}
				else
				{
					throw new SkipException(TestCaseName + " Test Case Skipping. Because Guest not stay away to test return test case. ");
				}
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Step away icon not displayed on Host cube");
			}
	
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_113 close
	
} //main close