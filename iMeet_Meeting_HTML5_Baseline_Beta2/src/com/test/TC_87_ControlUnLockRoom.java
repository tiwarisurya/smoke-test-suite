package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_87_ControlUnLockRoom 
{
	
	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC87";
	boolean isHostLogedIn = false;

	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName, vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, vlockedMeetingMessage;
		
	@Test //TC86 Controls - Lock Room
	public void TC87_controlUnLockRoom() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vGuestPswd = d.GuestPswd;
		vhostfirstName = d.HostFirstName; 
		vhostlastName = d.HostlastName;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vHostEmail = d.HostEmail;
		vGuestEmail = d.UnregisteredGuestEmail;
		vlockedMeetingMessage = d.LockedMeetingMessage.toUpperCase();
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			
			common.Host_LogIn_Option(host, sPath, PicName);		
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			//Click on Menu icon
			try
			{
				//Lock meeting room
				imeet_Room.control_menu.click();
				String meetingstatus = imeet_Room.lock_meeting.getText().toUpperCase();
				if(meetingstatus.contains("LOCK MEETING"))
				{
					imeet_Room.lock_meeting.click();  
					//Click on Close for Lock meeting
					imeet_Room.lock_meeting_confirm.click();
					Thread.sleep(1000);
				}
			}
			catch (Exception ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error occured to lock the meeting Room. Please check the Report log.");
				Reporter.log("Lock meeting Room Exception is " + ex.getMessage());
			}
			//Login with Guest
		
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(guest);
			guest = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
		
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
			
			//Login with Host
			login1.field_EnterEmail.sendKeys(vGuestEmail);
			
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			try
			{			
			    //Verify meeting is locked.
				imeet_Room1.waitForElement(guest, imeet_Room1.locked_meeting_message, 10);
				String lockedmeetingMessage = imeet_Room1.locked_meeting_message.getText().toUpperCase();
				s_assert.assertEquals(lockedmeetingMessage, vlockedMeetingMessage, "Meeting Lock failed. Expected message " + vlockedMeetingMessage +" where actual message " + lockedmeetingMessage);
			
				//Host Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(500);
				String meetingstatus = imeet_Room.lock_meeting.getText().toUpperCase();
				if(meetingstatus.contains("UNLOCK MEETING"))
				{
					imeet_Room.unlock_meeting.click();
					Thread.sleep(1000);
					imeet_Room.control_menu.click();
					Thread.sleep(2000);
				}
				
				//Refresh the guest screen
				guest.navigate().refresh();
				
				// Use the browser- button
				imeet_Room1.waitForElement(guest, login1.link_UseTheBrowser, 20);
				login1.link_UseTheBrowser.click();
				
				//Verify Audio Connect panel
				isHostLogedIn = common.Host_Audio_Panel_Display(guest, sPath, PicName, isHostLogedIn);
				s_assert.assertTrue(isHostLogedIn, "Meeting Room still lock. Audio Connect panel is not present for the guest.");
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error Occured");
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Please check the report log");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_87 close
	
} //main close