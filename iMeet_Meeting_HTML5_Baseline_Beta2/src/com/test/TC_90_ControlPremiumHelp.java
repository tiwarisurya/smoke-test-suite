package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_90_ControlPremiumHelp 
{
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC90";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String vhostfirstName, vhostlastName, vguestfirstName, vguestlastName, vHostEmail,vGuestEmail;
		
	@Test ////TC90 Controls - Lock Room
	public void TC_90controlPremiumHelp() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName =d.HostFirstName;
		vhostlastName = d.HostlastName;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vHostEmail = d.HostEmail;
		vGuestEmail = d.UnregisteredGuestEmail;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Login with Host
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
				
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
		
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
			
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
			
			//Click on Join Meeting
			login.button_Submit.click();
			common.Host_Enter_Password(host, vPswd, TestCaseName);
		
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				Thread.sleep(2000);
				//Click on Menu icon
				imeet_Room.control_menu.click();
			
				//Click on Help
				imeet_Room.help.click();
			
				//Verify Contact us
				imeet_Room.waitForElement(host, imeet_Room.help_contactus, 10);
				
				imeet_Room.help_contactus.click();
				Thread.sleep(2000);
				System.out.println(imeet_Room.help_contactUs_send.isDisplayed());
				s_assert.assertTrue(imeet_Room.help_contactUs_send.isDisplayed(), "Contact us Tab not present.");
				
				//Verify Resources
				imeet_Room.help_resource.click();
				Thread.sleep(500);
				s_assert.assertTrue(imeet_Room.help_Resource_Community.isDisplayed(), "Resources Tab not present.");
				//Verify Feedback
				imeet_Room.help_premiumfeedback.click();
				Thread.sleep(500);
				s_assert.assertTrue(imeet_Room.help_feedback_submit.isDisplayed(), "Feedback Tab is not present.");
				Thread.sleep(500);
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Contact us, Premium support, Feedback Tab not present. Detail " + t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_90 close
	
} //main close