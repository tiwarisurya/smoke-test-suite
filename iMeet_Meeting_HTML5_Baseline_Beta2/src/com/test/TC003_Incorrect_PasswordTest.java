package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class TC003_Incorrect_PasswordTest {
	
	public static WebDriver Host;
	
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC03";
	
	TestData d = new TestData();
	String vUrl = d.Url;
	String vBrowserType = d.BrowserType;
	
	@BeforeMethod
	public void before_Test() throws IOException, Exception 
	{
	  ChooseBrowser Browser = new ChooseBrowser(Host);
	  Host = Browser.OpenBrowser(vBrowserType, vUrl);
	  Thread.sleep(1000);
	}
	 
	@AfterMethod
	public void afterTest() 
	{
	  if (Host != null) {Host.quit();}
	}
	
	@Test(dataProvider = "IncorrectPswd") // TC003 incorrect password and Register User required as test data
	public void TC003_LoginIncorrectPassword(String Email, String Pswd) throws Exception
	{
		//Creating required objects
		iMeetLogin_Page login = new iMeetLogin_Page(Host);
		Thread.sleep(500);
		CommonFunctions common = new CommonFunctions();
		Thread.sleep(1000);
		login.switchFrame(Host);
		common.Host_LogIn_Option(Host, sPath, PicName);
		
		Thread.sleep(1000);
		//Enter first name		
		login.field_EnterFirstName.clear();
		login.field_EnterFirstName.sendKeys("Registered");
						
		//Enter Last name
		login.field_EnterLastName.clear();
		login.field_EnterLastName.sendKeys("User");
		
		//Enter email id
		login.field_EnterEmail.clear();
		login.field_EnterEmail.sendKeys(Email);
				
		//Click on Join Meeting
		login.button_Submit.click();
		Thread.sleep(2000);
		
		boolean vStatus = false;
		
		try // 1. Checking if the user register or unregister.
		{
			boolean isPswdFieldPresent = login.field_EnterPassword.isDisplayed();	
			if(isPswdFieldPresent)
			{
				login.field_EnterPassword.clear();
				login.field_EnterPassword.sendKeys(Pswd);
				Thread.sleep(1000);
				//Click on Sign In
				login.button_SignIn.click();
				Thread.sleep(1000);
				try{ // If right password provide, control goes to catch
					String actualerrorpassword = login.text_PasswordIncorrect.getText().trim(); 
					System.out.println(Email + " user's Password field error message " + actualerrorpassword);
					boolean isactualerrorpassword = actualerrorpassword.equalsIgnoreCase("Password incorrect.");
					if (isactualerrorpassword)
					{
						Reporter.log("Pass. TC003 Incorrect Password Test. AUT displayed '" +  actualerrorpassword + "' message as user entered wrong password.");
					}
					else
					{
						sshot.ScrShot(Host, sPath,PicName +common.CurrentTime()+".jpg");
						Reporter.log("Fail. TC003 Incorrect Password Test. Expected value 'Password incorrect.' where AUT displayed '" +  actualerrorpassword + "' message as user entered wrong password.");
						s_assert.fail(" AUT return '" + actualerrorpassword + "' Which is not expected.");
					}
				// If user entered correct password, control come here
				}
				catch(Throwable t2)
				{
					vStatus = true;
					System.out.println(Email + " user Enter right password");
					s_assert.assertTrue(vStatus, " TC003 Incorrect Password Test fail for right password.");
					Reporter.log("Pass. TC003 Incorrect Password Test. " + Email + " user enter correct password.");
				}
				
			}
			else // if password field not present
			{
				vStatus = true;
				System.out.println(Email + " is not a Register user....");
				s_assert.assertTrue(vStatus, " TC003 Incorrect Password Test Pass ");
				Reporter.log("Pass. TC003 Incorrect Password. Password field not present for unregister user "+  Email );
			}		
		}
		catch(Throwable t1)
		{
			System.out.println(Email + " is not a Register user from catch block");
			Reporter.log("Pass. TC003. "+  Email + " is unregister User. which scenario expected in this case. To resolve the warning issue, check your Test Data");
		}
		s_assert.assertAll();
	} // Method Close
	
	@DataProvider(name="IncorrectPswd")
		
	public Object[][] RegUser()
	{
		Object [][] RegData= new Object[3][2];
		
		RegData[0][0]="nancy.jain@nagarro.com";
		RegData[0][1]="myimeet123";
		RegData[1][0]="dummyemail@gmail.com";
		RegData[1][1]="Password10101";
		RegData[2][0]="test.user@test.com";
		RegData[2][1]="Passworddddd99";

		return RegData;
	}

} // Class Close
