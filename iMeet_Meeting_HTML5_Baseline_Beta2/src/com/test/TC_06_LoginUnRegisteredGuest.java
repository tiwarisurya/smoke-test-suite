package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_06_LoginUnRegisteredGuest {
	
	public WebDriver guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC_06";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String vfirstName, vlastName,vUnregisteredguestEmail;
		
	@Test //TC06 Login - UnRegistered Guest
	public void TC006_loginUnRegisteredGuest() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vfirstName = d.GuestFirstName;
		vlastName = d.GuestLastName;
		vUnregisteredguestEmail = d.UnregisteredGuestEmail;
		try
		{
			//Choose Browser and open for the guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
			login.switchFrame(guest);
			
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			Thread.sleep(1000);
			//Enter first name
			login.field_EnterFirstName.sendKeys(vfirstName);
				
			//Enter Last name
			login.field_EnterLastName.sendKeys(vlastName);
					
			//Enter email id
			login.field_EnterEmail.sendKeys(vUnregisteredguestEmail);
					
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			
			common.Guest_Enter_Password(guest, vPswd);
		
			isHostLogedIn = common.Host_Audio_Panel_Display(guest, sPath, PicName, isHostLogedIn);
			
			try
			{
				Thread.sleep(1000);
				//Click on control menu
				imeet_Room.control_menu.click();
				
				//click on Settings
				imeet_Room.settings.click();
				
				//Verify save profile is shown for unregistered guest
				s_assert.assertTrue(imeet_Room.save_profile.isDisplayed(), "Save profile is not shown for unregistered guest.");
				imeet_Room.save_profile_close.click();
				Thread.sleep(1000);
			}
			catch(Throwable t)
			{
				sshot.ScrShot(guest, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Save profile is not shown for unregistered guest");
			}
	}
	catch (Throwable t)
	{
		sshot.ScrShot(guest, sPath, PicName+".jpg");
		s_assert.fail("Error occured. plz see the Report log");
		Reporter.log(t.getMessage());
	}
	finally
	{
		if(isHostLogedIn)
		{
			ChooseBrowser cb = new ChooseBrowser(guest);
			cb.SignOutFromMeetingRoom(guest);
		}
		
		if (guest != null)
		{
			guest.quit();
			Thread.sleep(1000);
		}
	}
	s_assert.assertAll();
			
} //Test TC_06close
	
} //main close