package com.test;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_42_FileUpload_MP4 {
	
	public WebDriver host;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC42";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vhostfirstName, vhostlastName, vHostEmail, vFilename_MP4;
		
	@Test //TC42 File Upload MP4
	public void TC42FileUploadMp4() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vHostEmail = d.HostEmail;
		vPswd = d.Pswd;	
		vFilename_MP4 = d.FileName_MP4;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Enter email id
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on share
				imeet_Room.click_share.click();
				Thread.sleep(2000);
			
				//Click on share a file
				imeet_Room.click_share_file.click();
				Thread.sleep(6000);
			
				//Click on Add new file
				imeet_Room.click_addnew_file.click();
				Thread.sleep(5000);
			
				//Enter file name. File located in Resources Package
				String mp4filename = vFilename_MP4.toLowerCase();
				StringSelection sel = new StringSelection(ReadData.getTestDataDirectory(mp4filename));
			
				//Copy to clipboard
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
			
				//Create object of Robot class
				Robot robot = new Robot();
				Thread.sleep(1000);
			      
				// Press Enter
				robot.keyPress(KeyEvent.VK_ENTER);
			 
				// Release Enter
				robot.keyRelease(KeyEvent.VK_ENTER);
			 
				// Press CTRL+V
				robot.keyPress(KeyEvent.VK_CONTROL);
				robot.keyPress(KeyEvent.VK_V);
			 
				// Release CTRL+V
				robot.keyRelease(KeyEvent.VK_CONTROL);
				robot.keyRelease(KeyEvent.VK_V);
				Thread.sleep(1000);
			        
				//Press Enter 
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
			
				//***************************
				// Wait until file load
				Thread.sleep(5000);
				String FileInRoom = imeet_Room.file_memory.getText();
				int FirstTimeFilenumber=0;
				String[] Part1, Part2;
				String File;
			
				boolean isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
				if(isFileNumberDisplay)
				{
					Part1 = FileInRoom.split(":");
					Part2 = Part1[1].split("\\(");
					File = Part2[0].trim();
					FirstTimeFilenumber = Integer.parseInt(File);	
				}

				int Filecount = FirstTimeFilenumber;
				int timer =1;
				while (timer < 60) // While loop wait up to 1 minute
				{
					Thread.sleep(1000);
					if(Filecount>FirstTimeFilenumber){break;}
					FileInRoom = imeet_Room.file_memory.getText();
					isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
					if(isFileNumberDisplay)
					{
						Part1 = FileInRoom.split(":");
						Part2 = Part1[1].split("\\(");
						File = Part2[0].trim();
						Filecount = Integer.parseInt(File);	
					}
					timer++;
				}
			
				//**************************
			
				//Verify newly uploaded MP4 file
				Thread.sleep(4000);
				String uploaded_file_name = imeet_Room.click_first_file.getText().toLowerCase();
				System.out.println("Uploaded file name " + uploaded_file_name + " where expected file name " + mp4filename);
				s_assert.assertEquals(uploaded_file_name, mp4filename, "Uploaded file name " + uploaded_file_name + " where expected file name " + mp4filename );

			}
			catch(Throwable ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Uploaded mp4 file name does not match");
				Reporter.log(ex.getMessage());
			}
		}
		catch (Throwable ex)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured. See the Reporter Log.");
			Reporter.log(ex.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_42 close
	
} //main close