package com.test;

import java.io.IOException;


import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC002_ValidEmailRequireTest 
{
	
	public static WebDriver Host;
	
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC02";
	
	TestData d = new TestData();
	String vUrl = d.Url;
	String vBrowserType = d.BrowserType;
	
	@BeforeMethod
	public void before_Test() throws IOException, Exception 
	{
	  ChooseBrowser Browser = new ChooseBrowser(Host);
	  Host = Browser.OpenBrowser(vBrowserType, vUrl);
	  Thread.sleep(1000);
	}
	 
	@AfterMethod
	public void afterTest() 
	{
	  if (Host != null) {Host.quit();}
	}
	
	@Test(dataProvider = "Emaildata") 	// TC002 Email Required
	public void TC002_LoginEmailReq(String Email) throws Exception{
		
		//Creating required objects
		iMeetLogin_Page login = new iMeetLogin_Page(Host);
		CommonFunctions common = new CommonFunctions();
		Thread.sleep(1000);
		login.switchFrame(Host);
		
		common.Host_LogIn_Option(Host, sPath, PicName);
							
		//Enter first name
		login.field_EnterFirstName.clear();
		login.field_EnterFirstName.sendKeys("TestUser");
					
		//Enter Last name
		login.field_EnterLastName.clear();
		login.field_EnterLastName.sendKeys("LastName");
						
		//Enter email id
		login.field_EnterEmail.clear();
		login.field_EnterEmail.sendKeys(Email);
		
		//Click on Join Meeting
		login.button_Submit.click();
		Thread.sleep(1000);
		
		boolean isEmailInvalid = !StringUtils.contains(Email, "@") && !StringUtils.contains(Email, ".com");
		try{
			String actualerroremail = login.text_ErrormsgForEmail.getText().trim();
			boolean isactualerroremail = actualerroremail.equalsIgnoreCase("Please enter a valid email");	
			if (isactualerroremail){ 	// Email Required validation
				Reporter.log("Pass. TC002 _ Valid Email required test Pass. AUT return '" + isactualerroremail + "' which is expected.");
			}else{
				Reporter.log("Fail. TC002 _ Valid Email required test fail. AUT return '" + isactualerroremail + "' which is not expected.");
				s_assert.fail();
			}
		} catch (Throwable t){
			Thread.sleep(2000);
			if(!isEmailInvalid)
			{
				Reporter.log("Pass. TC002 _ Valid Email required test Pass. Test Data provide valid Email as " + Email);
			}
			else
			{
				Reporter.log("Fail. TC002 _ Valid Email required test fail. Because catch report:  " + t);
				s_assert.fail();
			}
		}
		s_assert.assertAll();
	}
		
	@DataProvider(name="Emaildata")
		
	public Object[][] EmailData(){
		Object [][] Email= new Object[6][1];
		
		Email[0][0]="TestUser01.com";
		Email[1][0]="@gmail.com";
		Email[2][0]="Testemail";
		Email[3][0]=" ";
		Email[4][0]="Testemail@com";
		Email[5][0]="Testemail@pgi.com";

		return Email;
	}
	

} // Class Close
