package com.test;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_27_PublicChat {
	
	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC27";
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vguestfirstName, vguestlastName,vhostfirstName, vhostlastName, vHostEmail, vGuestEmail, vChatmessage;
	boolean isHostLogedIn = false;
		
	@Test //TC027 Public Chat 
	public void TC027_publicChat() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vHostEmail = d.HostEmail;
		vGuestEmail = d.UnregisteredGuestEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vChatmessage= d.ChatMessage.toUpperCase();
		
		try // 1st try
		{
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login.switchFrame(guest);
			common.Guest_LogIn_Option(guest, sPath, PicName);
					
			//Enter first name
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			
			common.Guest_Enter_Password(guest, vPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			Thread.sleep(1000);			
			//Login with Host
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(host);
			host = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(host);
			Thread.sleep(1000);
			login1.switchFrame(host);
			
			common.Host_LogIn_Option(host, sPath, PicName);
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Host
			login1.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				Thread.sleep(3000);
				//Click on chat button
				imeet_Room1.chat_button.click();
			 
				//Enter chat text
				imeet_Room1.chat_input_field.sendKeys(vChatmessage);
				imeet_Room1.chat_input_field.sendKeys(Keys.ENTER);
				Thread.sleep(3000);
				String appMessage = imeet_Room.received_chat.getText().toUpperCase();
				s_assert.assertEquals(appMessage, vChatmessage, "Expected Chat " + vChatmessage + " where actual chat message " + appMessage );
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Expected and actual chat message did not match");
			}
		} // 1st try close
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			
			if (host != null && guest != null)
			{
				guest.quit();
				Thread.sleep(500);
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_27 close
	
} //main close