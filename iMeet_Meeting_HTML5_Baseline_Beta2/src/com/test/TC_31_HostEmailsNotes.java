package com.test;

import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_31_HostEmailsNotes
{
	
	public WebDriver host;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC31";
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vhostfirstName, vhostlastName, vHostEmail,vEmailNote;
	boolean isHostLogedIn = false;
		
	@Test //TC31 Host email notes
	public void TC31HostEmailNotes() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vHostEmail = d.HostEmail;
		vPswd = d.Pswd;
		vEmailNote = d.EmailRoomNote;		
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
				
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
					
			//Enter email id
			login.field_EnterEmail.sendKeys(vHostEmail);
					
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			try
			{
			    //Click on notes 
				imeet_Room.notes_button.click();
				Thread.sleep(2000);
				//Enter notes
				imeet_Room.mynote_input_field.sendKeys("Email note Test");
				Thread.sleep(2000);
				//Click Enter from keyboard
				imeet_Room.mynote_input_field.sendKeys(Keys.ENTER);
				Thread.sleep(2000);
				//Click on Notes Menu item
				imeet_Room.notes_menu_item.click();
				Thread.sleep(2000);
				//Enter Email id for note users.
				imeet_Room.notes_menu_item_email.click();
				Thread.sleep(2000);
				//Click on Add User
				imeet_Room.click_add_note_user.click();
				Thread.sleep(2000);
				//Enter email id
				imeet_Room.input_note_user_email.sendKeys(vEmailNote);
				Thread.sleep(2000);
				//Click on Done
				imeet_Room.button_Done_note.click();
				Thread.sleep(2000);
			}
			catch(Throwable ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Note is not sent as email.");
				Reporter.log(ex.getMessage());	
			}
		}
		catch (Throwable ex)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			Reporter.log(ex.getMessage());	
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}

		s_assert.assertAll();
			
	} //Test TC_31 close
	
} //main close