package com.test;

import java.io.IOException;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;
public class TC_30_PrivateNotes {
	
	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC30";
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName,vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, vNoteMessage, vNoteMessagehost,vNoteMessageguest ;
	boolean isHostLogedIn = false;

	@Test //TC030 Private Notes 
	public void TC030_privateNotes() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vguestfirstName= d.GuestFirstName;
		vguestlastName= d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		vNoteMessage = d.NoteMessage;
		vNoteMessagehost = vNoteMessage + "host";
		vNoteMessageguest = vNoteMessage + "guest";
		
		try
		{
			//Choose Browser and open for the guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
		
			//Enter first name
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			//Login with Host
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(host);
			host = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login1.switchFrame(host);
			//Click join
			common.Host_LogIn_Option(host, sPath, PicName);
					
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Host
			login1.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);	
			try
			{
				//Click on Note option for the host
				imeet_Room1.notes_button.click();
				Thread.sleep(2000);
			
				//Click on My note
				imeet_Room1.my_notes.click();
				Thread.sleep(2000);
			
				//Enter note text
				imeet_Room1.mynote_input_field.sendKeys(vNoteMessagehost);
				imeet_Room1.mynote_input_field.sendKeys(Keys.ENTER);
				Thread.sleep(3000);
			
				//Click on Note option for the guest
				imeet_Room.notes_button.click();
				Thread.sleep(2000);
			
				//Click on My note Guest
				imeet_Room.my_notes.click();
				Thread.sleep(2000);
			
				//Enter note text Guest
				imeet_Room.mynote_input_field.sendKeys(vNoteMessageguest);
				imeet_Room.mynote_input_field.sendKeys(Keys.ENTER);
				Thread.sleep(3000);
			}
			catch(Exception ex)
			{
				Reporter.log(ex.getMessage());	
			}
			
			try
			{
				//Verify private host note message is not shown on guest screen.
				Thread.sleep(3000);
				String appNoteMessageguest = imeet_Room.received_note.getText();
				s_assert.assertNotEquals(appNoteMessageguest, vNoteMessagehost, "Expected and actual note message should not match");
			
				//Verify private guest note message is not shown on host screen.
				Thread.sleep(3000);
				String appNoteMessagehost = imeet_Room1.received_note.getText();
				s_assert.assertNotEquals(appNoteMessagehost, vNoteMessageguest, "Expected and actual note message should not match");
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Expected and actual note message matched", t);
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
		}
		finally
		{
			ChooseBrowser cbhost = new ChooseBrowser(host);
			cbhost.SignOutFromMeetingRoom(host);
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_30 close
	
} //main close