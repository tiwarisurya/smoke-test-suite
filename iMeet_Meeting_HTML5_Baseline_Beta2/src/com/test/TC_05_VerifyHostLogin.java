package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class TC_05_VerifyHostLogin {
 
	
	public WebDriver host ;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC05";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String vhostfirstName, vhostlastName, vHostEmail;
		
	@Test //TC005_VerifyHostLogin
	public void TC005_verifyHostLogin() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vHostEmail = d.HostEmail;
		vPswd = d.Pswd;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
			login.switchFrame(host);
			
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
					
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
				
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
					
			//Enter email id
			login.field_EnterEmail.sendKeys(vHostEmail);
					
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(500);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			try
			{			
				//Verify Invite option for the Host
				s_assert.assertTrue(imeet_Room.invite.isDisplayed(), "Invite option not shown for the Host.");
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName +common.CurrentTime()+".jpg");
				s_assert.fail("Invite option not shown for the Host.");
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error occured. plz check the report log");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cb = new ChooseBrowser(host);
				cb.SignOutFromMeetingRoom(host);				
			}
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_05 close
	
} //main close