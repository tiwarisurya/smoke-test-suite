package com.test;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class TC_07_LoginRegisteredGuest{
	
	public WebDriver guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC07";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vfirstName, vlastName, vRegisteredGuestEmail;
		
	@Test //TC007 Verify--Login - Registered Guest- Continue As A Guest Link
	public void TC007_loginRegisteredGuest() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vfirstName = d.GuestFirstName;
		vlastName = d.GuestLastName;
		vRegisteredGuestEmail = d.RegisteredGuestEmail;
		
		try
		{
			//Choose Browser and open for the registered guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
			login.switchFrame(guest);
			 
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vlastName);
				
			//Enter email id
			login.field_EnterEmail.sendKeys(vRegisteredGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			//Click on Continue as guest
			login.link_ContinueAsGuest.click();
			
			isHostLogedIn = common.Host_Audio_Panel_Display(guest, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Control menu
				imeet_Room.control_menu.click();
			
				//Click on Settings
				imeet_Room.settings.click();
			
				//Verify dialing tab is not shown for the registered guest
				//s_assert.assertFalse(imeet_Room.setting_dialing.isDisplayed(), "Dialing tab is shown for the registered guest");
			
				// Guest Login function not supported message verification 
				String GuestLogInMessage = imeet_Room.continueAs_guest_message.getText();
				boolean GuestLogInNotSupport = StringUtils.containsIgnoreCase(GuestLogInMessage, "This function is not supported");
				s_assert.assertTrue(GuestLogInNotSupport, "Continue As Guest test failed.");
				imeet_Room.button_ok.click();
				Thread.sleep(1000);
			}
			catch(Throwable t)
			{
				sshot.ScrShot(guest, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail(" Continue As Guest test failed.");
				Reporter.log(" Continue As Guest test failed." + t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(guest, sPath, PicName+".jpg");
			s_assert.fail("Error Occured.");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cb = new ChooseBrowser(guest);
				cb.SignOutFromMeetingRoom(guest);
			}
			
			if (guest != null)
			{
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_007close
	
} //main close