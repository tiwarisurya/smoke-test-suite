package com.test;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_37_HostPassControlGuestRequest {
	
	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC37";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName,vguestfirstName, vguestlastName, vHostEmail,vGuestEmail;
		
	@Test //TC37 Host Pass Control - Guest Request
	public void TC37_hostPassControlguestRequest() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);	
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vGuestPswd = d.GuestPswd;
		vHostEmail = d.HostEmail;
		vGuestEmail = d.RegisteredGuestEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		
		try
		{
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
					
			//Enter first name
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(2000);
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			//Verify Audio Connect panel
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			//Login with Host
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(host);
			host = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login1.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Host
			login1.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on share
				Thread.sleep(1000);
				imeet_Room1.click_share.click();
				Thread.sleep(2000);
			
				//Click on share a file
				imeet_Room1.click_share_file.click();
				Thread.sleep(6000);
			
				//Select a file and 
				//imeet_Room1.click_first_file.click();
				int counter = 0;
				String xp1 = "//*[@id='";
				String xp2 = "']/div[1]";
				WebElement fileList = host.findElement(By.xpath(xp1+counter+xp2));
				while(true)
				{
					fileList = host.findElement(By.xpath(xp1+counter+xp2));
					String fileName = fileList.getText();
					boolean isVideoFile = StringUtils.containsIgnoreCase(fileName, "mp4");
					if(!isVideoFile || counter >= 4){break;}
					counter++;
				}
				
				
				fileList.click();
				Thread.sleep(2000);
				
				//Click on Present
				imeet_Room1.click_present.click();
				Thread.sleep(5000);
				
				//Guest clicks on Pass control 
				imeet_Room.click_pass_control.click();
				Thread.sleep(3000);
			
				//Guest clicks on Request Control
				imeet_Room.click_request_control.click();
				Thread.sleep(3000);
				//Host clicks on Grant
				imeet_Room1.click_grant.click();
				Thread.sleep(3000);
				
				// Validation for pass control
				String control_confirmation = imeet_Room.confirm_control_accepted.getText();
				boolean isControlConfirmed = StringUtils.containsIgnoreCase(control_confirmation,"control" );
				s_assert.assertTrue(isControlConfirmed, "Fail. Request Control not accepted by host.");
				Thread.sleep(3000);
				//Guest click on OK for confirmation on guest window
				imeet_Room.ok_button_passcontrol.click();	
			}
			catch(Throwable ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Control is not requested from guest to host");
				Reporter.log(ex.getMessage());
			}
		}
		catch (Throwable ex)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured. See the Reporter Log.");
			Reporter.log(ex.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				guest.quit();
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_37 close
	
} //main close