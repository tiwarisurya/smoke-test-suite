package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC001_First_LastNameRequireTest 
{
	public static WebDriver Host;
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC01";
	
	TestData d = new TestData();
	String vUrl = d.Url;
	String vBrowserType = d.BrowserType;

	@BeforeMethod
	public void before_Test() throws IOException, Exception 
	{
	  ChooseBrowser Browser = new ChooseBrowser(Host);
	  Host = Browser.OpenBrowser(vBrowserType, vUrl);
	  Thread.sleep(1000);
	}
	 
	@AfterMethod
	public void afterTest() 
	{	if (Host != null) {Host.quit();}}
	
	 @Test(dataProvider = "FnLNameReq")  //TC001 First and last name Required
	 
	 public void TC001_LogInFirstLastNameReq(String FirstName, String LastName, String Email) throws Exception
	 {
		 
		//Creating required objects
		 iMeetLogin_Page login = new iMeetLogin_Page(Host);
		 
		 CommonFunctions common = new CommonFunctions();
		 Thread.sleep(1000);
		 login.switchFrame(Host);
		 common.Host_LogIn_Option(Host, sPath, PicName);
		 Thread.sleep(2000);
		 
		 boolean isFirsNameDisplayed = login.field_EnterFirstName.isDisplayed();
		 if(isFirsNameDisplayed)
		 {
			//Enter first name
			 login.field_EnterFirstName.clear();
			 login.field_EnterFirstName.sendKeys(FirstName);
			 //Enter Last name
			 login.field_EnterLastName.clear();
			 login.field_EnterLastName.sendKeys(LastName);
			 //Enter email id
			 login.field_EnterEmail.clear();
			 login.field_EnterEmail.sendKeys(Email);
			 //Click on Join Meeting
			 login.button_Submit.click();
		 }
		 else
		 {
			 System.out.println("Skipping the TC001 test ");
			 throw new SkipException("Testing skip. First name field doesn't displayed");
		 }
		  
		 boolean isFirstLastNameEmpty = (FirstName.isEmpty() || FirstName == null) && (LastName.isEmpty() || LastName == null);
		 try
		 {
			 boolean isErrorMsgDisplay = login.text_ErrormsgForName.isDisplayed();
		 	 String actualerror = login.text_ErrormsgForName.getText().trim();
		  	 boolean isactualerror = actualerror.equalsIgnoreCase("Enter a valid name.");	
			 if (isErrorMsgDisplay && isactualerror){ 	// First and Last Name Required validation
					Reporter.log("Pass. TC001 _ First and last name required test Pass. AUT return '" + actualerror + "' which is expected.");
			 }else if(!isFirstLastNameEmpty && !isErrorMsgDisplay ){
				 Reporter.log("Pass. TC001 _ First and last name required test Pass. Coz First and Last name value " + FirstName + " and " + LastName + " Provided");
			 }else{
				Reporter.log("Fail. TC001 _ First and last name required test fail. AUT return '" + actualerror + "' which is not expected.");
				s_assert.fail();
			 }
			 
		  } catch (Throwable t)
		  {
			  Thread.sleep(3000);
			  if(!isFirstLastNameEmpty)
			  {
				  Reporter.log("Pass. TC001 _ First and last name required test Pass. Test Data provide First and Last name as " + FirstName + " - " + LastName);
			  }
			  else
			  {
					Reporter.log("Fail. TC001 _ First and last name required test fail. Because catch report:  " + t);
					s_assert.fail();
				}
			}
		 
		 s_assert.assertAll();
		}
		
		@DataProvider(name="FnLNameReq")
		
		public Object[][] FnLNameData(){
			
			Object [][] FnLName= new Object[4][3];
		
			FnLName[0][0]="TestUser 01"; // Enter data to row 0 column 0 for First Name
			FnLName[0][1]=""; // Enter data to row 0 column 1. No Last name
			FnLName[0][2]="validemail@gmail.com"; 
			
			FnLName[1][0]=""; // Enter data to row 1 column 0. No First name
			FnLName[1][1]="TestUser 02"; // Enter data to row 1 column 1 for Last Name
			FnLName[1][2]="validemail@gmail.com";  
			
			FnLName[2][0]=""; // Enter data to row 2 column 0 No First Name
			FnLName[2][1]=""; // Enter data to row 2 column 1. No last Name
			FnLName[2][2]="validemail@gmail.com";
			
			FnLName[3][0]="My First Name"; // Enter data to row 2 column 0 No First Name
			FnLName[3][1]="Last Name"; // Enter data to row 2 column 1. No last Name
			FnLName[3][2]="validemail@gmail.com"; 
			
			return FnLName;
		}
	

} // Class Close
