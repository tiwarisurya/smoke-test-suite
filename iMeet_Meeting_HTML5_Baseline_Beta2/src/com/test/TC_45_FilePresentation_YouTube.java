package com.test;

import java.io.IOException;



import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_45_FilePresentation_YouTube {
		
	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC45";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName,vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, vYoutubeSearchTerm, vChatmessage, vFilename_MP4;
		
	@Test //TC45 File Presentation youtube 
	public void TC45_filepresentationYoutube() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vGuestPswd = d.GuestPswd;
		vHostEmail = d.HostEmail;
		vGuestEmail = d.UnregisteredGuestEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vChatmessage= d.ChatMessage;
		vYoutubeSearchTerm = d.YouTubeVideoAdd;
		
		try
		{
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login.switchFrame(guest);
			//Click on Join 
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			//Verify Audio Connect panel
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
						
			//Login with Host
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(host);
			host = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login1.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
				
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Host
			login1.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on share
				imeet_Room1.click_share.click();
			
				//Click on share a file
				Thread.sleep(3000);
				imeet_Room1.click_share_file.click();
				
				//Click on Add Video
				Thread.sleep(6000);
				imeet_Room1.click_addnew_video.click();
				
				//Search for a video using keyword imeet
				Thread.sleep(6000);
				imeet_Room1.input_field_videosearch.sendKeys(vYoutubeSearchTerm);
				
				//Click on Search button
				Thread.sleep(2000);
				imeet_Room1.search_button.click();
				
				Thread.sleep(4000);
				String VideoTitle = imeet_Room1.search_result_video.getText().trim().toUpperCase();
				Thread.sleep(2000);
				//Click on Add video
				imeet_Room1.search_result_video.click();
				//Verify that Video added 
				Thread.sleep(3000);
				String uploaded_file_name = imeet_Room1.video_Added_Msg.getText();
				Thread.sleep(3000);
				if(uploaded_file_name.equalsIgnoreCase("Video Added"))
				{
					//Click on Done
					imeet_Room1.click_done.click();
					Thread.sleep(2000);
				}
				else
				{
					System.out.println(VideoTitle + " Video from youtube not added to file cabinet.");
					sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
					s_assert.fail(VideoTitle + " Video from youtube not added to file cabinet.");
					Reporter.log(VideoTitle + " Video from youtube not added to file cabinet.");
					throw new SkipException(TestCaseName + " Test Case Skipping. Because"+ VideoTitle + " Youtube video not added to file cabinet.");
				}
				// Get the First file name in the cabinate
				vFilename_MP4 = imeet_Room1.click_first_file.getText().toUpperCase();
				//Select a file and 
				Thread.sleep(3000);
				imeet_Room1.click_first_file.click();

				//Click on Present
				Thread.sleep(3000);
				imeet_Room1.click_present.click();
			
				//Verify presented file on guest screen
				Thread.sleep(5000);
				String presented_file_name = imeet_Room.presented_file_name.getText(); // Guest checking the file name
				String uppercasestring = presented_file_name.toUpperCase();
				System.out.println("Presented file name " + uppercasestring + " where expected mp4 file is " + vFilename_MP4);
				Thread.sleep(5000);
				s_assert.assertEquals(uppercasestring, vFilename_MP4, "Presented file name " + uppercasestring + " where expected file is " + vFilename_MP4);	
	
			}
			catch(Throwable ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error Occured for Presenting You Tube file. Please check reporter log.");
				Reporter.log(ex.getMessage());
			}
		}
		catch (Throwable ex)
		{
		
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Please check reporter log.");
			Reporter.log(ex.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);				
			}
			Thread.sleep(1000);
			
			if (host != null && guest != null)
			{
				guest.quit();
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_43 close
	
} //main close