package com.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_117_CubeStepAway{
	
	public WebDriver guest;
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC117";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vguestfirstName, vguestlastName,vhostfirstName, vhostlastName, vHostEmail,vGuestEmail;
		
	@Test //TC117 Cube - Step Away
	public void TC_117cubeStepAway() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.UnregisteredGuestEmail;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowserhost = new ChooseBrowser(host);
			host = chooseBrowserhost.OpenBrowser(vBrowserType, vUrl);
				
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
				
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(2000);
			login.switchFrame(host);
			common.Host_LogIn_Option(host, sPath, PicName);
							
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
					
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
					
			//Login with Host
			login.field_EnterEmail.sendKeys(vHostEmail);
						
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			//Login with Guest
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
				
			login1.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Guest_Enter_Password(guest, vPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			try
			{
				imeet_Room.participants_button.click();
			
				Thread.sleep(1000);
				String attrbutenamehost = host.findElement(By.xpath("//span[contains(.,'"+vhostfirstName.toUpperCase()+" "+vhostlastName.toUpperCase()+"')]")).getAttribute("id");
				String[] partshost = attrbutenamehost.split("r");
				String attributIDhost = partshost[1]; // 4644292
				
				imeet_Room.participants_button.click();
			
				Thread.sleep(2000);
				WebElement elghost = host.findElement(By.xpath("//a[@id='cubeFrontStepAwayIcon"+attributIDhost+"'"+" and @title='Step Away']"));
				Thread.sleep(2000);
				Actions actionhost = new Actions(host);
				WebElement cube = host.findElement(By.xpath("//img[@id='avatarImage"+attributIDhost+"']"));
				actionhost.moveToElement(cube).build().perform();
				Thread.sleep(2000);
				elghost.click(); // Host click stay away
				Thread.sleep(3000);

				imeet_Room1.participants_button.click();
			
				Thread.sleep(1000);
				String attrbutenameguest = guest.findElement(By.xpath("//span[contains(.,'"+vguestfirstName.toUpperCase()+" "+vguestlastName.toUpperCase()+"')]")).getAttribute("id");
				String[] partsguest = attrbutenameguest.split("r");
				String attributIDguest = partsguest[1]; // 4644292
		
				imeet_Room1.participants_button.click();
				Thread.sleep(2000);
				
				WebElement elgguest = guest.findElement(By.xpath("//a[@id='cubeFrontStepAwayIcon"+attributIDguest+"'"+" and @title='Step Away']"));
				Thread.sleep(2000);
				Actions actionguest = new Actions(guest);
				WebElement cubeguest = guest.findElement(By.xpath("//img[@id='avatarImage"+attributIDguest+"']"));
				actionguest.moveToElement(cubeguest).build().perform();
				Thread.sleep(2000);
				elgguest.click(); // Guest click stay away
				Thread.sleep(3000);
			
				//Guest Verify host stay away
				Thread.sleep(2000);
				WebElement stepawaycubehost = guest.findElement(By.xpath("//div[contains(@id,'stepAway"+attributIDhost+"')]"));
				//imeet_Room.waitForElement(host, stepawaycubehost, 4);
			
				boolean ishostStepAwayDisplay = stepawaycubehost.isDisplayed();
				s_assert.assertTrue(ishostStepAwayDisplay, "Fail. Host Step away, but not displayed on guest cube.");
			
				//Host Verify guest step away
				Thread.sleep(2000);
				WebElement stepawaycubeguest = host.findElement(By.xpath("//div[contains(@id,'stepAway"+attributIDguest+"')]"));
				//imeet_Room.waitForElement(guest, stepawaycubeguest, 4);
			
				boolean isguestStepAwayDisplay = stepawaycubeguest.isDisplayed();
				s_assert.assertTrue(isguestStepAwayDisplay, "Fail. Guest Step away, but not displayed on guest cube.");
			}
			catch(Throwable t)
			{
				sshot.ScrShot(guest, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Step away icon not displayed on cube for Guest and Host. Error " + t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured ." + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			if (guest != null && host != null)
			{
				guest.quit();
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_117 close
	
} //main close