package com.test;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_121_AddtoContact {
	
	public WebDriver guest;
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC121";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vguestfirstName, vguestlastName,vhostfirstName, vhostlastName, vHostEmail,vGuestEmail;

		
	@Test //TC121 Add to contact
	public void TC_121addtoContact() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.UnregisteredGuestEmail;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowserhost = new ChooseBrowser(host);
			host = chooseBrowserhost.OpenBrowser(vBrowserType, vUrl);
				
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
				
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(2000);
			login.switchFrame(host);
				
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
						
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
					
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
						
			//Login with Host
			login.field_EnterEmail.sendKeys(vHostEmail);
						
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
		
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
				
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
				
			login1.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			
			common.Guest_Enter_Password(guest, vPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
		
			try
			{
				//Store User Id into string 
				imeet_Room.participants_button.click();
				Thread.sleep(1000);
				WebElement guest_name = host.findElement(By.xpath("//span[contains(.,'"+vguestfirstName.toUpperCase()+" "+vguestlastName.toUpperCase()+"')]"));
				String attrbutename = guest_name.getAttribute("id");
				String[] parts = attrbutename.split("r");
				String attributID = parts[1]; // 4644292
				
				Thread.sleep(2000);
				guest_name.click();
				Thread.sleep(2000);
				try
				{
					//Click on Add to user
					WebElement addTocontact = host.findElement(By.xpath("//a[contains(@id,'panelAddToContactId"+attributID+"')]"));
					boolean AddToContactButtonDisplay = addTocontact.isDisplayed();
					if(AddToContactButtonDisplay)
					{
						addTocontact.click();
						//Verify that host is added as contact   
						Thread.sleep(2000);
						boolean isAddToContactDisplay = imeet_Room.Addto_contact_confirmation.isDisplayed();
						if(isAddToContactDisplay)
						{
							String AddToContactText = imeet_Room.Addto_contact_confirmation.getText();
							boolean AddNewContactConfirmation = StringUtils.containsIgnoreCase(AddToContactText, "has been added to your contacts");
							s_assert.assertTrue(AddNewContactConfirmation, "User not added to contact list.");
						}
						else
						{
							s_assert.fail("After clicking Add to contact, didn't receive any confirmation.");
						}
					}
					else
					{
						System.out.println("Add to Contact button not displayed. User already added to contact.");
					}
				}
				catch(Exception ex)
				{
					System.out.println("Exception Detail to add to contact " + ex.getMessage());
				}
			}
			catch(Throwable t)
			{
				sshot.ScrShot(guest, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("User not added");
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			if (guest != null && host != null)
			{
				guest.quit();
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_121 close
	
} //main close