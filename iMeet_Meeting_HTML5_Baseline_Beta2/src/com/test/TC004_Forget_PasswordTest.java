package com.test;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC004_Forget_PasswordTest {
	
	public static WebDriver Host;
	
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC04";
	
	TestData d = new TestData();
	String vUrl = d.Url;
	String vBrowserType = d.BrowserType;
		
	@BeforeMethod
	public void before_Test() throws IOException, Exception 
	{
	  ChooseBrowser Browser = new ChooseBrowser(Host);
	  Host = Browser.OpenBrowser(vBrowserType, vUrl);
	  Thread.sleep(1000);
	}
	 
	@AfterMethod
	public void afterTest() throws Exception 
	{
		if (Host != null) {Host.quit();}
		Thread.sleep(2000);
	}
	
	
	@Test(dataProvider = "ForgetPswd") // TC004 forget password
	public void TC004_LoginForgotPassword(String Email, String pswdEmail) throws Exception
	{
		//Creating required objects
		iMeetLogin_Page login = new iMeetLogin_Page(Host);
		CommonFunctions common = new CommonFunctions();
		Thread.sleep(1000);
		login.switchFrame(Host);
		
		common.Host_LogIn_Option(Host, sPath, PicName);

		//Enter first name
		login.field_EnterFirstName.sendKeys("Registered");
				
		//Enter Last name
		login.field_EnterLastName.sendKeys("User");
					
		//Enter email id
		login.field_EnterEmail.sendKeys(Email);
						
		//Click on Join Meeting
		login.button_Submit.click();
		Thread.sleep(1000);
		
		Set <String> windowids = Host.getWindowHandles();
		Iterator<String> it = windowids.iterator();
		
		try
		{
			boolean isForgetPswdLinkDisplayed = login.link_ForgotPassword.isDisplayed();
			if(isForgetPswdLinkDisplayed)
			{
				//Click on forgot password link.
				login.link_ForgotPassword.click();
				Thread.sleep(2000);
				windowids = Host.getWindowHandles();
				it = windowids.iterator();
				String mainId = it.next();
				String childId = it.next();
				Host.switchTo().window(childId);
				Thread.sleep(1000);
				String Child_Url = Host.getCurrentUrl();
				boolean isForgetPasswordUrl = Child_Url.contains("forgot_password") && login.text_forgetyourPassword.isDisplayed();
				if(isForgetPasswordUrl)
				{
					Reporter.log("Pass. TC004. Forgot Password link navigate to right page");
					// forget password page validation Code and //Send email to reset forgotten password
					login.field_emailaddress.sendKeys(pswdEmail); // email field
					login.link_emailmypassword.click(); // email my pswd
					Thread.sleep(1000);
					
					boolean isEmailPasswordEmpty = pswdEmail.length()<1;
					boolean isvalidEmail = (pswdEmail.contains("@") && pswdEmail.contains(".com"));
					if(isEmailPasswordEmpty){
						try
						{
							String EmailFieldRequired = login.text_entervalidemail.getText().trim();
							boolean isEmailRequired= StringUtils.containsIgnoreCase(EmailFieldRequired, "This field is required");
							if(isEmailRequired)
							{
								Reporter.log("Pass. TC004 - Forget Password. AUT displayed " + login.text_entervalidemail.getText());
							}
							else
							{
								Reporter.log("Fail. TC004 - Forget Password. AUT displayed wrong message as  " + login.text_entervalidemail.getText());
								s_assert.fail();
							}
							
						}
						catch(Throwable t)
						{
							Reporter.log("Fail. TC004 - Forget Password fail coz User don't provide any email address. AUT Error " + t.getMessage());
							s_assert.fail();
						}
						
					} else if(!isEmailPasswordEmpty && !isvalidEmail){
						try {
							String InvalidEmail = login.text_entervalidemail.getText().trim();
							boolean isEmailNotvalid= StringUtils.containsIgnoreCase(InvalidEmail, "Please enter a valid email address");
							if(isEmailNotvalid)
							{
								Reporter.log("Pass. TC004 - Forget Password. AUT displayed " + login.text_entervalidemail.getText());
							}
							else
							{
								Reporter.log("Fail. TC004 - Forget Password. AUT displayed wrong message as  " + login.text_entervalidemail.getText());
								s_assert.fail();
							}
								
						}
						catch(Throwable t)
						{
							Reporter.log("Fail. TC004 - Forget Password Coz User provide invalid email. AUT Error " + t.getMessage());
							s_assert.fail();
						}					
					} else {
						boolean tryController = false;
						try
						{
							String PswdOnTheWay = login.text_passwordontheway.getText().trim();
							boolean isPswdOnTheWay= StringUtils.containsIgnoreCase(PswdOnTheWay, "Password on the Way");
							if(isPswdOnTheWay)
							{
								Reporter.log("Pass. TC004 - Forget Password. AUT displayed " + login.text_passwordontheway.getText());
							}
							else
							{
								Reporter.log("Fail. TC004 - Forget Password. AUT displayed wrong message as  " + login.text_passwordontheway.getText());
								s_assert.fail();
							}
						}
						catch(Throwable t)
						{
							tryController = true;	
						}
						if(tryController)
						{
							try
							{
								String UserNotFound = login.text_couldntfinduser.getText().trim();
								boolean isUserNotFound= StringUtils.containsIgnoreCase(UserNotFound, "Could not find user with email");
								if(isUserNotFound)
								{
									Reporter.log("Pass. TC004 - Forget Password. AUT displayed " + login.text_couldntfinduser.getText());
								}
								else
								{
									Reporter.log("Fail. TC004 - Forget Password. AUT displayed wrong message as  " + login.text_couldntfinduser.getText());
									s_assert.fail();
								}
							}
							catch(Throwable t)
							{
								System.out.println("Error Occured. Check the forget Password flow manually.");
								Reporter.log("Fail. TC004 - Forget Password Error Occured. Check the forget Password flow manually. ");
								s_assert.fail();
							}	
						}	
					}
						
				}
				else
				{
					Reporter.log("Fail. TC004. Forgot Password link navigate to wrong page");
					s_assert.fail();
				}
					
				Thread.sleep(1000);
				Host.close();
				Host.switchTo().window(mainId);
				Thread.sleep(1000);
				}
				else // If Forget Password Link not displayed
				{
				Reporter.log("Pass " + Email + " is an unregistered user.");
				}
				}
				catch(Throwable t)
				{
				Reporter.log("Warning   " + Email + " is an unregistered user");
				}

				s_assert.assertAll();
				}
	
	/*
 	************************************************************************************************************
 					boolean tryController = false;
					try
					{
						
						String PswdOnTheWay = login.text_passwordontheway.getText().trim();
						boolean isPswdOnTheWay= StringUtils.containsIgnoreCase(PswdOnTheWay, "Password on the Way");
						if(isPswdOnTheWay)
						{
							Reporter.log("Pass. TC004 - Forget Password. AUT displayed " + login.text_passwordontheway.getText());
						}
						else
						{
							Reporter.log("Fail. TC004 - Forget Password. AUT displayed wrong message as  " + login.text_passwordontheway.getText());
							s_assert.fail();
						}
					}
					catch(Throwable t)
					{
						tryController = true;	
					}
					
					if(tryController) // No Else Statement for this block
					{
						try
						{
							String UserNotFound = login.text_couldntfinduser.getText().trim();
							boolean isUserNotFound= StringUtils.containsIgnoreCase(UserNotFound, "Could not find user with email");
							if(isUserNotFound)
							{
								Reporter.log("Pass. TC004 - Forget Password. AUT displayed " + login.text_couldntfinduser.getText());
							}
							else
							{
								Reporter.log("Fail. TC004 - Forget Password. AUT displayed wrong message as  " + login.text_couldntfinduser.getText());
								s_assert.fail();
							}
						}
						catch(Throwable t)
						{
							System.out.println("Error Occured. Check the forget Password flow manually.");
							Reporter.log("Fail. TC004 - Forget Password Error Occured. Check the forget Password flow manually. ");
							s_assert.fail();
						}	
					}			
				}
				else
				{
					Reporter.log("Fail. TC004. Forgot Password link navigate to wrong page");
					s_assert.fail();
				}
					
				Thread.sleep(1000);
				Host.close();
				Host.switchTo().window(mainId);
				Thread.sleep(1000);
			}
			else // If Forget Password Link not displayed
			{
				Reporter.log("Pass " + Email + " is an unregistered user.");
			}
		}
		catch(Throwable t)
		{
			Reporter.log("Warning   " + Email + " is an unregistered user");
		}
		
		s_assert.assertAll();
	}
	
	****************************************************************************************************************************
	*/
	  

	@DataProvider(name="ForgetPswd")
	
	public Object[][] ForgetPswdUser(){

		Object [][] RegData= new Object[5][2];
		
		/*RegData[0][0]="mahmudulhasanbeta2@pgi.com";
		RegData[0][1]=""; // no email
		RegData[1][0]="testaccount01@pgi.com";
		RegData[1][1]="testaccount.com"; // invalid email
		RegData[2][0]="testaccount02@pgi.com";
		RegData[2][1]="dummyemail@mailinator.com";
		RegData[3][0]="mahmudulhasanbeta2@pgi.com";
		RegData[3][1]="mahmudulhasanbeta2@pgi.com";
		RegData[4][0]="dummyemail@gmail.com";
		RegData[4][1]="dummyemailuser@gmail.com";
		return RegData;*/
		
		RegData[0][0]="mahmudul.hasan@pgi.com";
		RegData[0][1]=""; // no email
		RegData[1][0]="mahmudul.hasan@pgi.com";
		RegData[1][1]="mahmudul.hasan@com"; // invalid email
		RegData[2][0]="mahmudul.hasan@pgi.com";
		RegData[2][1]="mahmudulhasanbeta22@pgi.com";
		RegData[3][0]="mahmudul.hasan@pgi.com";
		RegData[3][1]="mahmudulhasanbeta2@pgi.com";
		RegData[4][0]="dummyemail@gmail.com";
		RegData[4][1]="dummyemailuser@gmail.com";
		return RegData;
	}
	

} // Class Close
