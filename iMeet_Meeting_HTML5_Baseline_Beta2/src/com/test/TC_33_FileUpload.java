package com.test;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.Screen;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_33_FileUpload {
	
	public WebDriver host;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC33";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vhostfirstName, vhostlastName, vHostEmail, vFileUpload;
		
	@Test //TC33 File Upload
	public void TC33FileUpload() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vHostEmail = d.HostEmail;
		vPswd = d.Pswd;
		vFileUpload = d.FileUpload;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Enter email id
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(500);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);		
			try
			{
				//Click on share
				imeet_Room.click_share.click();
				Thread.sleep(3000);
			
				//Click on share a file
				imeet_Room.click_share_file.click();
				Thread.sleep(6000);
				
				//Click on Add new file
				imeet_Room.click_addnew_file.click();
				Thread.sleep(4000);
				
				//Enter file path
				String FileUpload = vFileUpload.toLowerCase();
				Screen screen = new Screen();
				Thread.sleep(1000);
				System.out.println("Going to type");
				screen.type(ReadData.getTestDataDirectory("EnterFilePath.JPG"), ReadData.getTestDataDirectory(FileUpload));
				screen.click(screen.wait(ReadData.getTestDataDirectory("Open.JPG"), 10));
				
				//***************************
				// Wait until file load
				Thread.sleep(5000);
				String FileInRoom = imeet_Room.file_memory.getText();
				int FirstTimeFilenumber=0;
				String[] Part1, Part2;
				String File;
				
				boolean isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
				if(isFileNumberDisplay)
				{
					Part1 = FileInRoom.split(":");
					Part2 = Part1[1].split("\\(");
					File = Part2[0].trim();
					FirstTimeFilenumber = Integer.parseInt(File);	
				}

				int Filecount = FirstTimeFilenumber;
				int timer =1;
				while (timer < 60) // While loop wait up to 1 minute
				{
			    	Thread.sleep(1000);
			    	if(Filecount>FirstTimeFilenumber){break;}
			    	FileInRoom = imeet_Room.file_memory.getText();
			    	isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
					if(isFileNumberDisplay)
					{
						Part1 = FileInRoom.split(":");
						Part2 = Part1[1].split("\\(");
						File = Part2[0].trim();
						Filecount = Integer.parseInt(File);	
					}
					timer++;
				}
				
				//**************************
			
				//Verify newly uploaded file
				Thread.sleep(5000);
				String uploaded_file_name = imeet_Room.click_first_file.getText().toLowerCase();
				s_assert.assertEquals(uploaded_file_name, FileUpload, "Uploaded file name " + uploaded_file_name + " Where expected file Name " + FileUpload );	
				System.out.println("Uploaded file name " + uploaded_file_name + " Where expected file Name " + FileUpload );
			}
			catch(Throwable ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail( "Share file option not displayed for host." + PicName+" upload file" );
				Reporter.log(ex.getMessage());
			}
		}
		catch (Throwable ex)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			Reporter.log(ex.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_33 close
	
} //main close