package com.test;

import java.io.IOException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_29_PublicNotes {

	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC29";
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vguestfirstName, vguestlastName,vhostfirstName, vhostlastName, vHostEmail,vGuestEmail, vNoteMessage;
	boolean isHostLogedIn = false;
		
	@Test //TC29 Public Notes
	public void TC_29publicNotes() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		vNoteMessage = d.NoteMessage;
		
		try
		{
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login.switchFrame(guest);
			
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
					
			//Enter first name
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			//Login with Host
			//Open browser window for host
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(host);
			host = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login1.switchFrame(host);
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Host
			login1.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
		
			try{
				//Click on Note option for the guest
				imeet_Room1.notes_button.click();
			 
				//Enter note text
				Thread.sleep(3000);
				imeet_Room1.note_input_field.sendKeys(vNoteMessage);
				Thread.sleep(2000);
				imeet_Room1.note_input_field.sendKeys(Keys.ENTER);
				Thread.sleep(2000);
				//Verify received note message on guest screen.
				String appNoteMessage = "";
				if(imeet_Room.received_note.isDisplayed())
				{
					appNoteMessage = imeet_Room.received_note.getText();
				}
				else
				{
					imeet_Room.notes_button.click();
					Thread.sleep(2000);
					appNoteMessage = imeet_Room.received_note.getText();
				}
				System.out.println("Received Note is " + appNoteMessage);
				Thread.sleep(4000);
				s_assert.assertEquals(appNoteMessage, vNoteMessage, "Expected note " + appNoteMessage + " where actual Note " + vNoteMessage);
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Expected and actual note message did not match");
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_29close
	
} //main close