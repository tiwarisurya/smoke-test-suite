package com.test;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_44_FileUpload_YouTube 
{
	
	public WebDriver host;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC44";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vhostfirstName, vhostlastName, vHostEmail, vYoutubeSearchTerm;
	
		
	@Test //TC44 File Upload You tube
	public void TC44FileUploadYoutube() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vHostEmail = d.HostEmail;
		vPswd = d.Pswd;	
		vYoutubeSearchTerm = d.YouTubeVideoAdd;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
					
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Enter email id
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(2000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				Thread.sleep(3000);
			    //Click on share
				imeet_Room.click_share.click();
				
				//Click on share a file
				Thread.sleep(3000);
				imeet_Room.click_share_file.click();
				
				//Click on Add Video
				Thread.sleep(6000);
				imeet_Room.click_addnew_video.click();
				
				//Search for a video using keyword imeet
				Thread.sleep(6000);
				imeet_Room.input_field_videosearch.sendKeys(vYoutubeSearchTerm);
				
				//Click on Search button
				Thread.sleep(2000);
				imeet_Room.search_button.click();
				
				Thread.sleep(4000);
				String VideoTitle = imeet_Room.search_result_video.getText().trim().toUpperCase();
				Thread.sleep(2000);
				//Click on Add video
				imeet_Room.search_result_video.click();
				//Verify that Video added 
				Thread.sleep(3000);
				String uploaded_file_name = imeet_Room.video_Added_Msg.getText();
				Thread.sleep(3000);
				if(uploaded_file_name.equalsIgnoreCase("Video Added"))
				{
					//Click on Done
					imeet_Room.click_done.click();
					Thread.sleep(2000);
				}
				else
				{
					System.out.println(VideoTitle + " Video from youtube not added to file cabinet.");
					sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
					s_assert.fail(VideoTitle + " Video from youtube not added to file cabinet.");
					Reporter.log(VideoTitle + " Video from youtube not added to file cabinet.");
					throw new SkipException(TestCaseName + " Test Case Skipping. Because"+ VideoTitle + " Youtube video not added to file cabinet.");
				}
				
				//String FirstFileInCabinet = imeet_Room.click_first_file.getText().trim().toUpperCase();
				//System.out.println("Video title in Youtube " + VideoTitle + " Where First file in the cabinet " + FirstFileInCabinet);
				//s_assert.assertEquals(VideoTitle, FirstFileInCabinet, "Video title in Youtube " + VideoTitle + " Where First file in the cabinet " + FirstFileInCabinet);
				
				//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
				int counter = 0;
				String xp1 = "//*[@id='";
				String xp2 = "']/div[1]";
				WebElement fileList = host.findElement(By.xpath(xp1+counter+xp2));
				System.out.println("Youtube file name is " + VideoTitle);
				boolean isYoutubeFileInCabinet = false;
				try
				{
					while(true)
					{
						fileList = host.findElement(By.xpath(xp1+counter+xp2));
						String fileName = fileList.getText().toUpperCase();
						System.out.println(counter + 1 + " time file name " + fileName);
						boolean isBothFileNameSame = StringUtils.containsIgnoreCase(fileName, VideoTitle);
						//boolean isBothFileNameSame = StringUtils.containsIgnoreCase(fileName, VideoTitle+"Yahooo");
						if(isBothFileNameSame){isYoutubeFileInCabinet = true;break;}
						counter++;
					}
				}
				catch(Throwable t)
				{
					System.out.println(counter + 1 + " time True while loop came to exception");
				}
				s_assert.assertTrue(isYoutubeFileInCabinet, "Video title in Youtube " + VideoTitle + " Not exist in file Cabinet" );
				//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			}
			catch(Throwable ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Uploaded mp4 file from youtube failed");
				Reporter.log(ex.getMessage());
			}
		}
		catch (Throwable ex)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured. See the Reporter Log.");
			Reporter.log(ex.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
	} //Test TC_44 close
} //main close