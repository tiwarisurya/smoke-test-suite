package com.test;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_111_PanelRaiseHandDisplay{
	
	public WebDriver guest;
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC111";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vguestfirstName, vguestlastName,vhostfirstName, vhostlastName, vHostEmail,vGuestEmail;
		
	@Test //TC111 Panel - Raise Hand Displays
	public void TC_111panelRaiseHandDisplay() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vGuestPswd = d.GuestPswd;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.UnregisteredGuestEmail;

		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowserhost = new ChooseBrowser(host);
			host = chooseBrowserhost.OpenBrowser(vBrowserType, vUrl);
				
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
				
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(2000);
			login.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
						
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
				
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
						
			//Login with Host
			login.field_EnterEmail.sendKeys(vHostEmail);
						
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);		
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
					
			//Login with guest	
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
						
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
				
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
					
			//Login with Host
			login1.field_EnterEmail.sendKeys(vGuestEmail);
					
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			try
			{
				imeet_Room1.participants_button.click();
				Thread.sleep(1000);
		        String GuestAttrbuteName = guest.findElement(By.xpath("//span[contains(.,'"+vguestfirstName.toUpperCase()+" "+vguestlastName.toUpperCase()+"')]")).getAttribute("id");
				String[] parts = GuestAttrbuteName.split("r");
				String GuestAttributId = parts[1]; // 4644292
				Thread.sleep(1000);
				WebElement GuestName = guest.findElement(By.xpath("//*[@id='nameOfUser"+GuestAttributId+"']"));
				WebElement PanelGuestRaiseHand = guest.findElement(By.xpath("//a[@id='panelRaisedHand"+GuestAttributId+"']"));
				GuestName.click();
				Thread.sleep(500);
				//imeet_Room1.waitForElement(guest, PanelGuestRaiseHand, 5);
				PanelGuestRaiseHand.click();
				Thread.sleep(1000);
				
				//Guest checking on his cube to see guest raise hand
				//Panel verification
				String PanelGuestRaiseHandText = PanelGuestRaiseHand.getAttribute("class");
				boolean isPanelGuestRaiseHandActive = StringUtils.containsIgnoreCase(PanelGuestRaiseHandText, "active");
				s_assert.assertTrue(isPanelGuestRaiseHandActive, TestCaseName + " icon not displayed as Active");
				Thread.sleep(1000);
				//Panel Raise Hand count verification
				String GuestRaiseHandCount = imeet_Room1.raise_hand_count.getText();
				int GuestRaiseHandNumber = Integer.parseInt(GuestRaiseHandCount);
				boolean isRaiseHandNumberGuestPanelDisplay = GuestRaiseHandNumber ==1;
				s_assert.assertTrue(isRaiseHandNumberGuestPanelDisplay, "Fail. Guest Raise hand not displayed Raise Hand count on Guest Participants panel.");
				imeet_Room1.participants_button.click();
				Thread.sleep(500);
				//Cube Raise Hand verification
				WebElement GuestRaiseHandCube = guest.findElement(By.xpath("//div[contains(@id,'raiseHand"+GuestAttributId+"')]"));
				boolean isRaisHandDisplayGuestCube = GuestRaiseHandCube.isDisplayed();
				s_assert.assertTrue(isRaisHandDisplayGuestCube, "Fail. Guest Raise hand not displayed on Guest cube.");
				Thread.sleep(500);
		
				// Host checking on his cube to see guest raise hand
				//Cube Raise Hand verification
				WebElement HostRaiseHandCube = host.findElement(By.xpath("//div[contains(@id,'raiseHand"+GuestAttributId+"')]"));
				boolean isRaisHandDisplayHostCube = HostRaiseHandCube.isDisplayed();
				s_assert.assertTrue(isRaisHandDisplayHostCube, "Fail. Guest Raise hand not displayed on Host cube.");
				
				//Panel Raise Hand count verification
				imeet_Room.participants_button.click();
				Thread.sleep(1000);
				String RaiseHandCount = imeet_Room.raise_hand_count.getText();
				int RaiseHandNumber = Integer.parseInt(RaiseHandCount);
				boolean isRaiseHandNumberDisplay = RaiseHandNumber ==1;
				s_assert.assertTrue(isRaiseHandNumberDisplay, "Fail. Guest Raise hand not displayed on Raise Hand count on Host Participants panel.");
				imeet_Room.participants_button.click();
				Thread.sleep(1000);			
			}
			catch(Throwable t)
			{
				sshot.ScrShot(guest, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail(TestCaseName + " Raise hand icon not displayed");
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured." + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (guest != null && host != null)
			{
				guest.quit();
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_111 close
	
} //main close