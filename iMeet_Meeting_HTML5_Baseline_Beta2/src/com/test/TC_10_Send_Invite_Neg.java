package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_10_Send_Invite_Neg{
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC10";
	
	boolean isHostLogedIn = false;
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String vhostfirstName, vhostlastName, vHostEmail;
		
	@Test //TC10 Send Invite -Negative
	public void TC10_sendInviteNegative() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vHostEmail = d.HostEmail;
		vPswd = d.Pswd;		
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
			login.switchFrame(host);
			
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
						
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
				
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
					
			//Enter email id
			login.field_EnterEmail.sendKeys(vHostEmail);
					
			//Click on Join Meeting
			login.button_Submit.click();
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				Thread.sleep(2000);
				//Click on Invite
				imeet_Room.invite.click();
				
				//Click on Send invite
				Thread.sleep(2000);
				imeet_Room.send_invite_option.click();
				
				//Enter guest email address
				Thread.sleep(2000);
				imeet_Room.field_guest_email_address.clear();
				imeet_Room.field_guest_email_address.sendKeys("testgmail.com");
				
				//Enter Subject
				Thread.sleep(2000);
				imeet_Room.field_enter_subject.clear();
				imeet_Room.field_enter_subject.sendKeys("invalid email 've been invited to my iMeet meeting");
				
				//Click on Send Invite button
				Thread.sleep(2000);
				imeet_Room.button_send_invite.click();
				
				//Verify error message
				Thread.sleep(3000);
				String error_message = imeet_Room.error_message_wrong_emailid.getText();
				s_assert.assertEquals(error_message, "Invalid Email. Please try again.", "Either actual and expected notification message did not match for invalid email id.");
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error message not shown for the wrong email id.and error Detail is " + t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Login page not displayed.and error is " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cb = new ChooseBrowser(host);
				cb.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();	
	} //Test TC_10 close
	
} //main close