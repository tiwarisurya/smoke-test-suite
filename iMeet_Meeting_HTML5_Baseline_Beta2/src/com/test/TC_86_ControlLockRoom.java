package com.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_86_ControlLockRoom {

	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC86";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName,vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, vlockedMeetingMessage;
		
	@Test //TC86 Controls - Lock Room
	public void TC86_controlLockRoom() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vGuestPswd = d.GuestPswd;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vlockedMeetingMessage = d.LockedMeetingMessage.toUpperCase();
		
		try
		{
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
					
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			//Click on Menu icon to lock the meeting room
			imeet_Room.waitForElement(host, imeet_Room.control_menu, 4);
			imeet_Room.control_menu.click();
			Thread.sleep(1000);
			try
			{
				//Lock meeting room
				imeet_Room.waitForElement(host, imeet_Room.lock_meeting, 4);
				String meetingstatus = imeet_Room.lock_meeting.getText().trim().toUpperCase();
				
				if(meetingstatus.contains("LOCK MEETING"))
				{
					imeet_Room.lock_meeting.click(); 
					Thread.sleep(2000);
					imeet_Room.lock_meeting_confirm.click();
					Thread.sleep(1000);
				}
			}
			catch(Exception ex)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error occured to lock the meeting Room. Please check the Report log.");
				Reporter.log("Lock meeting Room Exception is " + ex.getMessage());
			}
	
			//Login as Guest
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(guest);
			guest = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
	
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
		
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
			
			//Login with Host
			login1.field_EnterEmail.sendKeys(vGuestEmail);
			
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(1000);
			common.Guest_Enter_Password(guest, vGuestPswd);
			
			try
			{
				//Verify meeting is locked.
				imeet_Room1.waitForElement(guest, imeet_Room1.locked_meeting_message, 20);
				String lockedmeetingMessage = imeet_Room1.locked_meeting_message.getText().toUpperCase();
				s_assert.assertEquals(lockedmeetingMessage, vlockedMeetingMessage, "Meeting Lock failed. Expected message " + vlockedMeetingMessage +" where actual message " + lockedmeetingMessage );
				Thread.sleep(1000);
			
				//Click on Unlock meeting(As it may affect the next test or other scenario to run hence reverting the setting after this test execution)
				imeet_Room.control_menu.click();
				Thread.sleep(1000);
				String meetingstatus = imeet_Room.lock_meeting.getText().toUpperCase();
				if(meetingstatus.contains("UNLOCK MEETING"))
				{
					imeet_Room.unlock_meeting.click();
					Thread.sleep(2000);
					imeet_Room.control_menu.click();
					Thread.sleep(1000);
				}
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error Occured on verifying meeting locked.");
				Reporter.log(t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				Thread.sleep(1000);
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();	
	} ////Test TC_86 close
	
} //main close