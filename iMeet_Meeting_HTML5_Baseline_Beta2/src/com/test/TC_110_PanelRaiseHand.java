package com.test;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_110_PanelRaiseHand{
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC110";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vhostfirstName, vhostlastName, vHostEmail;

		
	@Test //TC110 Panel Raise Hand
	public void TC_110panelRaiseHand() throws IOException, Exception
	{	
		
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vHostEmail = d.HostEmail;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(2000);
			login.switchFrame(host);
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
				
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
					
			//Login with Host
			login.field_EnterEmail.sendKeys(vHostEmail);
					
			//Click on Join Meeting
			login.button_Submit.click();
			Thread.sleep(1000);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);

			try
			{
				//Store User Id into string
				imeet_Room.participants_button.click();
				Thread.sleep(1000);
		        String attrbutename = host.findElement(By.xpath("//span[contains(.,'"+vhostfirstName.toUpperCase()+" "+vhostlastName.toUpperCase()+"')]")).getAttribute("id");
				String[] parts = attrbutename.split("r");
				String attributID = parts[1]; // 4644292
				
				WebElement HostName = host.findElement(By.xpath("//*[@id='nameOfUser"+attributID+"']"));
				WebElement PanelRaiseHand = host.findElement(By.xpath("//a[@id='panelRaisedHand"+attributID+"']"));
				HostName.click();
				Thread.sleep(500);
				//imeet_Room.waitForElement(host, PanelRaiseHand, 5);
				PanelRaiseHand.click();
				Thread.sleep(1000);
				//Panel verification
				String PanelRaiseHandText = PanelRaiseHand.getAttribute("class");
				boolean isPanelRaiseHandActive = StringUtils.containsIgnoreCase(PanelRaiseHandText, "active");
				s_assert.assertTrue(isPanelRaiseHandActive, TestCaseName + " icon not displayed as active.");
				Thread.sleep(1000);
				//Panel Raise Hand count verification
				String RaiseHandCount = imeet_Room.raise_hand_count.getText();
				int RaiseHandNumber = Integer.parseInt(RaiseHandCount);
				boolean isRaiseHandNumberDisplay = RaiseHandNumber ==1;
				s_assert.assertTrue(isRaiseHandNumberDisplay, TestCaseName + " Fail. Guest Raise hand not displayed Raise Hand count on Guest Participants panel.");
				imeet_Room.participants_button.click();
				Thread.sleep(1000);
				//Cube Raise Hand verification
				WebElement HostRaiseHandCube = host.findElement(By.xpath("//div[contains(@id,'raiseHand"+attributID+"')]"));
				boolean isRaisHandDisplayHostCube = HostRaiseHandCube.isDisplayed();
				s_assert.assertTrue(isRaisHandDisplayHostCube, TestCaseName + " Fail. Host Raise hand not displayed");
				
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Raise hand icon not displayed on cube");
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured." + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
								
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_110 close
	
} //main close