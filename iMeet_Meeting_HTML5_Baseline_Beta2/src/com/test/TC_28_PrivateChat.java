package com.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_28_PrivateChat {
	
	public WebDriver host;
	public WebDriver guest;
	
	TestData d = new TestData();
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC28";
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String vguestfirstName, vguestlastName,vhostfirstName,  vhostlastName, vHostEmail, vGuestEmail, vChatmessage;
	boolean isHostLogedIn = false;
		
	@Test //TC028 Private Chat 
	public void TC028_privateChat() throws IOException, Exception
	{	
		
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.UnregisteredGuestEmail;
		vChatmessage= d.ChatMessage;
		
		try
		{
			//Choose Browser and open for the guest
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login.switchFrame(guest);
			
			common.Guest_LogIn_Option(guest, sPath, PicName);
		
			//Enter first name for guest
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name for guest
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			common.Guest_Enter_Password(guest, vPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
		
			//Login with Host
			//Choose Browser and open for the host
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(host);
			host = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(host);
		
			Thread.sleep(2000);
			login1.switchFrame(host);
			
			common.Host_LogIn_Option(host, sPath, PicName);
				
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Host
			login1.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			Thread.sleep(3000);
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);

			try
			{
				//Click on Panel icon
				Thread.sleep(3000);
				imeet_Room1.participants_button.click();
			
				//Store User Id into string for guest
				Thread.sleep(3000);
				String attrbutenameguest = host.findElement(By.xpath("//span[contains(.,'"+vguestfirstName.toUpperCase()+" "+vguestlastName.toUpperCase()+"')]")).getAttribute("id");
				String[] parts = attrbutenameguest.split("r");
				//String part1 = parts[0]; // nameOfUse
				String attributIDguest = parts[1]; // 4644292
				imeet_Room1.participants_button.click();
				
				System.out.println("Guest Attribute ID " + attrbutenameguest );
				
				//click on Chat icon on guest cube 
				WebElement elg = host.findElement(By.xpath("//a[@id='cubeFrontChatIcon"+attributIDguest+"'"+" and @title='Chat']"));
				Thread.sleep(3000);
				Actions action = new Actions(host);
				WebElement cube = host.findElement(By.xpath("//img[@id='avatarImage"+attributIDguest+"']"));
				action.moveToElement(cube).build().perform();
				Thread.sleep(3000);
				elg.click();
				Thread.sleep(4000);
				
				//Enter chat text
				/*imeet_Room1.chat_input_field.sendKeys(vChatmessage);
				Thread.sleep(2000);
				imeet_Room1.chat_input_field.sendKeys(Keys.ENTER);*/
				
				WebElement chatarea = host.findElement(By.xpath("//*[@id='chatWindow_textEnter"+attributIDguest+"']"));
				chatarea.sendKeys(vChatmessage);
				Thread.sleep(2000);
				chatarea.sendKeys(Keys.ENTER);
				Thread.sleep(4000);
				
				//Verify received chat message on guest screen.
				String appMessage = imeet_Room.received_chat.getText();
				s_assert.assertEquals(appMessage, vChatmessage, "Expected and actual chat message did not match");
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Expected and actual private chat message did not match");
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_28 close
	
} //main close