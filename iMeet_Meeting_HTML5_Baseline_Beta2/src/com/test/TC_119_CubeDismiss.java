package com.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_119_CubeDismiss {
	
	public WebDriver guest;
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC119";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vguestfirstName, vguestlastName,vhostfirstName, vhostlastName, vHostEmail,vGuestEmail;

		
	@Test //TC119 Cube - Dismiss
	public void TC_119cubeDismiss() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vhostfirstName = d.HostFirstName;
		vhostlastName = d.HostlastName;
		vPswd = d.Pswd;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.UnregisteredGuestEmail;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowserhost = new ChooseBrowser(host);
			host = chooseBrowserhost.OpenBrowser(vBrowserType, vUrl);
				
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
				
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(2000);
			login.switchFrame(host);
				
			//Click on Join
			common.Host_LogIn_Option(host, sPath, PicName);
								
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
					
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
						
			//Login with Host
			login.field_EnterEmail.sendKeys(vHostEmail);
						
			//Click on Join Meeting
			login.button_Submit.click();
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			//Login with guest
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
		
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
				
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
			
			login1.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login1.button_Submit.click();
			common.Guest_Enter_Password(guest, vPswd);
			
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);

			try
			{
				// Count total participant in the room
				Thread.sleep(2000);
				String Total_Participant = imeet_Room.text_active_participant.getText();
				int Participants = Integer.parseInt(Total_Participant);
			
				imeet_Room.participants_button.click();
				//Store User Id into string 
				Thread.sleep(1000);
				String attrbutename = host.findElement(By.xpath("//span[contains(.,'"+vguestfirstName.toUpperCase()+" "+vguestlastName.toUpperCase()+"')]")).getAttribute("id");
				String[] parts = attrbutename.split("r");
				//String part1 = parts[0]; // nameOfUse
				String attributID = parts[1]; // 4644292
				
				imeet_Room.participants_button.click();
				Thread.sleep(2000);

				WebElement elg = host.findElement(By.xpath("//a[@id='cubeFrontDismissIcon"+attributID+"'"+" and @title='Dismiss']"));
				Thread.sleep(2000);
				Actions action = new Actions(host);
				WebElement cube = host.findElement(By.xpath("//img[@id='avatarImage"+attributID+"']"));
				action.moveToElement(cube).build().perform();
				Thread.sleep(2000);
				elg.click(); // Host click Dismiss
				Thread.sleep(3000);
				boolean isDismissConfirm = imeet_Room.button_template_blue.isDisplayed();
				if(isDismissConfirm)
				{
					imeet_Room.button_template_blue.click();
					Thread.sleep(3000);
					String Current_Participant = imeet_Room.text_active_participant.getText();
					int ParticipantsNow = Integer.parseInt(Current_Participant);
					boolean GuestNotInMeetingRoom = Participants == ParticipantsNow + 1;
					s_assert.assertTrue(GuestNotInMeetingRoom, "Guest not dismiss from the meeting room");
				}
				else
				{
					System.out.println("Dismiss guest test fail");
					s_assert.fail("Dismiss guest test fail");
				}
			}
			catch(Throwable t)
			{
				sshot.ScrShot(guest, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Guest is not dismissed. Error Detail " + t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);
				Thread.sleep(1000);				
			}
			Thread.sleep(1000);
			if (guest != null && host != null)
			{
				guest.quit();
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
	} //Test TC_119 close
	
} //main close