package com.test_cloud_control;

import java.io.IOException;


import org.openqa.selenium.WebDriver;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.CloudControl_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_01_AddUser {
	
	public WebDriver wd;
	SoftAssert s_assert = new SoftAssert();
	TestData testData = new TestData();
	public int iterator, TotalRow ;
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC_01_AddUser";
	
	String vBrowserType, vCloudUrl, CloudUser, CloudPwd;
	String vhostfirstName, vhostlastName, vRegisteredhostEmail;
	String 	user_fName,user_lName,user_email,user_phone,user_roomAddress,user_password, user_address1,user_address2, user_city,user_postalcode,user_state;
		
	@Test //Tc_01 Add user Cloud Control
	public void TC001_verifyAdduser() throws IOException, Exception
	{
		vBrowserType  = testData.BrowserType;
		vCloudUrl = testData.CloudUrl;
		CloudUser = testData.CloudUser;
		CloudPwd = testData.Pswd;
		user_fName = testData.user_fName;
		user_lName = testData.user_lName;
		user_email = testData.user_email;
		user_phone = testData.user_phone;
		user_roomAddress = testData.user_roomAddress;
		user_password = testData.user_password;
		user_address1 = testData.user_address1;
		user_address2 = testData.user_address2;
		user_city = testData.user_city;
		user_postalcode = testData.user_postalcode;
		user_state = testData.user_state;
			
		//Choose Browser and open for the Host
		ChooseBrowser chooseBrowser = new ChooseBrowser(wd);
		wd = chooseBrowser.OpenBrowser(vBrowserType, vCloudUrl);
		
		//Creating required objects
		CloudControl_Page cloud_home = new CloudControl_Page(wd);
		
		//Enter user name
		Thread.sleep(6000);
	    Assert.assertTrue(cloud_home.field_user_name.isDisplayed());
	    cloud_home.field_user_name.clear();
	    System.out.println("cloud user is .." + CloudUser);
	    cloud_home.field_user_name.sendKeys(CloudUser);
		
	    //Enter password
	    cloud_home.field_password.clear();
	    cloud_home.field_password.sendKeys(CloudPwd);
			
	    //Click on SignIn
	    cloud_home.button_Sign_In.click();
					
		try
		{
			
		//Click on manage my Company
		cloud_home.click_manage_my_company.click();
		
		Thread.sleep(6000);
		//Click on users tab
		cloud_home.tab_users.click();
		Thread.sleep(4000);
		//Click on New user
		cloud_home.buton_add_user.click();
		Thread.sleep(12000);
		
		
		wd.switchTo().frame("TB_iframeContent");
		//Enter First Name
	    cloud_home.field_first_name.clear();
	    cloud_home.field_first_name.sendKeys(user_fName);
	    
	    //Enter Last name
	    cloud_home.field_last_name.clear();
	    cloud_home.field_last_name.sendKeys(user_lName);
	    
	    //Enter Email
	    cloud_home.field_user_email.clear();
	    cloud_home.field_user_email.sendKeys(user_email);
	    
	    //Enter Phone
	    cloud_home.field_phone.clear();
	    cloud_home.field_phone.sendKeys(user_phone);
	    
	    //Enter Room Address
	    cloud_home.field_room_address.clear();
	    cloud_home.field_room_address.sendKeys(user_roomAddress);
	    
/*	    //Enter new user password
	    cloud_home.input_password.clear();
	    cloud_home.input_password.sendKeys(user_password);*/
	    
/*	    //Select User Type
	    Thread.sleep(3000);
	    Select select = cloud_home.select_user_type;
	    wd.findElement(By.xpath("//select[contains(@id,'user_type')]")).click();
	    select.selectByValue("512");
	    Thread.sleep(5000);*/
	    
	    //Enter Address1
	    cloud_home.field_address1.clear();
	    cloud_home.field_address1.sendKeys(user_address1);
	    
	    //Enter Address2
	    cloud_home.field_address2.clear();
	    cloud_home.field_address2.sendKeys(user_address2);
	    
	    //Enter City
	    cloud_home.field_city.clear();
	    cloud_home.field_city.sendKeys(user_city);
	    
	    //Enter postal Code
	    cloud_home.field_postal_code.clear();
	    cloud_home.field_postal_code.sendKeys(user_postalcode);
	    
/*	    //Select Country
	    Select selectcountry = cloud_home.select_country;
	    selectcountry.selectByVisibleText("country");*/
	    
	    //Enter State
	    cloud_home.field_state.clear();
	    cloud_home.field_state.sendKeys(user_state);
	    
	    //Click on Save Changes
	    cloud_home.click_save_changes.click();
		}
		catch(Exception ex)
		{
			sshot.ScrShot(wd, sPath, PicName+iterator+".jpg");
			s_assert.fail(iterator + " All the details required.");
		}
	    try{
	    //Verify that user created successfully
	    Thread.sleep(6000);
	    String successfull_msg = cloud_home.confirmation_message.getText();
	    System.out.println("message is:"+successfull_msg);
	    Assert.assertTrue(successfull_msg.contains("User Created Successfully"));
		
		}
		catch(Throwable t)
		{
			sshot.ScrShot(wd, sPath, PicName+iterator+".jpg");
			t.printStackTrace();
			s_assert.fail(iterator + " User is not Created successfully");
		}
		finally
		{
			if (wd != null)
			{
				wd.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_05 close
	
} //main close