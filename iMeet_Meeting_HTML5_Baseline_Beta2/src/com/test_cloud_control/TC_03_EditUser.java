package com.test_cloud_control;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.CloudControl_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_03_EditUser {
	
	public WebDriver wd;
	SoftAssert s_assert = new SoftAssert();
	TestData testData = new TestData();
	public int iterator, TotalRow ;
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC_03_EditUser";
	
	String vBrowserType, vCloudUrl, CloudUser, CloudPwd;
	String vhostfirstName, vhostlastName, vRegisteredhostEmail;
	String 	user_fName,user_lName,user_email,user_phone,user_roomAddress,user_password, user_address1,user_address2, user_city,user_postalcode,user_state, user_updated_phone;
		
	@Test //Tc_03 Edit user Cloud Control
	public void TC003_verifyEdituser() throws IOException, Exception
	{
		vBrowserType  = testData.BrowserType;
		vCloudUrl = testData.CloudUrl;
		CloudUser = testData.CloudUser;
		CloudPwd = testData.Pswd;
		user_fName = testData.user_fName;
		user_lName = testData.user_lName;
		user_email = testData.user_email;
		user_phone = testData.user_phone;
		user_roomAddress = testData.user_roomAddress;
		user_password = testData.user_password;
		user_address1 = testData.user_address1;
		user_address2 = testData.user_address2;
		user_city = testData.user_city;
		user_postalcode = testData.user_postalcode;
		user_state = testData.user_state;
		user_updated_phone = testData.user_updated_phone;
		
		//Choose Browser and open for the Host
		ChooseBrowser chooseBrowser = new ChooseBrowser(wd);
		wd = chooseBrowser.OpenBrowser(vBrowserType, vCloudUrl);
		
		//Creating required objects
		CloudControl_Page cloud_home = new CloudControl_Page(wd);
		
		//Enter user name
		Thread.sleep(6000);
	    Assert.assertTrue(cloud_home.field_user_name.isDisplayed());
	    cloud_home.field_user_name.clear();
	    cloud_home.field_user_name.sendKeys(CloudUser);
		
	    //Enter password
	    cloud_home.field_password.clear();
	    cloud_home.field_password.sendKeys(CloudPwd);
			
	    //Click on SignIn
	    cloud_home.button_Sign_In.click();
					
		try
		{
			
		//Click on manage my Company
		cloud_home.click_manage_my_company.click();
		
		Thread.sleep(6000);
		//Click on users tab
		cloud_home.tab_users.click();
		//Enter user name into search field
	    cloud_home.field_search_user.clear();
	    cloud_home.field_search_user.sendKeys(user_fName);
	    
	    //Click on Search button
		cloud_home.button_search.click();
		Thread.sleep(6000);
		//Select user and click on edit user
		new Select(wd.findElement(By.id("select-box-user-actions-3739117"))).selectByVisibleText("Edit Profile");
		Thread.sleep(12000);
		wd.switchTo().frame("TB_iframeContent");

		}
		catch(Exception ex)
		{
			sshot.ScrShot(wd, sPath, PicName+iterator+".jpg");
			s_assert.fail(iterator + " All the details required.");
		}
	    try{
		    //Modify Phone number
		    cloud_home.field_phone.clear();
		    cloud_home.field_phone.sendKeys(user_updated_phone);
		  
		    //Click on Save Changes
		    cloud_home.click_save_changes.click();
		
		}
		catch(Throwable t)
		{
			sshot.ScrShot(wd, sPath, PicName+iterator+".jpg");
			t.printStackTrace();
			s_assert.fail(iterator + " User is not Updated successfully");
		}
		finally
		{
			if (wd != null)
			{
				wd.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_03 Edit User
	
} //main close