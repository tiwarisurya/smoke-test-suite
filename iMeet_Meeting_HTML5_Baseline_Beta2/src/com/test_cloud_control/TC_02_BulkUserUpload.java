package com.test_cloud_control;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.CloudControl_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class TC_02_BulkUserUpload {
	
	public WebDriver wd;
	SoftAssert s_assert = new SoftAssert();
	
	TestData testData = new TestData();
	public int iterator, TotalRow ;
	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "TC_02_BulkUserUpload";
	
	String vBrowserType, vCloudUrl, CloudUser, CloudPwd;
	String vhostfirstName, vhostlastName, vRegisteredhostEmail;
	String 	user_fName,user_lName,user_email,user_phone,user_roomAddress,user_password, user_address1,user_address2, user_city,user_postalcode,user_state;
		
	@Test //Tc_02 Bulk user upload- Cloud Control
	public void TC_02_bulkUserUpload() throws IOException, Exception
	{
		vBrowserType  = testData.BrowserType;
		vCloudUrl = testData.CloudUrl;
		CloudUser = testData.CloudUser;
		CloudPwd = testData.Pswd;
		user_fName = testData.user_fName;
		user_lName = testData.user_lName;
		user_email = testData.user_email;
		user_phone = testData.user_phone;
		user_roomAddress = testData.user_roomAddress;
		user_password = testData.user_password;
		user_address1 = testData.user_address1;
		user_address2 = testData.user_address2;
		user_city = testData.user_city;
		user_postalcode = testData.user_postalcode;
		user_state = testData.user_state;
			
		//Choose Browser and open for the Host
		ChooseBrowser chooseBrowser = new ChooseBrowser(wd);
		wd = chooseBrowser.OpenBrowser(vBrowserType, vCloudUrl);
		
		//Creating required objects
		CloudControl_Page cloud_home = new CloudControl_Page(wd);
		
		//Enter user name
		Thread.sleep(6000);
	    Assert.assertTrue(cloud_home.field_user_name.isDisplayed());
	    cloud_home.field_user_name.clear();
	    System.out.println("cloud user is .." + CloudUser);
	    cloud_home.field_user_name.sendKeys(CloudUser);
		
	    //Enter password
	    cloud_home.field_password.clear();
	    cloud_home.field_password.sendKeys(CloudPwd);
			
	    //Click on SignIn
	    cloud_home.button_Sign_In.click();
					
/*		try
		{*/
			
		//Click on manage my Company
		cloud_home.click_manage_my_company.click();
		Thread.sleep(6000);
			
		//Click on users tab
		cloud_home.tab_users.click();
		Thread.sleep(4000);
		//Click on Add bulk user
		cloud_home.buton_add_bulk_user.click();
		Thread.sleep(6000);
		
		
		wd.switchTo().frame("TB_iframeContent");  
		
		cloud_home.buton_browse_your_file.click();
		
		
		  // Specify the file location with extension
		 StringSelection sel = new StringSelection("C:\\screenshot\\upload_users.csv");
		 
		  // Copy to clipboard
		 Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
		 System.out.println("selection" +sel);
		
		

		 // Create object of Robot class
		 Robot robot = new Robot();
		 Thread.sleep(1000);
		      
		  // Press Enter
		 robot.keyPress(KeyEvent.VK_ENTER);
		 
		// Release Enter
		 robot.keyRelease(KeyEvent.VK_ENTER);
		 
		  // Press CTRL+V
		 robot.keyPress(KeyEvent.VK_CONTROL);
		 robot.keyPress(KeyEvent.VK_V);
		 
		// Release CTRL+V
		 robot.keyRelease(KeyEvent.VK_CONTROL);
		 robot.keyRelease(KeyEvent.VK_V);
		 Thread.sleep(1000);
		        
		//Press Enter 
		 robot.keyPress(KeyEvent.VK_ENTER);
		 robot.keyRelease(KeyEvent.VK_ENTER);
		 
		 Thread.sleep(3000);
		 wd.findElement(By.xpath("//button[contains(@type,'submit')]")).click();
		 
/*		try{
	    //Click on Browse your file
		String path = "C:\\screenshot\\upload_users.csv";
	    cloud_home.buton_browse_your_file.sendKeys(path);
	    Thread.sleep(3000);
	    wd.findElement(By.xpath("//button[contains(@type,'submit')]")).click();
		Thread.sleep(12000);
		}
		catch(Exception ex)
		{
			sshot.ScrShot(wd, sPath, PicName+iterator+".jpg");
			s_assert.fail(iterator + " All the details required.");
		}*/
	    try{
/*	    //Verify that user created successfully
	    Thread.sleep(6000);
	    String successfull_msg = cloud_home.confirmation_message.getText();
	    System.out.println("message is:"+successfull_msg);
	    Assert.assertTrue(successfull_msg.contains("User Created Successfully"));*/
		
		}
		catch(Throwable t)
		{
			sshot.ScrShot(wd, sPath, PicName+iterator+".jpg");
			t.printStackTrace();
			s_assert.fail(iterator + " User is not Created successfully");
		}
		finally
		{
			if (wd != null)
			{
		//		wd.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test TC_05 close
	
} //main close