package com.pageobject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class iMeetRoom_Page {
	
	WebDriver driver;
	public iMeetRoom_Page(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	
	@FindBy(xpath="//div[contains(@id,'audioScreenAsideCancel')]")
	public WebElement closeicon;
	
	@FindBy(xpath="//select[contains(@id,'phoneType-Dropdown')]")
	public WebElement select_phonetype;
	
	@FindBy(xpath="//div[contains(@id,'addPhoneNumberCountryCode')]")
	public WebElement countrycode;
	
	@FindBy(xpath="//input[contains(@id,'addUserPhoneNumber')]")
	public WebElement phonenumber;
	
	@FindBy(xpath="//input[contains(@id,'addPhoneNumberExt')]")
	public WebElement phoneextension;
	
	@FindBy(xpath="//div[contains(@id,'audioPanelCallMeBtn')]")
	public WebElement button_callme;
	
	@FindBy(xpath="//div[contains(@id,'audioScreenDialInBtn')]")
	public WebElement dialIn;
	
	@FindBy(xpath="//div[contains(@id,'addNewNumberElem')]")
	public WebElement connectaudio_addnewnumber;
	
	@FindBy(xpath="//div[contains(@id,'otherAccessNumberContent')]")
	public WebElement dialIn_access_numbers; 
	
	@FindBy(xpath="//*[@id='addCountryNameSel']")
	public WebElement international_access_numbers;
	
	@FindBy(xpath="//a[contains(@id,'connect')]/span")
	public WebElement connect;
	
	@FindBy(xpath="//*[@id='webcamStatusText']")
	public WebElement connect_turn_on_webcam;
	
	@FindBy(xpath="//*[@id='callMyIpadSpan']")
	public WebElement connect_connect_audio;
	
	@FindBy(xpath="//a[contains(@id,'addGuest')]/span")
	public WebElement invite;
	
	@FindBy(xpath="//*[@id='sendInviteBubble']/div/span[2]")
	public WebElement send_invite_option;
	
	@FindBy(xpath="//*[@id='addGuestEmail']")
	public WebElement field_guest_email_address;
	
	@FindBy(xpath="//*[@id='addGuestSubject']")
	public WebElement field_enter_subject;
	
	@FindBy(xpath="//*[@id='emailGuestField']/footer/div/a")
	public WebElement button_send_invite;
	
	@FindBy(xpath="//*[@id='chatWindow_chatZoneimeetMsg']/div")
	public WebElement notification_sent_invite;
	
	@FindBy(xpath="//*[@id='errorOfAddGuestEmail']")
	public WebElement error_message_wrong_emailid;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'SEND INVITE')]")
	public WebElement send_invite;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'SEND INVITE')]")
	public WebElement chat_icon;
	
	@FindBy(xpath="//a[contains(@id,'filesCabinetMenu')]/span")
	public WebElement share;
	
	@FindBy(xpath="//a[contains(@title,'Turn On/Off Webcam')]")
	public WebElement video_icon;
	
	@FindBy(xpath="//a[contains(@title,'Call')]")
	public WebElement audio_icon;
	
	@FindBy(xpath="//a[contains(@title,'Info')]")
	public WebElement info_icon;
	
	@FindBy(xpath="//a[contains(@title,'Raise Hand')]")
	public WebElement raise_hand;
	
	@FindBy(xpath="//*[@id='raisedHandCount']")
	public WebElement raise_hand_count;
	
	@FindBy(xpath="//a[contains(@id,'participantRoomButtom')]")
	public WebElement participants_button;
	
	@FindBy(xpath="//span[contains(.,'NANCY12 JAIN12')]")
	public WebElement hostName;
	
	@FindBy(xpath="//a[contains(@id,'chatButton')]")
	public WebElement chat_button;
	
	@FindBy(xpath="//textarea[contains(@class,'chatWindow_textEnter')]")
	public WebElement chat_input_field; 
	
	@FindBy(xpath="//textarea[contains(@id,'roomNotesTextarea')]")
	public WebElement note_input_field; 
	
	@FindBy(xpath="//textarea[contains(@id,'selfNotesTextarea')]")
	//@FindBy(xpath="//textarea[contains(@id,'roomNotesTextarea')]") 
	public WebElement mynote_input_field; 
	
	
	@FindBy(xpath="//*[@id='selfNoteStatusContainer']/span")
	//@FindBy(xpath="//*[@id='roomNoteStatusContainer']/span")
	public WebElement notes_menu_item; 
	
	@FindBy(xpath="//*[@id='emailNotes']")
	public WebElement notes_menu_item_email; 
	
	@FindBy(xpath="//a[contains(@id,'addNoteUser')]")
	public WebElement click_add_note_user; 
	
	@FindBy(xpath="//*[@id='addNewNoteUserEmail']")
	public WebElement input_note_user_email; 
	
	@FindBy(xpath="//*[@id='emailNotesWindow']/fieldset/div[2]/a")
	public WebElement button_Done_note; 
	
	@FindBy(xpath="//span[contains(@class,'enter_chat_txt')]")
	public WebElement received_chat; 
	
	@FindBy(xpath="//p[contains(@class,'noteMsgContent')]") 
	public WebElement received_note; 
	
	@FindBy(xpath="//*[@id='cubeFrontChatIcon5026116']")
	public WebElement guest_chat_icon; 

	@FindBy(xpath="//a[contains(@id,'participantRoomButtom')]")
	public WebElement participantRoomButtom; 
	
	@FindBy(xpath="//a[contains(@id,'notesButton')]")
	public WebElement notes_button;
	
	@FindBy(xpath="//*[@id='selfNotesHeader']/span[2]")
	public WebElement my_notes;
	
	@FindBy(xpath="//a[contains(@id,'panelSort')]")
	public WebElement panel_icon;	
	
	@FindBy(xpath="//a[contains(@id,'roomControl')]")
	public WebElement control_menu;
	
	@FindBy(xpath="//span[contains(@id,'recordingIconMsg')]")
	public WebElement record_meeting;
	
	@FindBy(xpath="//span[contains(@id,'lockIconMsg')]")
	public WebElement lock_meeting;
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement lock_meeting_confirm;
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement button_template_blue;
	
	
	@FindBy(xpath="//*[@id='participantRoomButtom']/div")
	public WebElement text_active_participant;
	
	@FindBy(xpath="//div[contains(@id,'unlockMeetingBtn')]")
	public WebElement unlock_meeting;
	
	@FindBy(xpath="//p[contains(@id,'templateHeader')]")
	public WebElement locked_meeting_message;
	
	//@FindBy(xpath="//*[@id='roomkeyWindow']/div/div/strong") 
	@FindBy(xpath="//div[@id='roomKeyMsgTxt']")
	public WebElement locked_meeting_message_Key;
	
	@FindBy(xpath="//span[contains(@id,'endMeetingMsg')]")
	public WebElement leave_meeting;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'EDIT PROFILE')]")
	public WebElement edit_profile;
	
	@FindBy(xpath="//a[contains(@id,'details_tab')]")
	public WebElement edit_profile_details;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'Settings')]")
	public WebElement settings;
	
	@FindBy(xpath="//*[@id='gloginpasswordWindow']/header")
	public WebElement settings_unreg_guest_saveProfile;
	
	@FindBy(xpath="//*[@id='gLoginpass']")
	public WebElement settings_unreg_guest_enterpass;
	
	@FindBy(xpath="//*[@id='gLoginCnfrmpass']")
	public WebElement settings_unreg_guest_re_enterpass;
	
	@FindBy(xpath="//*[@id='btnSaveProfile']")
	public WebElement settings_unreg_guest_savebutton;
	
	@FindBy(xpath="//*[@id='btnSaveForProfileDetail']")
	public WebElement settings_unreg_guest_details_donebutton;
	
	@FindBy(xpath="//a[contains(@id,'helpPermimumContactUsTab')]")
	public WebElement help_contactus;
	
	@FindBy(xpath="//*[@id='givefeedback']/footer/div/a[2]")
	public WebElement help_contactUs_send;
	
	@FindBy(xpath="//a[contains(@id,'helpPermimumSupportTab')]")
	public WebElement help_resource;
	
	@FindBy(xpath="//*[@id='supportCommunityLink']")
	public WebElement help_Resource_Community;
	
	@FindBy(xpath="//a[contains(@id,'helpPermimumFeedbackTab')]")
	public WebElement help_premiumfeedback;
	
	@FindBy(xpath="//*[@id='tellusabout']/footer/div/a")
	public WebElement help_feedback_submit;
	
	@FindBy(xpath="//div[contains(@class,'navItem')]//*[contains(.,'HELP')]")
	public WebElement help;
	
	@FindBy(xpath="//a[contains(@id,'settings_meeting_tab')]")
	public WebElement setting_meeting;
	
	@FindBy(xpath="//*[@id='lockMeetingControl']/span[2]")
	public WebElement setting_meeting_lockMeeting;
	
	@FindBy(xpath="//input[contains(@id,'meetingNameField')]")
	public WebElement setting_meeting_name;
	
	@FindBy(xpath="//*[@id='roomNameDisplayForBrowsers']")
	public WebElement text_meeting_name;
	
	@FindBy(xpath="//*[@id='lockMeetingControl']")
	public WebElement setting_meeting_lockMeeting_status;
	
	@FindBy(xpath="//*[@id='showHostOnlyControl']")
	public WebElement setting_meeting_showonstage_status;
	
	@FindBy(xpath="//*[@id='autoWebcamControl']")
	public WebElement setting_meeting_autowebcamstatus;
	
	@FindBy(xpath="//*[@id='fullStageControl']")
	public WebElement setting_meeting_autofullstage_status;
	
	@FindBy(xpath="//*[@id='emailNotificationControl']")
	public WebElement setting_meeting_emailnotification_status;
	
	@FindBy(xpath="//*[@id='emailNotificationField']")
	public WebElement setting_meeting_emailnotification_inputfield;
	
	@FindBy(xpath="//input[contains(@id,'roomKeyField')]")
	public WebElement setting_meeting_roomkey;
	
	@FindBy(xpath="//*[@id='settingsWindow']/div[2]/a")
	public WebElement setting_meeting_roomkey_Done;
	
	@FindBy(xpath="//a[contains(@id,'settings_theme_tab')]")
	public WebElement setting_theme;
	
	@FindBy(xpath="//a[contains(@id,'settings_notification_tab')]")
	public WebElement setting_notifications;
	
	@FindBy(xpath="//a[contains(@id,'settings_files_tab')]")
	public WebElement setting_files;
	
	@FindBy(xpath="//a[contains(@id,'settings_dialing_tab')]")
	public WebElement setting_dialing;
	
	@FindBy(xpath="//*[@id='tollFreeControl']")
	public WebElement setting_dialing_enabletollfree;
	
	@FindBy(xpath="//*[@id='internationalAudioControl']")
	public WebElement setting_dialing_enableinternationaldialing;
	
	@FindBy(xpath="//*[@id='accountEmailIdUpdateButton']")
	public WebElement setting_account_editemailbutton;
	
	@FindBy(xpath="//*[@id='accountEmailId']")
	public WebElement setting_account_emailfield; 
	
	@FindBy(xpath="//*[@id='accountEmailIdUpdateButton']")
	public WebElement setting_account_emailSavebutton;
	
	@FindBy(xpath="//*[@id='profile_current_pass']")
	public WebElement setting_account_currentpassfield;
	
	@FindBy(xpath="//*[@id='btn_change']")
	public WebElement setting_account_changeroombutton;
	
	@FindBy(xpath="//*[@id='imeeturl']")
	public WebElement setting_account_roomurlfield;
	
	@FindBy(xpath="//*[@id='profile_new_pass']")
	public WebElement setting_account_newpassfield;
	
	@FindBy(xpath="//*[@id='profile_confirm_pass']")
	public WebElement setting_account_confirmpassfield;
	
	@FindBy(xpath="//*[@id='passSaveBtn']")
	public WebElement setting_account_savebutton;  
	
	@FindBy(xpath="//*[@id='imeeturlboxsave']")
	public WebElement setting_account_saveimeeturlbutton;
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement setting_account_changeurl_OK;
	
	@FindBy(xpath="//*[@id='settings_account_content']//p/a")
	public WebElement setting_account_forgotpaswordlink;
	
	@FindBy(xpath="//*[@id='vCardDownloadControl']")
	public WebElement setting_vCard_download;
	
	@FindBy(xpath="//*[@id='filesViewAccessControl']")
	public WebElement setting_file_view;
	
	@FindBy(xpath="//*[@id='filesDownloadAccessControl']")
	public WebElement setting_file_download;
	
	@FindBy(xpath="//*[@id='filesUploadAccessControl']")
	public WebElement setting_file_upload;
	
	@FindBy(xpath="//a[contains(@id,'settings_more_tab')]")
	public WebElement setting_more;
	
	@FindBy(xpath="//a[contains(@id,'settings_more_account')]")
	public WebElement setting_more_account;
	
	@FindBy(xpath="//a[contains(@id,'settings_more_privacy')]")
	public WebElement setting_more_privacy;
	
	@FindBy(xpath="//*[@id='settings_account_tab']")
	public WebElement setting_account;
	
	@FindBy(xpath="//*[@id='settings_privacy_tab']")
	public WebElement setting_privacy;
	
	@FindBy(xpath="//div[contains(@class,'feedreport')]")
	public WebElement save_profile;
	
	@FindBy(xpath="//*[@id='chatWindow_chatZoneimeetMsg']/div")
	public WebElement Addto_contact_confirmation;
	
	@FindBy(xpath="//*[@id='filesCabinetMenu']/span")
	public WebElement click_share;
	
	@FindBy(xpath="//*[@id='startScreenShare']/div/span[2]")
	public WebElement click_share_screen_share;
	
	@FindBy(xpath="//*[@id='fileNavigationTxt']")
	public WebElement click_share_file;
	
	@FindBy(xpath="//a[contains(@id,'files_tab')]")
	public WebElement share_share_file_files;
	
	@FindBy(xpath="//*[@id='dvr_tab']")
	public WebElement share_share_file_meetingminutes;
	
	@FindBy(xpath="//*[@id='guest_tab']")
	public WebElement share_share_file_guestaccess;
	
	@FindBy(xpath="//*[@id='viewFilesYes']")
	public WebElement share_share_file_guestaccess_viewfile;
	
	@FindBy(xpath="//*[@id='uploadFilesYes']")
	public WebElement share_share_file_guestaccess_uploadfile;
	
	@FindBy(xpath="//*[@id='downloadFilesYes']")
	public WebElement share_share_file_guestaccess_downloadfile;
	
	@FindBy(xpath="//*[@id='addNewfile']")
	public WebElement click_addnew_file;
	
	@FindBy(xpath="//*[@id='addNewVideo']")
	public WebElement click_addnew_video;
	
	@FindBy(xpath="//*[@id='0']/div[1]")
	public WebElement click_first_file;
	
	@FindBy(xpath="//*[@id='0']/div[3]/a[1]")
	public WebElement click_first_file_unlock;
	
	@FindBy(xpath="//*[@id='fileshowlink']")
	public WebElement click_present;
	
	@FindBy(xpath="//*[@id='presentationNameDisplay']")
	public WebElement verify_presented_file;
	
	@FindBy(xpath="//*[@id='passControlBtn']")
	public WebElement click_pass_control;
	
	@FindBy(xpath="//*[@id='passControlSearch']")
	public WebElement search_user_field;
	
	@FindBy(xpath="//*[@id='pC_userRow_4644292']/div[2]/p")
	public WebElement search_result_user;
	
	@FindBy(xpath="//*[@id='acceptBtn']/span")
	public WebElement accept_control;
	
	@FindBy(xpath="//*[@id='passControlPopUpBody']/font[2]")  
	public WebElement confirm_control_accepted;

	@FindBy(xpath="//*[@id='okBtn']/span")
	public WebElement ok_button_passcontrol;
	
	@FindBy(xpath="//*[@id='requestControlBtn']/span")
	public WebElement click_request_control;
	
	@FindBy(xpath="//*[@id='acceptBtn']/span")
	public WebElement click_grant;

	@FindBy(xpath="//*[@id='presentationNameDisplay']")
	public WebElement presented_file_name;
	
	@FindBy(xpath="//*[@id='presentationNextHandler']/span")
	public WebElement presented_file_next;
	
	@FindBy(xpath="//*[@id='templateHeader']")
	public WebElement screenshare_alert; 
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement click_close_alert;
	
	@FindBy(xpath="//*[@id='presentationPrevHandler']/span")
	public WebElement presented_file_prev;
	
	@FindBy(xpath="//*[@id='presentationPageCountId']")
	public WebElement presented_file_pagination;
	
	@FindBy(xpath="//nav[contains(@id,'switchPresentationMode')]")
	public WebElement presentation_mode;
	
	@FindBy(xpath="//*[@id='gloginpasswordWindow']/header/div[2]/span")
	public WebElement save_profile_close;
	
	@FindBy(xpath="//*[@id='templateMessage']")
	public WebElement continueAs_guest_message;
	
	@FindBy(xpath="//*[@id='templateButton']")
	public WebElement button_ok;
	
	@FindBy(xpath="//*[@id='fileMemory']")
	public WebElement file_memory;
	
	@FindBy(xpath="//*[@id='farenRadioBtn']")
	public WebElement setting_status_temp_F;
	
	@FindBy(xpath="//*[@id='celRadioBtn']")
	public WebElement setting_status_temp_C;
	
	@FindBy(xpath="//*[@id='settingsClockTwelve']")
	public WebElement setting_status_clock_12;
	
	@FindBy(xpath="//*[@id='settingsClockTwentyFour']")
	public WebElement setting_status_clock_24;
	
	@FindBy(xpath="//*[@id='amPmTimeInfoForBrowser']/span")
	public WebElement room_clock_type;
	
	@FindBy(xpath="//*[@id='tempForBrowsers']")
	public WebElement room_temp_type;
	
	
	
	
	
	//Search for a video using keyword imeet
	//*[@id='videoSearch']
	
	//Click on Add video
	//*[@id='addvideorow0']/div[1]
	
	//Verify that Video added 
	//*[@id='videoaddedstatus0']  
	@FindBy(xpath="//*[@id='videoSearch']")
	public WebElement input_field_videosearch;
	
	@FindBy(xpath="//*[@id='addVideoSearchButton']")
	public WebElement search_button;
	
	@FindBy(xpath="//*[@id='addvideorow0']/div[1]")
	public WebElement search_result_video;
	
	@FindBy(xpath="//*[@id='videoaddedstatus0']")
	public WebElement video_Added_Msg;
	
	@FindBy(xpath="//*[@id='youtubeDonelink']")
	public WebElement click_done;
	
	@FindBy(xpath="//a[contains(@class,'vcardColor')]")
	public WebElement email_VCard;
	
	@FindBy(xpath="//p[contains(@id,'templateHeader')]")
	public WebElement vCard_msg_confirmation;
	
	//Wait for element
	public void waitForElement(WebDriver driver, WebElement myElement, int time){
		WebDriverWait wait = new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOf(myElement));
	}
}
