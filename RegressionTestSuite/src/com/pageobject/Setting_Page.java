package com.pageobject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Setting_Page {
	
	WebDriver driver;
	public Setting_Page(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	@FindBy(xpath="//p[contains(@id,'goToSignInButton')]")
	public WebElement link_UseTheBrowser;
	
	@FindBy(xpath="//*[@id='downloadAppButton']")
	public WebElement link_DownloadiMeetapp;
	
	@FindBy(xpath="//*[@id='howToJoinPanel']/img")
	public WebElement logo_iMeet;
	
	@FindBy(xpath="//a[contains(@class,'join-via-browser-link')]")
	public WebElement link_JoinUsingBrowser;
	
	@FindBy(xpath="//input[contains(@name,'first-name')]")
	public WebElement field_EnterFirstName;
	
	@FindBy(xpath="//input[contains(@name,'last-name')]")
	public WebElement field_EnterLastName;
	
	@FindBy(xpath="//input[contains(@type,'email')]")
	public WebElement field_EnterEmail;
	
	@FindBy(xpath="//input[contains(@type,'password')]")
	public WebElement field_EnterPassword;
	
	@FindBy(xpath="//button[contains(@id,'sign-In-SubmitBtn')]")
	public WebElement button_Submit;
	
	@FindBy(xpath="//div[contains(@class,'enter-sign-in blue-button-wide')]")
	public WebElement button_SignIn;
	
	@FindBy(xpath="//label[contains(@class,'controls-container-Label')]")
	public WebElement checkbox_keepmesignedIn;
	
	@FindBy(xpath="//p[contains(@id,'sign-in-error')]")
	public WebElement text_ErrormsgForName;
	
	@FindBy(xpath="//p[contains(@id,'sign-in-email-error')]")
	public WebElement text_ErrormsgForEmail;
	
	@FindBy(xpath="//a[contains(.,'Continue as guest')]")
	public WebElement link_ContinueAsGuest;
	
	@FindBy(xpath="//a[contains(.,'Forgot password?')]")
	public WebElement link_ForgotPassword;
	
	@FindBy(xpath="//*[@id='howToJoinPanel']//h3")
	public WebElement text_UseYourWebBrowser;
	
	@FindBy(xpath="//*[@id='howToJoinPanel']//div[3]/h3")
	public WebElement text_DownloadappandTalkinMeeting;
	
	@FindBy(xpath="//*[@id='alreadyInLink']")
	public WebElement text_IAlreadyHaveapp;
	
	@FindBy(xpath="//a[contains(@id,'loginTermOfService')]")
	public WebElement link_TermsofService;
	
	@FindBy(xpath="//*[@id='loginPrivacyPolicy']")
	public WebElement link_privacy_statement;
	
	@FindBy(xpath="//*[@id='loginSupportUrl']")
	public WebElement link_Support;
	
	@FindBy(xpath="//div[contains(@class,'select-styled')]")
	public Select dropdown_language;
	
	@FindBy(xpath="//div[contains(@class,'wpb_wrapper')]//small[contains(.,'Terms of Service According to Solution Type:')]")
	public WebElement text_heading_termsofservicepage;
	
	@FindBy(xpath="//div[contains(@class,'wpb_wrapper')]//h2[contains(.,'Privacy Policy')]")
	public WebElement text_heading_privacypage;
	
	@FindBy(xpath="//*[@id='language-dropdown']/div/div/span")
	public WebElement text_heading_supportpage;
}
