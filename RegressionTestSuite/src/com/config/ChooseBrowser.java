package com.config;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ChooseBrowser {
	
	WebDriver Driver = null;
	
	public ChooseBrowser(WebDriver Driver){
		this.Driver = Driver;
	}
	
	public WebDriver OpenBrowser( String browserName, String url) throws Exception
	{
		if(browserName.equalsIgnoreCase("FF") || browserName.equalsIgnoreCase("firefox"))
		{
			Driver = new FirefoxDriver();
		} 
		else if(browserName.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\src\\Resources\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-extensions");
			Driver = new ChromeDriver(options);
		} 
		else if(browserName.equals("IE"))
		{
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\src\\Resources\\IEDriverServer.exe");
			Driver = new InternetExplorerDriver();
		}
		else
		{
			Driver = new FirefoxDriver();
		}
		
		Driver.manage().window().maximize();
		Thread.sleep(1000);
		Driver.manage().deleteAllCookies();
		Thread.sleep(5000);
		Driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Driver.navigate().to(url);
		
		return Driver;	
	}
	public WebDriver SignOutFromMeetingRoom(WebDriver Driver) throws Exception{
		
		try
		{
			//boolean isLeaveMeetingVisible = Driver.findElement(By.xpath("//*[@id='endMeetingMsg']")).isDisplayed();
			Driver.findElement(By.xpath("//a[@id='roomControl']")).click();
			Thread.sleep(1000);
			Driver.findElement(By.xpath("//*[@id='endMeetingMsg']")).click();
			Thread.sleep(1000); 
			Driver.findElement(By.xpath("//*[@id='signOutDiv']/label")).click();
			Thread.sleep(1000); 
			Driver.findElement(By.xpath("//*[@id='leaveMeetingYesBtn']")).click();
			Thread.sleep(5000);
		}
		catch(Throwable t)
		{
			System.out.println("Error detail for sign out: " + t.getMessage());
		}
		
		return Driver;
	}
	
	public WebDriver GridOpenBrowser(String browserName, String platform) throws Exception
	{
		  
			new DesiredCapabilities();
		  DesiredCapabilities cap = DesiredCapabilities.firefox();
			if (browserName.equals("firefox"))
			{
				cap.setBrowserName("firefox");
			}
			else if(browserName.equals("chrome"))
			{
				DesiredCapabilities.chrome();
				cap.setBrowserName("chrome");
				cap.setPlatform(org.openqa.selenium.Platform.WINDOWS); 
				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\src\\Resources\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-extensions");
				Driver = new ChromeDriver(options);
			      
			}
			else if(browserName.equals("internet explorer"))
			{
				DesiredCapabilities.internetExplorer();
				cap.setBrowserName("internet explorer");
				cap.setPlatform(org.openqa.selenium.Platform.WINDOWS); 
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + "\\src\\Resources\\IEDriverServer.exe");
			}
			else
			{
				cap.setBrowserName("firefox");
			}
			return Driver;
			
			   //cap.setPlatform(Platform.WINDOWS);
			   
		/*  Host = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
		  Host.navigate().to(vUrl);
	      Thread.sleep(4000);*/
		  
	/*	  Host.manage().window().maximize();
		  Thread.sleep(2000);
		  Host.manage().deleteAllCookies();
		  Thread.sleep(5000);
		  Host.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		  Host.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		  Host.navigate().to(vUrl);
		  Thread.sleep(2000);	*/		
	  }
}
