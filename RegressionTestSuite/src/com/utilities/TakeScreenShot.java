package com.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TakeScreenShot {
	
	public void ScrShot(WebDriver dr,String fPath, String fPicName){
		File scrshot = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrshot, new File(fPath+"/"+fPicName+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
