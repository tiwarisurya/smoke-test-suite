package com.utilities;

import java.time.LocalDateTime;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.asserts.SoftAssert;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;

public class CommonFunctions {
	
	
	SoftAssert s_assert = new SoftAssert();
	TakeScreenShot sshot = new TakeScreenShot();
	
	public void Guest_LogIn_Option(WebDriver driver, String sPath, String PicName)
	{
		iMeetLogin_Page login = new iMeetLogin_Page(driver);

		try
		{
			login.waitForElement(driver, login.link_UseTheBrowser, 20);
			if(login.link_UseTheBrowser.isDisplayed())
			{
				login.link_UseTheBrowser.click();
			}
			else if(login.link_JoinUsingBrowser.isDisplayed())
			{
				login.link_JoinUsingBrowser.click();
			}
			else
			{
				System.out.println(PicName + " Guest Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present");
				Reporter.log(PicName + " Guest Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present");
				s_assert.fail(PicName + " Guest Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present");
			}
		}
		catch(Throwable t)
		{
			sshot.ScrShot(driver, sPath,PicName+"_GuestLogIn.jpg");
			s_assert.fail(PicName + " Guest Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present from catch block");
		}
		
	}
	
	public void Guest_Enter_Password(WebDriver driver, String vPswd)
	{
		iMeetLogin_Page login = new iMeetLogin_Page(driver);
		try
		{
			//Enter password
			login.field_EnterPassword.clear();
			Thread.sleep(500);
			login.field_EnterPassword.sendKeys(vPswd);
			Thread.sleep(500);
			//Click on Sign In
			login.button_SignIn.click();
			Thread.sleep(500);
			login.waitForElement(driver, login.closeicon, 10);
		}
		catch(Exception ex)
		{
			ex.getMessage();	
	   	}
		
	}
	
	@SuppressWarnings("static-access")
	public String CurrentTime()
	{
		LocalDateTime x = LocalDateTime.now();
		String RightNow = x.now().toString();
		RightNow = RightNow.replaceAll("\\:", "-").replaceAll("\\.", "-");
		return RightNow;	
	}
	
	public void Host_Enter_Password(WebDriver driver, String vPswd, String TestCaseName)
	{
		iMeetLogin_Page login = new iMeetLogin_Page(driver);
		try
		{
			//Enter password
			Thread.sleep(1000);
			login.field_EnterPassword.clear();
			Thread.sleep(500);
			login.field_EnterPassword.sendKeys(vPswd);
			Thread.sleep(500);
			//Click on Sign In
			login.button_SignIn.click();
			Thread.sleep(500);
			login.waitForElement(driver, login.closeicon, 10);
		}
		catch(Exception ex)
		{
			Reporter.log(TestCaseName + "_Host Password Field exception is " + ex.getMessage());
			s_assert.fail(TestCaseName + " Test case Host not able to login");
			throw new SkipException(TestCaseName + " Test Case Skipping. Because Host failed to log in to the meeting room.");
		}
	}
	
	public void Host_LogIn_Option(WebDriver driver, String sPath, String PicName)
	{
		iMeetLogin_Page login = new iMeetLogin_Page(driver);

		try
		{
			login.waitForElement(driver, login.link_UseTheBrowser, 20);
			if(login.link_UseTheBrowser.isDisplayed())
			{
				login.link_UseTheBrowser.click();
			}
			else if(login.link_JoinUsingBrowser.isDisplayed())
			{
				login.link_JoinUsingBrowser.click();
			}
			else
			{
				System.out.println(PicName + " Host Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present");
				Reporter.log(PicName + " Host Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present");
				s_assert.fail(PicName + " Host Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present");
			}
		}
		catch(Throwable t)
		{
			sshot.ScrShot(driver, sPath,PicName+"_HostLogIn.jpg");
			s_assert.fail(PicName + " Host Login page not completely loaded or the links UseTheBrowser and JoinUsingBrowser Not Present. Catch block");
		}
		
	}
	
	public void Guest_Audio_Panel_Display(WebDriver driver, String sPath, String PicName) throws Exception
	{
		
		iMeetRoom_Page imeet_Room = new iMeetRoom_Page(driver);
		Thread.sleep(2000);
		try
		{
			//Verify Audio Connect panel
			imeet_Room.waitForElement(driver, imeet_Room.closeicon, 20);
			//Close audio connect panel
			imeet_Room.closeicon.click();
			Thread.sleep(1000);
			imeet_Room.waitForElement(driver, imeet_Room.participants_button, 5);
		}
		catch(Throwable t) 
		{
			sshot.ScrShot(driver, sPath, PicName+"_GuestAudio Control.jpg");
			s_assert.fail("Guest Audio Control not displayed. Log is " + t.getMessage());
		}
		//return driver;
		
	}
	
	
	public boolean Host_Audio_Panel_Display(WebDriver driver, String sPath, String PicName, boolean isHostLogedIn) throws Exception
	{
		isHostLogedIn = false;
		iMeetRoom_Page imeet_Room = new iMeetRoom_Page(driver);
		Thread.sleep(2000);
		try
		{
			//Verify Audio Connect panel
			imeet_Room.waitForElement(driver, imeet_Room.closeicon, 20);
			//Close audio connect panel
			imeet_Room.closeicon.click();
			Thread.sleep(1000);
			imeet_Room.waitForElement(driver, imeet_Room.participants_button, 5);
			isHostLogedIn = true;
		}
		catch(Throwable t) 
		{
			sshot.ScrShot(driver, sPath, PicName+"_HostAudio Control.jpg");
			s_assert.fail("Audio Control not displayed. Log is " + t.getMessage());
		}
		
		return isHostLogedIn;

	}
	
	
	
	
	

} // Class Close
