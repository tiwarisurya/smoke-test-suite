package com.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;

import org.testng.annotations.Test;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ReadData {
		
		//Get the current directory
		public static String getTestDataDirectory(String filename)
		{
			Path currentRelativePath = Paths.get("");
			String s = currentRelativePath.toAbsolutePath().toString();
			String workingpath = s +"\\src\\Resources\\"+filename;
			return(workingpath);
		}
		
		//Get the screenshot directory
		public static String getScreenshotDirectory()
		{
			Path currentRelativePath = Paths.get("");
			String s = currentRelativePath.toAbsolutePath().toString();
			String workingpath = s +"\\Screenshot";
			return(workingpath);
		}
}
