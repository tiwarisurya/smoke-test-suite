package com.utilities;

public class TestData {
	
	public String BrowserType = "ff";
	public String Url = "https://beta2.imeetbeta.net/testurl/surya?html=true";             
	public String NewUrl = "https://beta2.imeetbeta.net/testurl/surya123?html=true";  
	public String HostFirstName = "surya";
	public String HostlastName = "tiwari";
	public String HostEmail = "surya.tiwari@nagarro.com";
	public String NewHostEmail = "123surya.tiwari@nagarro.com";
	public String GuestFirstName = "Guestfname";
	public String GuestLastName = "GuestlName";
	public String GuestEmail = "dfghjk789tt@nagarro.com";
	public String Pswd = "myimeet123";
	public String UseBrowser_Text = "Use the browser";
	public String DownloadiMeet_App_Text = "Download iMeet App";
	public String RegisteredGuestEmail	= "test.user@test.com";
	public String GuestPswd	= "myimeet123"; // "myimeet123";
	public String NewGuestPswd	= "myimeet133"; 
	public String NewRoomUrl	= "surya123"; 
	public String RoomUrl	= "surya"; 
	public String RoomKey = "1234";
	public String LockedMeetingMessage_KEY = "This meeting is locked. Please enter the room key to continue.";
	public String MeetingName = "Surya's Meeting Room";
	public String FileName_MP4 = "Wildlife_mp4.mp4"; //"imeetmeeting.mp4";
}
