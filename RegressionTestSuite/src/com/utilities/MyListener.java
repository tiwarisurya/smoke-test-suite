package com.utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

/*
 * in main function use the next 3 lines which is responsible for calling the listener
EventFiringWebDriver dr = new EventFiringWebDriver(dr1);
MyListener listener = new MyListener();
dr.register(listener);*/


public class MyListener extends AbstractWebDriverEventListener {
	
	public void OnError (WebDriver driver, String fPath, String fPicName){
		System.out.println("Listener being called on error.");
		File scrshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrshot, new File(fPath+"/"+fPicName+".jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
