package com.client.cube;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8474_Cube_10 {
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8474_Cube_10";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd;
	String vhostfirstName, vhostlastName, vHostEmail;
	int totalcontactnumber, totalcontactnumberafterdelete;
	Random r = new Random();
	int number = r.nextInt(1000000000);
	String num = Integer.toString(number);
		
	@Test //IM_8474_Cube_10
	public void IM_8474_Cube_10() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			//Add numbers to the profile
			try
			{				
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Edit Profile	
				imeet_Room.edit_profile.click();
				//Click on Details
				imeet_Room.edit_profile_details.click();
				//Total number already added in this profile 
				
				totalcontactnumber = host.findElements(By.xpath("//div[contains(@id,'autofillPhoneEntry')]//a")).size();
				System.out.println("Total contact number..." + totalcontactnumber);
				if(totalcontactnumber==3)
				{
					System.out.println("Three numbers are already added, no more contacts can be added.");
				}
				else if (totalcontactnumber<3)
				{
					for(int i=totalcontactnumber; i<3; i++)
					{
						//Click on Add icon
						host.findElement(By.xpath("//a[contains(@class,'addPhoneField')]")).click();
						Thread.sleep(2000);
						
						
						//Select country
						new Select(host.findElement(By.id("phoneFieldsCountry"))).selectByVisibleText("United States of America, including U.S. territories(1)");
						Thread.sleep(2000);
						
						
						//Enter phone number
						host.findElement(By.xpath("//*[@id='addPhoneNumber']")).clear();
						host.findElement(By.xpath("//*[@id='addPhoneNumber']")).sendKeys(num+i);
						Thread.sleep(2000);
						
						
						//Enter phone number extension
						host.findElement(By.xpath("//*[@id='addPhoneExt']")).clear();
						host.findElement(By.xpath("//*[@id='addPhoneExt']")).sendKeys("11");
						Thread.sleep(2000);
						
						//Click on Add
						host.findElement(By.xpath("//*[@id='addPhoneEntry']")).click();
						
					}
					//Click on Done
					Thread.sleep(2000);
					host.findElement(By.xpath("//*[@id='btnSaveForProfileDetail']")).click();
				}
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
			//Edit existing profile number 
			try
			{				
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Edit Profile	
				imeet_Room.edit_profile.click();
				//Click on Details
				imeet_Room.edit_profile_details.click();
				
				//Click on number Drop down
				Thread.sleep(800);
				host.findElement(By.xpath("//*[@id='primaryUserContactNum']")).click();
				
				//Click on Edit
				Thread.sleep(800);
				host.findElement(By.xpath("//div[contains(@id,'autofillPhoneEntry')]//a[1]")).click();
				
				//Enter phone number extension
				Thread.sleep(800);
				host.findElement(By.xpath("//*[@id='addPhoneExt']")).clear();
				host.findElement(By.xpath("//*[@id='addPhoneExt']")).sendKeys("12");
				Thread.sleep(2000);
				
				//Click on Save
				host.findElement(By.xpath("//*[@id='savePhoneEntry']")).click();
				
				//Click on number Drop down
				Thread.sleep(800);
				host.findElement(By.xpath("//*[@id='primaryUserContactNum']")).click();
				
				//Verify the changes
				System.out.println("Stored extention code.." + host.findElement(By.xpath("//*[@id='phone1P']")).getText());
				s_assert.assertTrue(host.findElement(By.xpath("//*[@id='phone1P']")).getText().contains("12"), "Extension code is not updated");
				
				//Click on Done
				Thread.sleep(2000);
				host.findElement(By.xpath("//*[@id='btnSaveForProfileDetail']")).click();
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
			
			//Delete existing profile number			
			try
			{				
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Edit Profile	
				imeet_Room.edit_profile.click();
				//Click on Details
				imeet_Room.edit_profile_details.click();
				
				//Store total number of contact before perform delete action
				int totalcontactnumberbeforedelete = host.findElements(By.xpath("//div[contains(@id,'autofillPhoneEntry')]//a")).size();
				
				//Click on number Drop down
				Thread.sleep(800);
				host.findElement(By.xpath("//*[@id='primaryUserContactNum']")).click();
				
				//Click on Edit
				Thread.sleep(800);
				host.findElement(By.xpath("//div[contains(@id,'autofillPhoneEntry')]//a[1]")).click();
				
				//Click on Delete number
				Thread.sleep(800);
				host.findElement(By.xpath("//*[@id='delPhoneEntry']")).click();
				
				//Verify number is deleted.
				totalcontactnumberafterdelete = host.findElements(By.xpath("//div[contains(@id,'autofillPhoneEntry')]//a")).size();
				System.out.println("totalcontactnumberafterdelete..." + totalcontactnumberafterdelete + "totalcontactnumberbeforedelete" + totalcontactnumberbeforedelete);
				s_assert.assertTrue((totalcontactnumberafterdelete<totalcontactnumberbeforedelete),"Contact number is not deleted successfully");	
				
				//Click on Done
				Thread.sleep(2000);
				host.findElement(By.xpath("//*[@id='btnSaveForProfileDetail']")).click();
				
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	       }
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8474_Cube_10 close
	
} //main close