package com.client.login;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.Login_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class IM_10530_Login_05 {
 
	
	public WebDriver driver ;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_10530_Login_05";
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
		
	@Test //To verify the functionality when user selects option to "join meeting using the browser" on the Client Selection Panel.
	public void IM_10530_Login_05() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		
		try
		{
			//Choose Browser and open url
			ChooseBrowser chooseBrowser = new ChooseBrowser(driver);
			driver = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			Login_Page login = new Login_Page(driver);
			
			//Switch frame if required
			Thread.sleep(8000);
			login.switchFrame(driver);
			
			//Click on Use the Browser
			login.link_UseTheBrowser.click();
			
			Thread.sleep(8000);
			//Verify First name field
			s_assert.assertTrue(login.field_EnterFirstName.isDisplayed(), "First name input field not displayed on Client selection panel");
			
			//Verify last name field
			s_assert.assertTrue(login.field_EnterLastName.isDisplayed(), "Last name input field not displayed on Client selection panel");
			
			//Verify Email field
			s_assert.assertTrue(login.field_EnterEmail.isDisplayed(), "Email input field not displayed on Client selection panel");
			
			//Verify Join Meeting button
			s_assert.assertTrue(login.button_Submit.isDisplayed(), "Join Meeting button not displayed on Client selection panel");
			
			//Verify Keep me sign in checkbox
			s_assert.assertTrue(login.checkbox_keepmesignedIn.isDisplayed(), "Keep me signed In, check box not displayed on Client selection panel");
		
		}
		catch (Throwable t)
		{
			sshot.ScrShot(driver, sPath, PicName+".jpg");
			s_assert.fail("Error occured. plz check the report log");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if (driver != null)
			{
				driver.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_10530Login_05 close
	
} //main close