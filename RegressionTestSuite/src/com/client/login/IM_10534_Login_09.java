package com.client.login;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.Login_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class IM_10534_Login_09 {
 
	
	public WebDriver driver ;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_10534_Login_09";
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String useBrowser_Text, downloadiMeet_App_Text;
		
	@Test //Verify language selector is displayed on the Client Selection Panel.
	public void IM_10534_Login_09() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		
		try
		{
			//Choose Browser and open url
			ChooseBrowser chooseBrowser = new ChooseBrowser(driver);
			driver = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			Login_Page login = new Login_Page(driver);
			
			//Switch frame if required
			Thread.sleep(1000);
			login.switchFrame(driver);
			
			//...................................DEUTSCH language.................................................
			
			//Select DEUTSCH language
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]/ul/li[contains(.,'DEUTSCH')]")).click();
			Thread.sleep(7000);
			
		    //Verify "Browser benutzen" text
			String text_useTheBrowser_DEUTSCH = login.link_UseTheBrowser.getText();
			s_assert.assertTrue(text_useTheBrowser_DEUTSCH.contains("Browser benutzen"), "'Use the browser' button is not visible in DEUTSCH language");
			
			//Verify "iMeet App herunterladen" text
			String text_download_imeet_app_DEUTSCH = login.link_DownloadiMeetapp.getText();
			s_assert.assertTrue(text_download_imeet_app_DEUTSCH.contains("iMeet App herunterladen"), "'Download iMeet app' button is not visible in DEUTSCH language");
			
			//Verify Text "Nutzen Sie Ihren Webbrowser und sprechen Sie mittels Ihres Telefons im Meeting."
			String text_useWebBrowser_DEUTSCH = login.text_UseYourWebBrowser.getText();
			s_assert.assertTrue(text_useWebBrowser_DEUTSCH.contains("Nutzen Sie Ihren Webbrowser und sprechen Sie mittels Ihres Telefons im Meeting."), "Text 'Use your browser and talk in the meeting...' is not visible in DEUTSCH language");
			
			//Verify text "Laden Sie die iMeet Anwendung herunter und sprechen Sie mittels Ihres Computers und VoIP im Meeting"
			String text_downloadimeetappusingcomputer_DEUTSCH = login.text_DownloadappandTalkinMeeting.getText();
			s_assert.assertTrue(text_downloadimeetappusingcomputer_DEUTSCH.contains("Laden Sie die iMeet Anwendung herunter und sprechen Sie mittels Ihres Computers und VoIP im Meeting"), "Text 'Download iMeet app and talk in the meeting...' is not visible in DEUTSCH language");
			
			//Verify "Ich habe die Anwendung bereits" text.
			String text_I_alreadyhaveapp_DEUTSCH = login.text_IAlreadyHaveapp.getText();
			s_assert.assertTrue(text_I_alreadyhaveapp_DEUTSCH.contains("Ich habe die Anwendung bereits"), "Link 'I already have this app' is not visible in DEUTSCH language");
			
			//Verify "Nutzungsbedingungen" text 
			String text_TermsofService_DEUTSCH = login.link_TermsofService.getText();
			s_assert.assertTrue(text_TermsofService_DEUTSCH.contains("Nutzungsbedingungen"), "Link 'Terms of Service' is not visible in DEUTSCH language");
			
			//Verify "Datenschutzerklärung" text
			String text_PrivacyStatement_DEUTSCH = login.link_privacy_statement.getText();
			s_assert.assertTrue(text_PrivacyStatement_DEUTSCH.contains("Datenschutzerklärung"), "link 'Privacy Statement' is not visible in DEUTSCH language");
			
			//Verify "Hilfe" text
			String text_Support_DEUTSCH = login.link_Support.getText();
			s_assert.assertTrue(text_Support_DEUTSCH.contains("Hilfe"), "Link 'Support' is not visible in DEUTSCH language");
			
			
			//...................................... 日本語...................................
			
			//Select 日本語 language
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]/ul/li[contains(.,'日本語')]")).click();
			//Verify selected language
			Thread.sleep(7000);
		    //Verify "ブラウザを使用" text
			String text_useTheBrowser_日本語 = login.link_UseTheBrowser.getText();
			s_assert.assertTrue(text_useTheBrowser_日本語.contains("ブラウザを使用"), "'Use the browser' button is not visible in Chinese language");
			
			//Verify "iMeet アプリのダウンロード" text
			String text_download_imeet_app_日本語 = login.link_DownloadiMeetapp.getText();
			s_assert.assertTrue(text_download_imeet_app_日本語.contains("iMeet アプリのダウンロード"), "'Download iMeet app' button is not visible in Chinese language");
			
			//Verify Text "Web ブラウザから入室し、音声は電話を使用"
			String text_useWebBrowser_日本語 = login.text_UseYourWebBrowser.getText();
			s_assert.assertTrue(text_useWebBrowser_日本語.contains("Web ブラウザから入室し、音声は電話を使用"), "Text 'Use your browser and talk in the meeting...' is not visible in Chinese language");
			
			//Verify text "iMeet アプリをダウンロードし、VoIP で音声接続"
			String text_downloadimeetappusingcomputer_日本語 = login.text_DownloadappandTalkinMeeting.getText();
			s_assert.assertTrue(text_downloadimeetappusingcomputer_日本語.contains("iMeet アプリをダウンロードし、VoIP で音声接続"), "Text 'Download iMeet app and talk in the meeting...' is not visible in Chinese language");
			
			//Verify "すでにアプリを持っています" text.
			String text_I_alreadyhaveapp_日本語 = login.text_IAlreadyHaveapp.getText();
			s_assert.assertTrue(text_I_alreadyhaveapp_日本語.contains("すでにアプリを持っています"), "Link 'I already have this app' is not visible in Chinese language");
			
			//Verify "ご利用規約" text 
			String text_TermsofService_日本語 = login.link_TermsofService.getText();
			s_assert.assertTrue(text_TermsofService_日本語.contains("ご利用規約"), "Link 'Terms of Service' is not visible in v language");
			
			//Verify "個人情報保護方針" text
			String text_PrivacyStatement_日本語 = login.link_privacy_statement.getText();
			s_assert.assertTrue(text_PrivacyStatement_日本語.contains("個人情報保護方針"), "link 'Privacy Statement' is not visible in Chinese language");
			
			//Verify "サポート" text
			String text_Support_日本語 = login.link_Support.getText();
			s_assert.assertTrue(text_Support_日本語.contains("サポート"), "Link 'Support' is not visible in Chinese language");
			
			//...................................FRANÇAIS................................
			
			//Select FRANÇAIS language
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]/ul/li[contains(.,'FRANÇAIS')]")).click();
			Thread.sleep(7000);
		    //Verify "Utiliser le navigateur" text
			String text_useTheBrowser_FRANÇAIS = login.link_UseTheBrowser.getText();
			s_assert.assertTrue(text_useTheBrowser_FRANÇAIS.contains("Utiliser le navigateur"), "'Use the browser' button is not visible in FRANÇAIS language");
			
			//Verify "Télécharger l'appli iMeet" text
			String text_download_imeet_app_FRANÇAIS = login.link_DownloadiMeetapp.getText();
			s_assert.assertTrue(text_download_imeet_app_FRANÇAIS.contains("Télécharger l'appli iMeet"), "'Download iMeet app' button is not visible in FRANÇAIS language");
			
			//Verify Text "Veuillez utiliser votre navigateur web, et en même temps parlez dans la"
			String text_useWebBrowser_FRANÇAIS = login.text_UseYourWebBrowser.getText();
			s_assert.assertTrue(text_useWebBrowser_FRANÇAIS.contains("Veuillez utiliser votre navigateur web, et en même temps parlez dans la"), "Text 'Use your browser and talk in the meeting...' is not visible in FRANÇAIS language");
			
			//Verify text "Veuillez télécharger l’App iMeet pour parler dans la conférence via votre ordinateur."
			String text_downloadimeetappusingcomputer_FRANÇAIS = login.text_DownloadappandTalkinMeeting.getText();
			s_assert.assertTrue(text_downloadimeetappusingcomputer_FRANÇAIS.contains("Veuillez télécharger l’App iMeet pour parler dans la conférence via votre ordinateur."), "Text 'Download iMeet app and talk in the meeting...' is not visible in FRANÇAIS language");
			
			//Verify "Je possède déjà l'appli" text.
			String text_I_alreadyhaveapp_FRANÇAIS = login.text_IAlreadyHaveapp.getText();
			s_assert.assertTrue(text_I_alreadyhaveapp_FRANÇAIS.contains("Je possède déjà l'appli"), "Link 'I already have this app' is not visible in FRANÇAIS language");
			
			//Verify "Conditions de service" text 
			String text_TermsofService_FRANÇAIS = login.link_TermsofService.getText();
			s_assert.assertTrue(text_TermsofService_FRANÇAIS.contains("Conditions de service"), "Link 'Terms of Service' is not visible in FRANÇAIS language");
			
			//Verify "Politique de confidentialité" text
			String text_PrivacyStatement_FRANÇAIS = login.link_privacy_statement.getText();
			s_assert.assertTrue(text_PrivacyStatement_FRANÇAIS.contains("Politique de confidentialité"), "link 'Privacy Statement' is not visible in FRANÇAIS language");
			
			//Verify "Assistance" text
			String text_Support_FRANÇAIS = login.link_Support.getText();
			s_assert.assertTrue(text_Support_FRANÇAIS.contains("Assistance"), "Link 'Support' is not visible in FRANÇAIS language");
			
			//.................................. ENGLISH ..................................
			
			//Select ENGLISH language
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]")).click();
			Thread.sleep(3000);
			driver.findElement(By.xpath("//div[contains(@class,'select-styled')]/ul/li[contains(.,'ENGLISH')]")).click();
			Thread.sleep(7000);
		    //Verify "Use the browser" text
			String text_useTheBrowser_ENGLISH = login.link_UseTheBrowser.getText();
			s_assert.assertTrue(text_useTheBrowser_ENGLISH.contains("Use the browser"), "'Use the browser' button is not visible in ENGLISH language");
			
			//Verify "Download iMeet App" text
			String text_download_imeet_app_ENGLISH = login.link_DownloadiMeetapp.getText();
			s_assert.assertTrue(text_download_imeet_app_ENGLISH.contains("Download iMeet App"), "'Download iMeet app' button is not visible in ENGLISH language");
			
			//Verify Text "Use your web browser and talk in the meeting using your phone."
			String text_useWebBrowser_ENGLISH = login.text_UseYourWebBrowser.getText();
			s_assert.assertTrue(text_useWebBrowser_ENGLISH.contains("Use your web browser and talk in the meeting using your phone."), "Text 'Use your browser and talk in the meeting...' is not visible in ENGLISH language");
			
			//Verify text "Download the iMeet app and talk in the meeting using your computer."
			String text_downloadimeetappusingcomputer_ENGLISH = login.text_DownloadappandTalkinMeeting.getText();
			s_assert.assertTrue(text_downloadimeetappusingcomputer_ENGLISH.contains("Download the iMeet app and talk in the meeting using your computer."), "Text 'Download iMeet app and talk in the meeting...' is not visible in ENGLISH language");
			
			//Verify "I already have the app" text.
			String text_I_alreadyhaveapp_ENGLISH = login.text_IAlreadyHaveapp.getText();
			s_assert.assertTrue(text_I_alreadyhaveapp_ENGLISH.contains("I already have the app"), "Link 'I already have this app' is not visible in ENGLISH language");
			
			//Verify "Terms of Service" text 
			String text_TermsofService_ENGLISH = login.link_TermsofService.getText();
			s_assert.assertTrue(text_TermsofService_ENGLISH.contains("Terms of Service"), "Link 'Terms of Service' is not visible in ENGLISH language");
			
			//Verify "Privacy Statement" text
			String text_PrivacyStatement_ENGLISH = login.link_privacy_statement.getText();
			s_assert.assertTrue(text_PrivacyStatement_ENGLISH.contains("Privacy Statement"), "link 'Privacy Statement' is not visible in ENGLISH language");
			
			//Verify "Support" text
			String text_Support_ENGLISH = login.link_Support.getText();
			s_assert.assertTrue(text_Support_ENGLISH.contains("Support"), "Link 'Support' is not visible in ENGLISH language");	
		}
		catch (Throwable t)
		{
			sshot.ScrShot(driver, sPath, PicName+".jpg");
			s_assert.fail("Error occured. plz check the report log");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if (driver != null)
			{
				driver.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_10534_Login_09 close
	
} //main close