package com.client.login;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.Login_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class IM_25893_Login_11 {
 
	
	public WebDriver driver ;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_25893_Login_11";
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
		
	@Test //IM_25893_Login_11 Verify iMeet logo on landing page
	public void IM_25893_Login_11_verifyiMeetLogo() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		
		try
		{
			//Choose Browser and open url
			ChooseBrowser chooseBrowser = new ChooseBrowser(driver);
			driver = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			Login_Page login = new Login_Page(driver);
			
			//Switch frame if required
			Thread.sleep(1000);
			login.switchFrame(driver);
					
			//Verify iMeet logo on Client Selection Panel.
			Thread.sleep(10000);
			s_assert.assertTrue(login.logo_iMeet.isDisplayed(), "iMeet logo not displayed on Client selection panel");
		}
		catch (Throwable t)
		{
			sshot.ScrShot(driver, sPath, PicName+".jpg");
			s_assert.fail("Error occured. plz check the report log");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if (driver != null)
			{
				driver.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_25893_Login_11 close
	
} //main close