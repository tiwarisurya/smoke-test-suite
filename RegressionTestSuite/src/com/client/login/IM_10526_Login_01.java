package com.client.login;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.config.ChooseBrowser;
import com.pageobject.Login_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class IM_10526_Login_01 {
 
	
	public WebDriver driver ;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_10526_Login_01";
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String useBrowser_Text, downloadiMeet_App_Text;
		
	@Test //IM_10526_Login_01 Verify the landing page
	public void IM_10526_Login_01_verifyLandingPage() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		useBrowser_Text = d.UseBrowser_Text;
		downloadiMeet_App_Text = d.DownloadiMeet_App_Text;
		
		try
		{
			//Choose Browser and open url
			ChooseBrowser chooseBrowser = new ChooseBrowser(driver);
			driver = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			Login_Page login = new Login_Page(driver);
			
			//Switch frame if required
			Thread.sleep(18000);
			login.switchFrame(driver);
					
			//Verify Client Selection Panel should be displayed with option to login using browser web client or to download the iMeet app.
			String actual = login.link_UseTheBrowser.getText();
			String actualapp = login.link_DownloadiMeetapp.getText();			
			
			s_assert.assertEquals(actual, useBrowser_Text, "'Use the browser link' not shown on the page.");
			s_assert.assertEquals(actualapp, downloadiMeet_App_Text, "'Download iMeet App' not shown on the page.");

            //Verify The option to login using browser web client should be emphasized.
			String emphasized_button = login.link_UseTheBrowser.getAttribute("class");
			s_assert.assertEquals(emphasized_button, "blue-button-wide", "'login using browser' is not emphasized");
		}
		catch (Throwable t)
		{
			sshot.ScrShot(driver, sPath, PicName+".jpg");
			s_assert.fail("Error occured. plz check the report log");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if (driver != null)
			{
				driver.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_10526_Login_01 close
	
} //main close