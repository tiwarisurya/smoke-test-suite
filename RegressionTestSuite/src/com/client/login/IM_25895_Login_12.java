package com.client.login;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.Login_Page;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;


public class IM_25895_Login_12 {
 
	
	public WebDriver driver ;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();	
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_25895_Login_12";
	
	String TestCaseName, vPswd, vBrowserType, vUrl;
	String useBrowser_Text, downloadiMeet_App_Text;
		
	@Test //IM_25895_Login_12 Verify "Term of Service","Privacy policy","Support" footers on the Client Selection Panel.
	public void IM_25895_Login_12() throws IOException, Exception
	{
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		useBrowser_Text = d.UseBrowser_Text;
		downloadiMeet_App_Text = d.DownloadiMeet_App_Text;
		
		try
		{
			//Choose Browser and open url
			ChooseBrowser chooseBrowser = new ChooseBrowser(driver);
			driver = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			Login_Page login = new Login_Page(driver);
			
			//Switch frame if required
			Thread.sleep(1000);
			login.switchFrame(driver);
			
			//Verify user is able to access "Term of Service","Privacy policy" and "Support" footer links on the Client Selection Panel.
			Thread.sleep(10000);
			s_assert.assertTrue(login.link_TermsofService.isDisplayed(), "Terms of Service link not displayed on Client selection panel");
			s_assert.assertTrue(login.link_privacy_statement.isDisplayed(), "Privacy Statement link not displayed on Client selection panel");
			s_assert.assertTrue(login.link_Support.isDisplayed(), "Support link not displayed on Client selection panel");

			//Verify, clicking on "Term of Service" will redirect to "https://www.pgi.com/terms-of-service/"
			
			// Store the current window handle
			String winHandleParent = driver.getWindowHandle();
			//Click on Terms of Service link.
			login.link_TermsofService.click();

			// Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}
			System.out.println("driver.getCurrentUrl();" + driver.getCurrentUrl());
			s_assert.assertEquals(driver.getCurrentUrl(), "https://www.pgi.com/terms-of-service/", "User is not redirected to corresponding terms of service window");
			
			
			//Verify some checkpoints on the redirected pages.
            driver.close();
            driver.switchTo().window(winHandleParent);

			//Verify clicking on "Privacy policy" will redirect to "https://www.pgi.com/privacy-policy/"
			login.link_privacy_statement.click();
			// Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			driver.switchTo().window(winHandle);
			}
			System.out.println("driver.getCurrentUrl();" + driver.getCurrentUrl());
			s_assert.assertEquals(driver.getCurrentUrl(), "https://www.pgi.com/privacy-policy/", "User is not redirected to privacy policy");
			
			//Verify some checkpoints on the redirected pages.
			
			
			driver.close();
			driver.switchTo().window(winHandleParent);			
			
			//Verify, clicking on "Support" will redirect to "https://imeetcommunity.mymeetinghelp.com/hc/en-us"
			login.link_Support.click();
			// Switch to new window opened
			for(String winHandle : driver.getWindowHandles()){
			driver.switchTo().window(winHandle);
			}
			System.out.println("driver.getCurrentUrl();" + driver.getCurrentUrl());
			s_assert.assertEquals(driver.getCurrentUrl(), "https://assets.imeetbeta.net/support", "User is not redirected to support page");		
			
			//Verify some checkpoints on the redirected pages.
			
			
		}
		catch (Throwable t)
		{
			sshot.ScrShot(driver, sPath, PicName+".jpg");
			s_assert.fail("Error occured. plz check the report log");
			Reporter.log(t.getMessage());
		}
		finally
		{
			if (driver != null)
			{
				driver.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_25895_Login_12 close
	
} //main close