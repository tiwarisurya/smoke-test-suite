package com.client.fileshare;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8371_FS_15 {
	
	public WebDriver host;
	public WebDriver guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8371_FS_15";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName, vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, vFilename_MP4, mp4filename;
		
	@Test //Guest upload file in Host File cabinet
	public void IM_8371_FS_15Guest_upload_fileHostCabinet() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		vFilename_MP4 = d.FileName_MP4.toUpperCase();
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Files
				imeet_Room.setting_files.click();

				Thread.sleep(1000);
				//Click on Allow Guest upload
				String status_guestupload = imeet_Room.setting_file_upload.getAttribute("class");
				Thread.sleep(1000);
				if(StringUtils.containsIgnoreCase(status_guestupload, "turnOff"))
				{
					imeet_Room.setting_file_upload.click();
					Thread.sleep(800);
				}
				//Click on Done
				imeet_Room.setting_meeting_roomkey_Done.click();
				Thread.sleep(5000);	
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	        //Login with Guest
			System.out.println("Now log in guest");
		
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(guest);
			guest = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
		
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
				
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
					
			//Login with Host
			login1.field_EnterEmail.sendKeys(vGuestEmail);
					
			//Click on Join Meeting
			login1.button_Submit.click();
			imeet_Room1.waitForElement(guest, login1.field_EnterPassword, 5);
			common.Guest_Enter_Password(guest, vPswd);
				
			//Verify Audio Connect panel
			boolean isGuestLogedIn = false;
			isGuestLogedIn = common.Host_Audio_Panel_Display(guest, sPath, PicName, isGuestLogedIn);	
			
			//Click on share
			imeet_Room1.click_share.click();
			Thread.sleep(2000);
			
			//Click on share a file
			imeet_Room1.click_share_file.click();
			Thread.sleep(6000);
			
			//Verify that guest is able to upload file.
			//Click on Add new file
			imeet_Room1.click_addnew_file.click();
			Thread.sleep(5000);
		
			//Enter file name. File located in Resources Package
			String mp4filename = vFilename_MP4.toLowerCase();
			StringSelection sel = new StringSelection(ReadData.getTestDataDirectory(mp4filename));
		
			//Copy to clipboard
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(sel,null);
		
			//Create object of Robot class
			Robot robot = new Robot();
			Thread.sleep(1000);
		      
			// Press Enter
			robot.keyPress(KeyEvent.VK_ENTER);
		 
			// Release Enter
			robot.keyRelease(KeyEvent.VK_ENTER);
		 
			// Press CTRL+V
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
		 
			// Release CTRL+V
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			Thread.sleep(1000);
		        
			//Press Enter 
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		
			//***************************
			// Wait until file load
			Thread.sleep(5000);
			String FileInRoom = imeet_Room1.file_memory.getText();
			int FirstTimeFilenumber=0;
			String[] Part1, Part2;
			String File;
		
			boolean isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
			if(isFileNumberDisplay)
			{
				Part1 = FileInRoom.split(":");
				Part2 = Part1[1].split("\\(");
				File = Part2[0].trim();
				FirstTimeFilenumber = Integer.parseInt(File);	
			}

			int Filecount = FirstTimeFilenumber;
			int timer =1;
			while (timer < 60) // While loop wait up to 1 minute
			{
				Thread.sleep(1000);
				if(Filecount>FirstTimeFilenumber){break;}
				FileInRoom = imeet_Room1.file_memory.getText();
				isFileNumberDisplay = StringUtils.containsIgnoreCase(FileInRoom, "used");
				if(isFileNumberDisplay)
				{
					Part1 = FileInRoom.split(":");
					Part2 = Part1[1].split("\\(");
					File = Part2[0].trim();
					Filecount = Integer.parseInt(File);	
				}
				timer++;
			}
			//**************************
		
			//Verify newly uploaded file is in locked state
			Thread.sleep(4000);
			String uploaded_file_name1 = imeet_Room1.click_first_file.getText().toLowerCase();
			System.out.println("Uploaded file name " + uploaded_file_name1 + " where expected file name " + mp4filename);
			s_assert.assertEquals(uploaded_file_name1, mp4filename, "Uploaded file name " + uploaded_file_name1 + " where expected file name " + mp4filename );			
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8371_FS15 close
	
} //main close