package com.client.settings;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8427_SP_09 {
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8427_SP_09";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName, vHostEmail;
		
	@Test //IM-8427:SP_09_Change temperature and clock display from Meeting tab
	public void IM_8427_SP_09_Changetemperature() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vGuestPswd = d.GuestPswd;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(20000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(1000);
				//Check theTemperature format
				String status_temp_F = imeet_Room.setting_status_temp_F.getAttribute("checked");
				String status_temp_C = imeet_Room.setting_status_temp_C.getAttribute("checked");
				//Check the Clock format
				String status_clock_12 = imeet_Room.setting_status_clock_12.getAttribute("checked");
				String status_clock_24 = imeet_Room.setting_status_clock_24.getAttribute("checked");
				Thread.sleep(1000);
				System.out.println("status_temp_F...." + status_temp_F + "...status_temp_C..." + status_temp_C + "...status_clock_12.." + status_clock_12 + "..status_clock_24.." + status_clock_24);
				
				Boolean temp_F = false;
				Boolean temp_C = false;
				Boolean clock_12 = false;
				Boolean clock_24 = false;
				
				if(status_temp_F.contains("null"))
				{
					imeet_Room.setting_status_temp_F.click();
					temp_F = true;
					Thread.sleep(800);
				}
				
				if(status_temp_C.contains("null"))
				{
					imeet_Room.setting_status_temp_C.click();
					temp_C = true;
					Thread.sleep(800);
				}
				if(status_clock_12.contains("null"))
				{
					imeet_Room.setting_status_clock_12.click();
					clock_12 = true;
					Thread.sleep(800);
				}
				
				if(status_clock_24.contains("null"))
				{
					imeet_Room.setting_status_clock_24.click();
					clock_24 = true;
					Thread.sleep(800);
				}
				
				System.out.println("..temp_F.." + temp_F + "...temp_C.." + temp_C + "...clock_12.." + clock_12 + "...clock_24.." + clock_24);
				
				//Click on Done
				imeet_Room.setting_meeting_roomkey_Done.click();
				Thread.sleep(1000);
				String clockformat = imeet_Room.room_clock_type.getText();
				String tempformat = imeet_Room.room_temp_type.getText();
				if(temp_F == true)
				{
					//Verify  
					s_assert.assertTrue(StringUtils.containsIgnoreCase(tempformat, "F"));
					
				}
				else if(temp_C == true)
				{
					//Verify	
					s_assert.assertTrue(StringUtils.containsIgnoreCase(tempformat, "C"));
				}
				if(clock_12 == true)
				{
					//Verify 
					s_assert.assertTrue(StringUtils.containsIgnoreCase(clockformat, "M"));
				}
				else if(clock_24 == true)
				{
					//Verify 
					s_assert.assertFalse(imeet_Room.room_clock_type.isDisplayed());
				}
				
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	       	
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8427_SP_09 close
	
} //main close