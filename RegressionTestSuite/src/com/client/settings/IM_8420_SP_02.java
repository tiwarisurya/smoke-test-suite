package com.client.settings;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8420_SP_02 {
	
	public WebDriver host;
	public WebDriver guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8420_SP_02";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName, vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, vRoomKey, vlockedMeetingMessage_key;
		
	@Test //Toggle Lock Meeting on and set room key from Meeting tab
	public void IM_8420_SP_02SettingRoomKey() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		vRoomKey = d.RoomKey;
		vlockedMeetingMessage_key = d.LockedMeetingMessage_KEY;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(1000);
				//Click on Lock Meeting room with key
				String status_Roomkey = imeet_Room.setting_meeting_lockMeeting_status.getAttribute("class");
				Thread.sleep(1000);
				if(StringUtils.containsIgnoreCase(status_Roomkey, "turnOff"))
				{
					imeet_Room.setting_meeting_lockMeeting_status.click();
					Thread.sleep(800);
					//Enter Room key
					System.out.println("going to clear room");
					imeet_Room.setting_meeting_roomkey.clear();
					Thread.sleep(800);
					imeet_Room.setting_meeting_roomkey.sendKeys(vRoomKey);
					Thread.sleep(800);
				}
				//Click on Done
				imeet_Room.setting_meeting_roomkey_Done.click();	
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	        //Login with Guest
			System.out.println("Now log in guest");
		
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(guest);
			guest = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
		
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
				
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
					
			//Login with Host
			login1.field_EnterEmail.sendKeys(vGuestEmail);
					
			//Click on Join Meeting
			login1.button_Submit.click();
			imeet_Room1.waitForElement(guest, login1.field_EnterPassword, 5);
			common.Guest_Enter_Password(guest, vPswd);
			
			try
			{			
			    //Verify meeting is locked.
				imeet_Room1.waitForElement(guest, imeet_Room1.locked_meeting_message_Key, 15);
				String lockedmeetingMessage = imeet_Room1.locked_meeting_message_Key.getText();
				System.out.println(" Lock meeting message " + lockedmeetingMessage);
				boolean isRoomKeyMsgDisplay = StringUtils.containsIgnoreCase(lockedmeetingMessage, vlockedMeetingMessage_key);
				s_assert.assertTrue(isRoomKeyMsgDisplay, "Failed. Expected Message is " + vlockedMeetingMessage_key + " where actual msg display " + lockedmeetingMessage);				
				//Revert the changes
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(800);
				//Click on Lock Meeting room with key
				imeet_Room.setting_meeting_lockMeeting_status.click();
				Thread.sleep(800);
				//Click on Done
				imeet_Room.setting_meeting_roomkey_Done.click();
				Thread.sleep(800);
				
				//Refresh the guest screen
				guest.navigate().refresh();
				
				// Use the browser- button
				imeet_Room1.waitForElement(guest, login1.link_UseTheBrowser, 20);
				login1.link_UseTheBrowser.click();
				
				//Verify Audio Connect panel
				boolean isGuestLogedIn = false;
				isGuestLogedIn = common.Host_Audio_Panel_Display(guest, sPath, PicName, isGuestLogedIn);
				if(isGuestLogedIn)
				{
					Reporter.log("Pass.Guest loged in after turn off the roomkey");
					System.out.println("Pass.Guest loged in after turn off the roomkey");
				}
				else
				{
					Reporter.log("Fail. Guest not loged in after turn off the roomkey");
					System.out.println("Fail. Guest not loged in after turn off the roomkey");
				}
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error occured to verify meering locked by room key. Detail " + t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8420_SP_02 close
	
} //main close