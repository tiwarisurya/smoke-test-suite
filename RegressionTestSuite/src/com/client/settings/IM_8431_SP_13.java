package com.client.settings;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8431_SP_13 {
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8431_SP_13";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName, vHostEmail;
		
	@Test //IM-8431:SP_13_Enable international audio access from Dialing tab
	public void IM_8431_SP_13EnableInternationalAccess() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vGuestPswd = d.GuestPswd;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(20000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Dialing tab
				imeet_Room.setting_dialing.click();
				Thread.sleep(1000);
				//Click on enable international dialing
				String status_internationaldialing = imeet_Room.setting_dialing_enableinternationaldialing.getAttribute("class");
				Thread.sleep(1000);
				if(StringUtils.containsIgnoreCase(status_internationaldialing, "turnOff"))
				{
					imeet_Room.setting_dialing_enableinternationaldialing.click();
					Thread.sleep(800);
				}
				//Click on Done
				imeet_Room.setting_meeting_roomkey_Done.click();
				Thread.sleep(5000);
				
				//Click on Connect
				imeet_Room.connect.click();
				//Click on Connect Audio
				imeet_Room.connect_connect_audio.click();
				//Click on Plus icon
				Thread.sleep(1000);
				imeet_Room.connectaudio_addnewnumber.click();
				Thread.sleep(2000);
				//Verify United States of America is shown in the international list.
				System.out.println("List of Countries is ..." + imeet_Room.international_access_numbers.getText());
				s_assert.assertTrue(imeet_Room.international_access_numbers.getText().contains("United States of America"), "United States of America not displayed in the list");	
				
				//Verify Australia is shown in the international list.
				s_assert.assertTrue(imeet_Room.international_access_numbers.getText().contains("Australia"), "Australia not displayed in the list");	
			
				//Verify Canada is shown in the international list.
				s_assert.assertTrue(imeet_Room.international_access_numbers.getText().contains("Canada"), "Canada not displayed in the list");	
			
				//Verify France is shown in the international list.
				s_assert.assertTrue(imeet_Room.international_access_numbers.getText().contains("France"), "France not displayed in the list");	
			
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	       	
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_25898_SP_30 close
	
} //main close