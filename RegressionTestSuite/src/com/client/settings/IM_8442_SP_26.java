package com.client.settings;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8442_SP_26 {
	
	public WebDriver reg_guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8442_SP_26";
	boolean isRegisterdGuestLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vguestfirstName, vguestlastName, vRegisteredGuestEmail;
		
	@Test //IM-8442:SP_26_Tabs displayed on Settings panel for registered guest
	public void IM_8442_SP_26_VerifyRegisteredGuest() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vguestfirstName= d.GuestFirstName;
		vguestlastName= d.GuestLastName;
		vRegisteredGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(reg_guest);
			reg_guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(reg_guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(reg_guest);
			Thread.sleep(20000);
			login.switchFrame(reg_guest);
			//Login with Host
			common.Host_LogIn_Option(reg_guest, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vguestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vguestlastName);
				
			//Login with Registered Guest
			login.field_EnterEmail.sendKeys(vRegisteredGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(reg_guest, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(reg_guest, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isRegisterdGuestLogedIn = common.Host_Audio_Panel_Display(reg_guest, sPath, PicName, isRegisterdGuestLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(2000);
				//Verify Meetings tab is shown for the registered guest.
				s_assert.assertTrue(imeet_Room.setting_meeting.isDisplayed(), "Meetings tab is not shown for the registered guest");
				imeet_Room.setting_meeting.click();
				Thread.sleep(2000);
				//Verify "Always Turn on my webcam automatically option shown under meeting tab
				s_assert.assertTrue(imeet_Room.setting_meeting_autowebcamstatus.isDisplayed(), "Always Turn on my webcam automatically" + ". option not shown under meeting tab for the registered guest");
				//Verify Auto full stage during one to one video option displayed
				s_assert.assertTrue(imeet_Room.setting_meeting_autofullstage_status.isDisplayed(), "Auto full stage during one to one video" + ". option not shown under meeting tab for the registered guest");
				//Verify F radio button shown for temperature
				s_assert.assertTrue(imeet_Room.setting_status_temp_F.isDisplayed(), "F radio button" + ". option not shown under meeting tab for the registered guest");
				//Verify C radio button shown for the temperature 
				s_assert.assertTrue(imeet_Room.setting_status_temp_C.isDisplayed(), "C radio button" + ". option not shown under meeting tab for the registered guest");
				//Verify 12hour radio button shown for the Clock
				s_assert.assertTrue(imeet_Room.setting_status_clock_12.isDisplayed(), "12hour radio" + ". option not shown under meeting tab for the registered guest");
				//Verify 24hour radio button shown for the Clock
				s_assert.assertTrue(imeet_Room.setting_status_clock_24.isDisplayed(), "24hour radio" + ". option not shown under meeting tab for the registered guest");
				
				//Verify Account tab is shown for the registered guest.
				imeet_Room.setting_account.click();
				Thread.sleep(2000);
				s_assert.assertTrue(imeet_Room.setting_account.isDisplayed(), "Account tab is not shown for the registered guest");
				
				//Verify Edit option for email field
				s_assert.assertTrue(imeet_Room.setting_account_editemailbutton.isDisplayed(), "Edit email button is not shown under Account tab for the registered guest");
				
				//Verify current password field
				s_assert.assertTrue(imeet_Room.setting_account_currentpassfield.isDisplayed(), "Current password filed is not shown under Account tab for the registered guest");
				
				//Verify New Password field
				s_assert.assertTrue(imeet_Room.setting_account_newpassfield.isDisplayed(), "New Password field is not shown under Account tab for the registered guest");
				
				//Verify confirm password field
				s_assert.assertTrue(imeet_Room.setting_account_confirmpassfield.isDisplayed(), "Confirm password field is not shown under Account tab for the registered guest");
				
				//Verify Forget your password link
				s_assert.assertTrue(imeet_Room.setting_account_forgotpaswordlink.isDisplayed(), "Forgot your password link is not shown under Account tab for the registered guest");
				
				//Verify Save button
				s_assert.assertTrue(imeet_Room.setting_account_savebutton.isDisplayed(), "Save button is not shown under Account tab for the registered guest");
				
				//Click on Privacy tab
				imeet_Room.setting_privacy.click();
				Thread.sleep(1000);
				
				//Verify Privacy tab is shown for the registered guest.
				s_assert.assertTrue(imeet_Room.setting_privacy.isDisplayed(), "Privacy tab is not shown for the registered guest");
				Thread.sleep(1000);
				
				//Verify Vcard option displayed.
				s_assert.assertTrue(imeet_Room.setting_vCard_download.isDisplayed(), "Download Vcard option is not shown under Privacy tab for the registered guest");
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	       	
		}
		catch (Throwable t)
		{
			sshot.ScrShot(reg_guest, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isRegisterdGuestLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(reg_guest);
				cbhost.SignOutFromMeetingRoom(reg_guest);			
			}
			Thread.sleep(1000);
			if (reg_guest != null)
			{
				reg_guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8442_SP_26 close
	
} //main close