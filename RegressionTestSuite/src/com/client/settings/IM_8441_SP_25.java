package com.client.settings;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8441_SP_25 {
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8441_SP_25";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd, vNewRoomUrl, vNewUrl, vRoomURl;
	String vhostfirstName, vhostlastName, vHostEmail;
		
	@Test //Change room address from Account tab when no guests in meeting room
	public void IM_8441_SP_25ChangeRoomUrl() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vGuestPswd = d.GuestPswd;
		vNewRoomUrl = d.NewRoomUrl;
		vNewUrl = d.NewUrl;
		vRoomURl = d.RoomUrl;
		ChooseBrowser cbhost = new ChooseBrowser(host);
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(20000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(1000);
				//Verify more option is present
				s_assert.assertTrue(imeet_Room.setting_more.isDisplayed(), "More option is not displayed");
				//Click on More
				imeet_Room.setting_more.click();
				//Click on Account
				imeet_Room.setting_more_account.click();
				Thread.sleep(2000);
				//Click on edit button to change room address
				imeet_Room.setting_account_changeroombutton.click();
				
				//Enter new room url
				imeet_Room.setting_account_roomurlfield.clear();
				imeet_Room.setting_account_roomurlfield.sendKeys(vNewRoomUrl);
				//Click on Save
				imeet_Room.setting_account_saveimeeturlbutton.click();
				Thread.sleep(3000);	
				
				//Click on OK
				imeet_Room.setting_account_changeurl_OK.click();
				
				//Log out
				cbhost.SignOutFromMeetingRoom(host);
				
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
			
			try
			{
			
				host.navigate().to(vNewUrl);
				//Login with Guest
				common.Guest_LogIn_Option(host, sPath, PicName);
				
				//Enter first name
				login.field_EnterFirstName.sendKeys(vhostfirstName);
				
				//Enter Last name
				login.field_EnterLastName.sendKeys(vhostlastName);
					
				//Login with Guest
				login.field_EnterEmail.sendKeys(vHostEmail);
					
				//Click on Join Meeting
				login.button_Submit.click();
				login.waitForElement(host, login.field_EnterPassword, 5);
		
				//Enter password
				common.Guest_Enter_Password(host, vPswd);
				
				//Verify Audio Connect panel
				isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
				
				//Revert the room url change so it may not affect other test cases
				try
				{
					//Click on Menu icon
					imeet_Room.control_menu.click();
					Thread.sleep(800);
					//Click on Setting
					imeet_Room.settings.click();
					Thread.sleep(800);
					//Click on Meetings
					imeet_Room.setting_meeting.click();
					Thread.sleep(1000);
					//Verify more option is present
					s_assert.assertTrue(imeet_Room.setting_more.isDisplayed(), "More option is not displayed");
					//Click on More
					imeet_Room.setting_more.click();
					//Click on Account
					imeet_Room.setting_more_account.click();
					Thread.sleep(2000);
					//Click on edit button to change room address
					imeet_Room.setting_account_changeroombutton.click();
					
					//Enter new room url
					imeet_Room.setting_account_roomurlfield.clear();
					imeet_Room.setting_account_roomurlfield.sendKeys(vRoomURl);
					//Click on Save
					imeet_Room.setting_account_saveimeeturlbutton.click();
					Thread.sleep(3000);	
					
					//Click on OK
					imeet_Room.setting_account_changeurl_OK.click();
				}
				catch(Exception ex)
				{
					Reporter.log("Exception is .." + ex.getMessage());
				}
				
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	       	
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8441_SP_25 close
	
} //main close