package com.client.settings;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8422_SP_04 {
	
	public WebDriver host;
	public WebDriver guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8422_SP_04";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName, vguestfirstName, vguestlastName, vHostEmail,vGuestEmail;
		
	@Test //Set 'Show only me on Stage' to Yes from Meeting tab
	public void IM_8422_SP_04showOnlymeOnStage() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(1000);
				//Click on show only me on stage
				String status_showonstage = imeet_Room.setting_meeting_showonstage_status.getAttribute("class");
				Thread.sleep(1000);
				if(StringUtils.containsIgnoreCase(status_showonstage, "turnOff"))
				{
					imeet_Room.setting_meeting_showonstage_status.click();
					Thread.sleep(800);
				}
				//Click on Done
				imeet_Room.setting_meeting_roomkey_Done.click();	
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	        //Login with Guest
			System.out.println("Now log in guest");
		
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(guest);
			guest = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
		
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
				
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
					
			//Login with Host
			login1.field_EnterEmail.sendKeys(vGuestEmail);
					
			//Click on Join Meeting
			login1.button_Submit.click();
			imeet_Room1.waitForElement(guest, login1.field_EnterPassword, 5);
			common.Guest_Enter_Password(guest, vPswd);
				
			//Verify Audio Connect panel
			boolean isGuestLogedIn = false;
			isGuestLogedIn = common.Host_Audio_Panel_Display(guest, sPath, PicName, isGuestLogedIn);
			
			//Verify that guest cube is not shown in meeting room
			//Store User Id into string
			imeet_Room.participants_button.click();
			Thread.sleep(1000);
	        String attrbutename = host.findElement(By.xpath("//span[contains(.,'"+vhostfirstName.toUpperCase()+" "+vhostlastName.toUpperCase()+"')]")).getAttribute("id");
			String[] parts = attrbutename.split("r");
			String attributID = parts[1]; // 4644292
			
			//Host click on the eye icon
			WebElement GuestName = host.findElement(By.xpath("//*[@id='eye"+attributID+"']"));
			GuestName.click();
			Thread.sleep(500);
			
			//Verify host can see the cube again
			s_assert.assertTrue(host.findElement(By.xpath("//img[@id='avatarImage"+attributID+"']")).isDisplayed());
			//Change the setting to no from host window
			
			//Verify host can see the cube now
			s_assert.assertTrue(host.findElement(By.xpath("//img[@id='avatarImage"+attributID+"']")).isDisplayed());
			
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8422_SP_04 close
	
} //main close