package com.client.settings;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_25727_SP_15 {
	
	public WebDriver host;
	public WebDriver guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_25727_SP_15";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vhostfirstName, vhostlastName, vguestfirstName, vguestlastName, vHostEmail,vGuestEmail, oldmeetingname, meetingname, newMeetingname;
		
	@Test //IM-25727:SP_15_Settings remains same on updating room name
	public void IM_25727_SP_15() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vguestfirstName = d.GuestFirstName;
		vguestlastName = d.GuestLastName;
		vGuestEmail = d.RegisteredGuestEmail;
		vGuestPswd = d.GuestPswd;
		oldmeetingname = d.MeetingName;
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(2000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{				
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(1000);
				
				//Check the meeting Settings
				//Show only me on stage
				String status_showonstage = imeet_Room.setting_meeting_showonstage_status.getAttribute("class");
				Thread.sleep(1000);
				System.out.println("status_showonstage.." + status_showonstage);
				
				//Enter Meeting name  surya�s iMeet Room
			    //getting current date and time using Date class
			    DateFormat df = new SimpleDateFormat("mmss");
			    Date dateobj = new Date();
			    String d = df.format(dateobj).toString();
			    newMeetingname = oldmeetingname+d;
			    System.out.println(d);
				imeet_Room.setting_meeting_name.clear();
				imeet_Room.setting_meeting_name.sendKeys(newMeetingname);
		
				//Click on Done
				imeet_Room.setting_meeting_roomkey_Done.click();	
				Thread.sleep(4000);
				//Store meeting text
				meetingname = imeet_Room.text_meeting_name.getText();
				System.out.println("meetingname .." + meetingname + "newMeetingname.." + newMeetingname);
				//Verify Updated meeting name
				s_assert.assertTrue(meetingname.equalsIgnoreCase(newMeetingname),"Meeting name is not shown updated for host:.. Expected Meeting name is: "+ newMeetingname + "   but shown :.." + meetingname + "");
				System.out.println("title is   " + host.getTitle());
				//Verify Updated Browser tab name
				s_assert.assertTrue(host.getTitle().equalsIgnoreCase(newMeetingname),"Browser tab name is not shown updated for host:");
				
				//Verify that meeting remains same.
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(1000);
				
				//Show only me on stage
				String status_showonstageR = imeet_Room.setting_meeting_showonstage_status.getAttribute("class");
				Thread.sleep(1000);
				System.out.println("status_showonstage after return.." + status_showonstage);
				
				//Verify Setting not changed after meeting room updated.
				s_assert.assertTrue(status_showonstageR.equals(status_showonstage),"Setting for show me on stage changed after meeting room name changed.");
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	        //Login with Guest
			System.out.println("Now log in guest");
		
			//Choose Browser and open for the Guest
			ChooseBrowser chooseBrowser1 = new ChooseBrowser(guest);
			guest = chooseBrowser1.OpenBrowser(vBrowserType, vUrl);
			
			//Creating required objects
			iMeetLogin_Page login1 = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room1 = new iMeetRoom_Page(guest);
			Thread.sleep(2000);
			login1.switchFrame(guest);
			//Click on Join
			common.Guest_LogIn_Option(guest, sPath, PicName);
		
			//Enter first name
			login1.field_EnterFirstName.sendKeys(vguestfirstName);
				
			//Enter Last name
			login1.field_EnterLastName.sendKeys(vguestlastName);
					
			//Login with Guest
			login1.field_EnterEmail.sendKeys(vGuestEmail);
					
			//Click on Join Meeting
			login1.button_Submit.click();
			imeet_Room1.waitForElement(guest, login1.field_EnterPassword, 5);
			common.Guest_Enter_Password(guest, vPswd);
			
			try
			{				
				//Verify Audio Connect panel
				boolean isGuestLogedIn = false;
				isGuestLogedIn = common.Host_Audio_Panel_Display(guest, sPath, PicName, isGuestLogedIn);
				if(isGuestLogedIn)
				{
					Reporter.log("Pass.Guest loged in");
					System.out.println("Pass.Guest loged in");
				}
				else
				{
					Reporter.log("Fail. Guest not loged in");
					System.out.println("Fail. Guest not loged in");
				}
				
				//Verify Updated meeting name
				s_assert.assertTrue(meetingname.equalsIgnoreCase(newMeetingname),"Meeting name is not shown updated for Guest:.. Expected Meeting name is: "+ newMeetingname + "   but shown :.." + meetingname + "");
				
			}
			catch(Throwable t)
			{
				sshot.ScrShot(host, sPath, PicName+common.CurrentTime()+".jpg");
				s_assert.fail("Error occured to verify meering locked by room key. Detail " + t.getMessage());
			}
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				ChooseBrowser cbhost = new ChooseBrowser(host);
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null && guest != null)
			{
				host.quit();
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8419_SP_01 close
	
} //main close