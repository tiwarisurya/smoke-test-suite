package com.client.settings;

import java.io.IOException;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_8443_SP_27 {
	
	public WebDriver guest;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_8443_SP_27";
	boolean isGuestLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd;
	String vGuestfirstName, vGuestlastName, vGuestEmail;
		
	@Test //IM-8443:SP_27_Save profile window on clicking Settings for unregistered guest
	public void IM_8443_SP_27_SaveProfile() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		Random r = new Random();
		int num = r.nextInt(1000);
		ChooseBrowser cbhost = new ChooseBrowser(guest);
		
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vGuestfirstName= d.GuestFirstName;
		vGuestlastName= d.GuestLastName;
		vGuestEmail = num+d.GuestEmail;
		vGuestPswd = d.GuestPswd;
		System.out.println("new emai is .." + vGuestEmail);
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(guest);
			guest = chooseBrowser.OpenBrowser(vBrowserType, vUrl);

			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(guest);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(guest);
			Thread.sleep(20000);
			login.switchFrame(guest);
			//Login with Guest
			common.Guest_LogIn_Option(guest, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vGuestfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vGuestlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vGuestEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			
			//Verify Audio Connect panel
			common.Guest_Audio_Panel_Display(guest, sPath, PicName);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);

				//Verify Save Profile window displayed
				String text_save_profile = imeet_Room.settings_unreg_guest_saveProfile.getText();
				System.out.println("text_save_profile..." + text_save_profile);
				s_assert.assertTrue(text_save_profile.contains("SAVE PROFILE"), "SAVE PROFILE window not shown for unregistered guest");
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
			try
			{
				//Enter new password into Create Password
				imeet_Room.settings_unreg_guest_enterpass.clear();
				imeet_Room.settings_unreg_guest_enterpass.sendKeys(vGuestPswd);
				//Enter password into renter password field
				imeet_Room.settings_unreg_guest_re_enterpass.clear();
				imeet_Room.settings_unreg_guest_re_enterpass.sendKeys(vGuestPswd);
				//Click on Save
				imeet_Room.settings_unreg_guest_savebutton.click();
				Thread.sleep(3000);
				
				//Click on Done
				imeet_Room.settings_unreg_guest_details_donebutton.click();
				Thread.sleep(3000);
				
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
			
			try
			{
				cbhost.SignOutFromMeetingRoom(guest);
				
				guest.navigate().to(vUrl);
				//Login with Guest
				common.Guest_LogIn_Option(guest, sPath, PicName);
				
				//Enter first name
				login.field_EnterFirstName.sendKeys(vGuestfirstName);
				
				//Enter Last name
				login.field_EnterLastName.sendKeys(vGuestlastName);
					
				//Login with Guest
				login.field_EnterEmail.sendKeys(vGuestEmail);
					
				//Click on Join Meeting
				login.button_Submit.click();
				login.waitForElement(guest, login.field_EnterPassword, 5);
		
				//Enter password
				common.Guest_Enter_Password(guest, vPswd);
				
				//Verify Audio Connect panel
				common.Guest_Audio_Panel_Display(guest, sPath, PicName);
				
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
			
			//Click on Menu icon
			imeet_Room.control_menu.click();
			Thread.sleep(800);
			//Click on Setting
			imeet_Room.settings.click();
			Thread.sleep(2000);
			//Verify Meetings tab is shown for the registered guest.
			s_assert.assertTrue(imeet_Room.setting_meeting.isDisplayed(), "Meetings tab is not shown for the registered guest");
			imeet_Room.setting_meeting.click();
			Thread.sleep(2000);
			//Verify "Always Turn on my webcam automatically option shown under meeting tab
			s_assert.assertTrue(imeet_Room.setting_meeting_autowebcamstatus.isDisplayed(), "Always Turn on my webcam automatically" + ". option not shown under meeting tab for the registered guest");
			//Verify Auto full stage during one to one video option displayed
			s_assert.assertTrue(imeet_Room.setting_meeting_autofullstage_status.isDisplayed(), "Auto full stage during one to one video" + ". option not shown under meeting tab for the registered guest");
			//Verify F radio button shown for temperature
			s_assert.assertTrue(imeet_Room.setting_status_temp_F.isDisplayed(), "F radio button" + ". option not shown under meeting tab for the registered guest");
			//Verify C radio button shown for the temperature 
			s_assert.assertTrue(imeet_Room.setting_status_temp_C.isDisplayed(), "C radio button" + ". option not shown under meeting tab for the registered guest");
			//Verify 12hour radio button shown for the Clock
			s_assert.assertTrue(imeet_Room.setting_status_clock_12.isDisplayed(), "12hour radio" + ". option not shown under meeting tab for the registered guest");
			//Verify 24hour radio button shown for the Clock
			s_assert.assertTrue(imeet_Room.setting_status_clock_24.isDisplayed(), "24hour radio" + ". option not shown under meeting tab for the registered guest");
			
			//Verify Account tab is shown for the registered guest.
			imeet_Room.setting_account.click();
			Thread.sleep(2000);
			s_assert.assertTrue(imeet_Room.setting_account.isDisplayed(), "Account tab is not shown for the registered guest");
			
			//Verify Edit option for email field
			s_assert.assertTrue(imeet_Room.setting_account_editemailbutton.isDisplayed(), "Edit email button is not shown under Account tab for the registered guest");
			
			//Verify current password field
			s_assert.assertTrue(imeet_Room.setting_account_currentpassfield.isDisplayed(), "Current password filed is not shown under Account tab for the registered guest");
			
			//Verify New Password field
			s_assert.assertTrue(imeet_Room.setting_account_newpassfield.isDisplayed(), "New Password field is not shown under Account tab for the registered guest");
			
			//Verify confirm password field
			s_assert.assertTrue(imeet_Room.setting_account_confirmpassfield.isDisplayed(), "Confirm password field is not shown under Account tab for the registered guest");
			
			//Verify Forget your password link
			s_assert.assertTrue(imeet_Room.setting_account_forgotpaswordlink.isDisplayed(), "Forgot your password link is not shown under Account tab for the registered guest");
			
			//Verify Save button
			s_assert.assertTrue(imeet_Room.setting_account_savebutton.isDisplayed(), "Save button is not shown under Account tab for the registered guest");
			
			//Click on Privacy tab
			imeet_Room.setting_privacy.click();
			Thread.sleep(1000);
			
			//Verify Privacy tab is shown for the registered guest.
			s_assert.assertTrue(imeet_Room.setting_privacy.isDisplayed(), "Privacy tab is not shown for the registered guest");
			Thread.sleep(1000);
			
			//Verify Vcard option displayed.
			s_assert.assertTrue(imeet_Room.setting_vCard_download.isDisplayed(), "Download Vcard option is not shown under Privacy tab for the registered guest");
		}
		catch(Exception ex)
		{
			Reporter.log("Exception is .." + ex.getMessage());
		}
	       	
		catch (Throwable t)
		{
			sshot.ScrShot(guest, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isGuestLogedIn)
			{
				cbhost.SignOutFromMeetingRoom(guest);			
			}
			Thread.sleep(1000);
			if (guest != null)
			{
				guest.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8443_SP_27 close
	
} //main close