package com.client.settings;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.config.ChooseBrowser;
import com.pageobject.iMeetLogin_Page;
import com.pageobject.iMeetRoom_Page;
import com.utilities.CommonFunctions;
import com.utilities.ReadData;
import com.utilities.TakeScreenShot;
import com.utilities.TestData;

public class IM_25901_SP_33 {
	
	public WebDriver host;
	SoftAssert s_assert = new SoftAssert();
	TestData d = new TestData();
	TakeScreenShot sshot = new TakeScreenShot();
	String sPath = ReadData.getScreenshotDirectory();
	String PicName = "IM_25901_SP_33";
	boolean isHostLogedIn = false;
	
	String TestCaseName, vBrowserType, vUrl, vPswd, vGuestPswd, vNewGuestPswd;
	String vhostfirstName, vhostlastName, vHostEmail;
		
	@Test //To verify user is able to change the password from the Account tab
	public void IM_25901_SP_33ChangePassword() throws IOException, Exception
	{	
		TestCaseName = new Object(){}.getClass().getEnclosingMethod().getName();
		System.out.println("Executing Scenario ........" + TestCaseName);
		vBrowserType  = d.BrowserType;
		vUrl = d.Url;
		vPswd = d.Pswd;
		vhostfirstName= d.HostFirstName;
		vhostlastName= d.HostlastName;
		vHostEmail = d.HostEmail;
		vGuestPswd = d.GuestPswd;
		vNewGuestPswd = d.NewGuestPswd;
		ChooseBrowser cbhost = new ChooseBrowser(host);
		
		try
		{
			//Choose Browser and open for the Host
			ChooseBrowser chooseBrowser = new ChooseBrowser(host);
			host = chooseBrowser.OpenBrowser(vBrowserType, vUrl);
			
			CommonFunctions common = new CommonFunctions();
			Thread.sleep(1000);
		
			//Creating required objects
			iMeetLogin_Page login = new iMeetLogin_Page(host);
			iMeetRoom_Page imeet_Room = new iMeetRoom_Page(host);
			Thread.sleep(20000);
			login.switchFrame(host);
			//Login with Host
			common.Host_LogIn_Option(host, sPath, PicName);
			
			//Enter first name
			login.field_EnterFirstName.sendKeys(vhostfirstName);
			
			//Enter Last name
			login.field_EnterLastName.sendKeys(vhostlastName);
				
			//Login with Guest
			login.field_EnterEmail.sendKeys(vHostEmail);
				
			//Click on Join Meeting
			login.button_Submit.click();
			login.waitForElement(host, login.field_EnterPassword, 5);
			
			common.Host_Enter_Password(host, vPswd, TestCaseName);
			
			//Verify Audio Connect panel
			isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
			
			try
			{
				//Click on Menu icon
				imeet_Room.control_menu.click();
				Thread.sleep(800);
				//Click on Setting
				imeet_Room.settings.click();
				Thread.sleep(800);
				//Click on Meetings
				imeet_Room.setting_meeting.click();
				Thread.sleep(1000);
				//Verify more option is present
				s_assert.assertTrue(imeet_Room.setting_more.isDisplayed(), "More option is not displayed");
				//Click on More
				imeet_Room.setting_more.click();
				//Click on Account
				imeet_Room.setting_more_account.click();
				Thread.sleep(2000);
				//Enter current password
				imeet_Room.setting_account_currentpassfield.clear();
				imeet_Room.setting_account_currentpassfield.sendKeys(vGuestPswd);
				//Enter new password
				imeet_Room.setting_account_newpassfield.clear();
				imeet_Room.setting_account_newpassfield.sendKeys(vNewGuestPswd);
				//Enter confirm password
				imeet_Room.setting_account_confirmpassfield.clear();
				imeet_Room.setting_account_confirmpassfield.sendKeys(vNewGuestPswd);
				//Click on Save
				imeet_Room.setting_account_savebutton.click();
				Thread.sleep(3000);	
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
			
			try
			{
				cbhost.SignOutFromMeetingRoom(host);
				
				host.navigate().to(vUrl);
				//Login with Guest
				common.Guest_LogIn_Option(host, sPath, PicName);
				
				//Enter first name
				login.field_EnterFirstName.sendKeys(vhostfirstName);
				
				//Enter Last name
				login.field_EnterLastName.sendKeys(vhostlastName);
					
				//Login with Guest
				login.field_EnterEmail.sendKeys(vHostEmail);
					
				//Click on Join Meeting
				login.button_Submit.click();
				login.waitForElement(host, login.field_EnterPassword, 5);
		
				//Enter password
				common.Guest_Enter_Password(host, vNewGuestPswd);
				
				//Verify Audio Connect panel
				isHostLogedIn = common.Host_Audio_Panel_Display(host, sPath, PicName, isHostLogedIn);
				
				//Revert the password change so it may not affect other test cases
				try
				{
					//Click on Menu icon
					imeet_Room.control_menu.click();
					Thread.sleep(800);
					//Click on Setting
					imeet_Room.settings.click();
					Thread.sleep(800);
					//Click on Meetings
					imeet_Room.setting_meeting.click();
					Thread.sleep(1000);
					//Verify more option is present
					s_assert.assertTrue(imeet_Room.setting_more.isDisplayed(), "More option is not displayed");
					//Click on More
					imeet_Room.setting_more.click();
					//Click on Account
					imeet_Room.setting_more_account.click();
					Thread.sleep(2000);
					//Enter current password
					imeet_Room.setting_account_currentpassfield.clear();
					imeet_Room.setting_account_currentpassfield.sendKeys(vNewGuestPswd);
					//Enter new password
					imeet_Room.setting_account_newpassfield.clear();
					imeet_Room.setting_account_newpassfield.sendKeys(vGuestPswd);
					//Enter confirm password
					imeet_Room.setting_account_confirmpassfield.clear();
					imeet_Room.setting_account_confirmpassfield.sendKeys(vGuestPswd);
					//Click on Save
					imeet_Room.setting_account_savebutton.click();
					Thread.sleep(3000);	
				}
				catch(Exception ex)
				{
					Reporter.log("Exception is .." + ex.getMessage());
				}
				
			}
			catch(Exception ex)
			{
				Reporter.log("Exception is .." + ex.getMessage());
			}
	       	
		}
		catch (Throwable t)
		{
			sshot.ScrShot(host, sPath, PicName+".jpg");
			s_assert.fail("Error Occured . Detail " + t.getMessage());
		}
		finally
		{
			if(isHostLogedIn)
			{
				cbhost.SignOutFromMeetingRoom(host);			
			}
			Thread.sleep(1000);
			if (host != null)
			{
				host.quit();
				Thread.sleep(1000);
			}
		}
		s_assert.assertAll();
			
	} //Test IM_8444_SP_19 close
	
} //main close